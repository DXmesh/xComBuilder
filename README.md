## xComBuilder

用于 DXMesh 应用组件开发的生成器。提供 dxc 工程创建、工程代码生成、组件编译、组件打包以及安装的命令。


## 使用方法

- 在任一目录下 下载代码, 命令如下：

  ```git
  git clone  https://gitee.com/DXmesh/xComBuilder.git
  ```


## 编译 xCom Builder

进入 xComBuilder 目录，在控制台执行命令，按照到 cargo 目录：

```
cargo install --path . 
```

  提供的具体命令如下：

  ```
    build      编译 dxc
    init       初始化工程
    install    安装到xport
    new        创建工程
    pack       打包成组件(DXC)
    unpack     解压成组件(DXC)
  ```

- new :

  该命令会在当前目录中，生成 protos 文件夹，以及工程所需要的 protobuf 文件。

  可选参数：
  ```
    -n, --name <name>          服务名字
    -p, --path <path>          工程路径 [default: ./]
    -v, --version <version>    版本号 [default: 0.0.1]
  ```

- init :

  编写好 protobuf 文件后，在当前路径下执行该命令，会生成 dxc 工程所需的代码以及配置文件。

  可选参数：
  ```
    -f, --force       强制初始化
    -u, --use-self    生成调用自身SDK
    -l, --language    生成的语言工程，目前支持 生成 python 和 rust。其中 python 工程，输入： python 或 py。rust 工程，输入 rust 或 rs
  ```

- build :

  编写好组件代码后，执行该命令，会编译工程。

  可选参数：
  ```
  -r, --release    编译 release
  ```
  默认编译为 debug 模式的组件。如果需要编译 release 的组件则加上 -r 或者 --release 参数

- pack

  打包编译好的 dxc 组件, 打包成标准的 dxc 组件。并且会在输出目录，生成自身接口的 protobuf 文件

  可选参数：
  ```
    -m, --mode <mode>        打包的编译内容，输入 debug 或 release默认为 debug [default: debug]
    -o, --output <output>    输出路径 [default: ./]
    -p, --path <path>        工程路径 [default: ./]
  ```

- install

  将组件安装到 xPort 中。

  可选参数：
  ```
    -d, --dxc-path <dxc-path>        DXC路径，默认寻找当前工程生成的 dxc 文件
    -x, --xport-path <xport-path>    Xport所在路径 [default: ./]
  ```

- unpack

  将打包好的 dxc 组件，解包到指定的输出目录。解压后的文件有：
  - manifest.json 描述 dxc 基本信息，如：名字，版本号等
  - source-api.proto dxc 的接口描述文件 
  - XportService.dll/so dxc 组件

  可选参数：
 
  ```
    -o, --output <output>    输出路径
    -p, --path <path>        工程路径
  ```

