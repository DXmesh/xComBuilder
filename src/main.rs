mod generator;
mod proto_utils;
use crate::generator::{get_dxc_name_and_version_source_api, get_source_proto_file_name};
use generator::gen_doc;
use generator::gen_serial_code;
use generator::gen_tauri;
use generator::{gen_sdk, init_project, new_project, pack_project};
use std::fmt::Debug;
use std::io::BufRead;
use std::io::BufReader;
use std::process::{Command, Stdio};
use structopt::StructOpt;
use tracing::{error, info, Level};
use x_common_lib::utils::env_utils;
use x_common_lib::utils::file_utils;

fn init_logger() {
    let subscriber = tracing_subscriber::fmt()
        .with_max_level(Level::TRACE)
        .with_file(false)
        .with_target(false)
        .with_line_number(false)
        .finish();
    tracing::subscriber::set_global_default(subscriber).expect("初始化日志库失败！");
}

#[derive(StructOpt, Debug)]
#[structopt(name = "xbuilder", about = "DXMesh 组件开发工具", version = "0.1.10")]
enum DXCCommand {
    #[structopt(name = "new", about = "创建工程")]
    New {
        #[structopt(short, long, help = "工程路径", default_value = "./")]
        path: String,

        #[structopt(short, long, help = "服务名字")]
        name: String,

        #[structopt(short, long, help = "版本号", default_value = "0.0.1")]
        version: String,
    },

    #[structopt(name = "init", about = "初始化工程")]
    Init {
        #[structopt(short, long, help = "工程路径", default_value = "./")]
        path: String,

        #[structopt(short, long, help = "强制初始化")]
        force: bool,

        #[structopt(short, long, help = "生成调用自身SDK")]
        use_self: bool,

        #[structopt(
            short,
            long,
            help = "python 工程，输入： python 或 py。rust 工程，输入 rust 或 rs, java 工程 输入 java",
            default_value = "rust"
        )]
        language: String,
    },

    #[structopt(name = "pack", about = "打包成组件(DXC)")]
    Pack {
        #[structopt(short, long, help = "工程路径", default_value = "./")]
        path: String,

        #[structopt(short, long, help = "输出路径", default_value = "./")]
        output: String,

        #[structopt(
            short,
            long,
            help = "打包的编译内容，输入 debug 或 release默认为 debug",
            default_value = "debug"
        )]
        mode: String,
    },

    #[structopt(name = "unpack", about = "解压成组件(DXC)")]
    UnPack {
        #[structopt(short, long, help = "工程路径")]
        path: String,

        #[structopt(short, long, help = "输出路径")]
        output: String,
    },

    #[structopt(name = "gen", about = "生成序列化/反序列化文件")]
    Gen {
        #[structopt(short, long, help = "工程路径", default_value = "./")]
        path: String,

        #[structopt(
            short,
            long,
            help = "生成的代码类型 serial sdk tauri sys dqtrader",
            default_value = "serial"
        )]
        code: String,

        #[structopt(short, long, help = "文件名", default_value = "")]
        name: String,

        #[structopt(short, long, help = "输出路径", default_value = "./")]
        output: String,

        #[structopt(short, long, help = "原生代码")]
        raw: bool,
    },

    #[structopt(name = "install", about = "安装到xport")]
    Install {
        #[structopt(short, long, help = "Xport所在路径", default_value = "./")]
        xport_path: String,

        #[structopt(short, long, help = "DXC路径", default_value = "")]
        dxc_path: String,

        #[structopt(short, long, help = "编译/打包/安装")]
        all: bool,
    },

    #[structopt(name = "build", about = "编译 dxc")]
    Build {
        #[structopt(short, long, help = "编译 release")]
        release: bool,
    },

    #[structopt(name = "doc", about = "生成文档")]
    Doc {
        #[structopt(short, long, help = "输出路径", default_value = "./")]
        output: String,
    },
}

async fn do_build(release: bool) {
    let mut args = vec!["build".to_string()];
    if release {
        args.push("--release".to_string());
    }

    let mut child = Command::new("cargo")
        .args(&args)
        .stdout(Stdio::piped()) // 设置标准输出为管道，以便读取输出
        .spawn()
        .expect("Failed to execute 'cargo build'");

    // 获取标准输出的读取器
    let stdout = child.stdout.take().expect("Failed to get stdout");
    // 逐行读取输出并输出到控制台
    let reader = BufReader::new(stdout);
    for line in reader.lines() {
        if let Ok(line) = line {
            println!("{}", line);
        }
    }
    // 等待命令执行完成
    let status = child.wait().expect("Failed to wait for command");

    // 检查命令是否成功执行
    if status.success() {
        println!("Build succeeded!");
    } else {
        println!("Build failed!");
    }
}

#[tokio::main]
async fn main() {
    init_logger();
    let command = DXCCommand::from_args();
    match command {
        DXCCommand::New {
            path,
            name,
            version,
        } => {
            //
            new_project::new(path, name, version).await;
        }
        DXCCommand::Init {
            path,
            force,
            use_self,
            language,
        } => {
            //
            init_project::gen(path, force, use_self, language).await;
        }
        DXCCommand::Build { release } => {
            do_build(release).await;
        }
        DXCCommand::Pack { path, output, mode } => {
            // 获取文件列表
            let python_core_path = format!("{}/src/x_com/xport_core.py", path);
            if file_utils::is_file_exist(&python_core_path) {
                pack_project::pack_python(path, output, mode).await;
                return;
            }
            //
            let rust_core_path = format!("{}/src/x_com/xport_core.rs", path);
            if file_utils::is_file_exist(&rust_core_path) {
                pack_project::pack_rust(path, output, mode).await;
                return;
            }

            let java_core_path = format!("{}/src/main/java/io/dxmesh/Lib.java", path);
            if file_utils::is_file_exist(&java_core_path) {
                pack_project::pack_java(path, output, mode).await;
                return;
            }
        }
        DXCCommand::UnPack { path, output } => {
            pack_project::unpack(path, output).await;
        }
        DXCCommand::Gen {
            path,
            output,
            name,
            code,
            raw,
        } => match code.as_str() {
            "serial" => {
                gen_serial_code::gen_rust(path, output, name, false).await;
            }
            "sys" => {
                gen_serial_code::gen_sys().await;
            }
            "sdk" => {
                gen_sdk::rust::gen(path, String::default(), output, false, None, None, None).await;
            }
            "dqtrader" => {
                gen_sdk::rust::gen_dqtrader(path).await;
            }
            "tauri" => {
                if raw {
                    gen_tauri::gen_raw_api(path, output).await;
                } else {
                    gen_tauri::gen_http_api(path, output).await;
                }
            }
            _ => {
                error!("无法生成 {} 类型的代码", code);
            }
        },

        DXCCommand::Install {
            xport_path,
            dxc_path,
            all,
        } => {
            if all {
                do_build(false).await;
                pack_project::pack_rust("./".into(), "./".into(), "debug".into()).await;
            }

            let mut dxc_with_version = String::default();

            let dxc_path = if dxc_path.is_empty() {
                let project_path = file_utils::get_absoule_path("./").expect("获取当前路径失败！");
                let source_file_name = get_source_proto_file_name(&project_path).await;
                let (package_name, package_version) =
                    get_dxc_name_and_version_source_api(&project_path, &source_file_name);

                let sys_cpu_info = env_utils::get_system_cpu_info().unwrap();

                dxc_with_version = format!("{}-{}", package_name, package_version);

                let dxc_path = format!(
                    "{}/{}-{}-{}.dxc",
                    project_path, package_name, package_version, sys_cpu_info
                );

                if file_utils::is_file_exist(&dxc_path) {
                    dxc_path
                } else {
                    format!(
                        "{}/{}-{}-none-none.dxc",
                        project_path, package_name, package_version
                    )
                }
            } else {
                dxc_path
            };

            let file_name = file_utils::get_file_name_from_path(&dxc_path);
            if file_name.is_none() {
                error!("安装失败: DXC 不存在！");
                return;
            }
            // 删除现存的 dxc 组件

            let dxc_folder = format!("{}/dxc", xport_path);
            if file_utils::is_dir_exist_sync(&dxc_folder) {
                let exist_dxc_list = file_utils::get_file_list(&dxc_folder).await.unwrap();
                let dxc_with_version = dxc_with_version.to_lowercase();
                for exist_dxc in exist_dxc_list {
                    let lower_exist_dxc = exist_dxc.to_lowercase();
                    if lower_exist_dxc.starts_with(&dxc_with_version) {
                        let old_dxc_path = format!("{}/dxc/{}", xport_path, exist_dxc);

                        let ret = if file_utils::is_dir_exist_sync(&old_dxc_path) {
                            file_utils::remove_all_dir(&old_dxc_path).await
                        } else {
                            file_utils::remove_file_sync(&old_dxc_path)
                        };

                        if ret.is_err() {
                            error!(
                                "安装失败:{}, 删除旧组件目录失败！",
                                ret.err().unwrap().to_string()
                            );

                            return;
                        }
                    }
                }
            }
            //
            let file_name = file_name.unwrap();
            let dst = format!("{}/dxc/{}", xport_path, file_name);
            let ret = file_utils::copy_file(&dxc_path, &dst).await;
            if ret.is_err() {
                error!("安装失败:{}", ret.err().unwrap().to_string());
            } else {
                info!("安装成功！")
            }
        }
        DXCCommand::Doc { output } => {
            gen_doc::gen(output).await;
        }
    };
}
