use x_common_lib::{
    serial::api_descriptor::{get_inner_field, is_array, is_optional, ServiceApiDescriptor},
    utils::file_utils,
};

use crate::proto_utils::proto_field_type_to_proto;

fn remove_first_dot(content: String) -> String {
    if content.starts_with(".") {
        content.chars().skip(1).collect()
    } else {
        content
    }
}

pub async fn gen(output_path: String) {
    let protobuf_path = "./protos/source-api-0.0.1.proto";
    let protobuf_path = file_utils::get_absoule_path(protobuf_path);
    if protobuf_path.is_none() {
        panic!("文件: source-api-0.0.1.proto 不存在！");
    }

    let protobuf_path = protobuf_path.unwrap();
    let service_api_descriptor = ServiceApiDescriptor::load_with_include_path(&protobuf_path, true);
    if let Err(err) = service_api_descriptor {
        panic!("解析描述文件 {} 出错：{}...", protobuf_path, err);
    }
    //
    let service_api_descriptor = service_api_descriptor.unwrap();

    let mut builder = string_builder::Builder::new(1024 * 1024);

    builder.append("### API 列表\n");
    builder.append("|Api|输入参数|输出参数|说明|\n");
    builder.append("|--------|--------|--------|--------|\n");
    for method in service_api_descriptor.methods() {
        let input_type = method.input_type.get_path();
        let input_type = if input_type.is_empty() {
            String::from("无")
        } else {
            remove_first_dot(input_type)
        };
        let output_type = method.output_type.get_path();
        let output_type = if output_type.is_empty() {
            String::from("无")
        } else {
            remove_first_dot(output_type)
        };
        let annotation = if let Some(ref annotation) = method.annotation {
            annotation.value.clone()
        } else {
            String::from("无")
        };

        builder.append(format!(
            "|{}|{}|{}|{}|\n",
            method.name, input_type, output_type, annotation
        ));
    }

    if let Some(ref topic) = service_api_descriptor.topic {
        builder.append("### Topic 列表\n");
        builder.append("|Topic|参数|说明|\n");
        builder.append("|--------|--------|--------|\n");

        for method in topic.methods.iter() {
            let input_type = method.input_type.get_path();
            let input_type = if input_type.is_empty() {
                String::from("无")
            } else {
                remove_first_dot(input_type)
            };

            let annotation = if let Some(ref annotation) = method.annotation {
                annotation.value.clone()
            } else {
                String::from("无")
            };

            builder.append(format!("|{}|{}|{}|\n", method.name, input_type, annotation));
        }
    }

    //
    for event in service_api_descriptor.events.iter() {
        builder.append(format!("### 订阅 {} 事件\n", event.name));

        builder.append("|事件|说明|\n");
        builder.append("|--------|--------|\n");
        for event_item in event.event_items.iter() {
            let annotation = if let Some(ref annotation) = event_item.annotation {
                annotation.value.clone()
            } else {
                String::from("无")
            };

            builder.append(format!("|{}|{}|\n", event_item.name, annotation));
        }
    }

    for topic in service_api_descriptor.subscribe_topics.iter() {
        builder.append(format!("### 订阅 {} 主题\n", topic.name));

        builder.append("|主题|参数|说明|\n");
        builder.append("|--------|--------|--------|\n");

        for method in topic.methods.iter() {
            let annotation = if let Some(ref annotation) = method.annotation {
                annotation.value.clone()
            } else {
                String::from("无")
            };

            let input_type = method.input_type.get_path();
            let input_type = if input_type.is_empty() {
                String::from("无")
            } else {
                remove_first_dot(input_type)
            };

            builder.append(format!("|{}|{}|{}|\n", method.name, input_type, annotation));
        }
    }

    if !service_api_descriptor.enum_descriptor_map.is_empty()
        || !service_api_descriptor.message_descriptor_holder.is_empty()
    {
        //
        builder.append("### 出/入参说明 \n\n");

        // 枚举
        for enum_value in service_api_descriptor.enum_descriptor_map.values() {
            builder.append(format!("#### {}(枚举)\n", enum_value.name));

            let annotation = if let Some(ref annotation) = enum_value.annotation {
                annotation.value.clone()
            } else {
                String::from("无")
            };
            builder.append(format!("说明: {} \n", annotation));
            builder.append("|字段|值|说明|\n");
            builder.append("|--------|--------|--------|\n");
            for item in enum_value.values.iter() {
                let annotation = if let Some(ref annotation) = item.annotation {
                    annotation.value.clone()
                } else {
                    String::from("无")
                };
                builder.append(format!("|{}|{}|{}|\n", item.name, item.number, annotation));
            }
        }
        // 结构体
        for message_descriptor_holder in service_api_descriptor.message_descriptor_holder.values() {
            builder.append(format!(
                "#### {}(结构体)\n",
                message_descriptor_holder.descriptor_proto.name
            ));

            let annotation = if let Some(ref annotation) =
                message_descriptor_holder.descriptor_proto.annotation
            {
                annotation.value.clone()
            } else {
                String::from("无")
            };

            builder.append(format!("说明: {} \n", annotation));
            //
            builder.append("|字段|类型|必选|说明\n");
            builder.append("|--------|--------|--------|--------|\n");

            for field in message_descriptor_holder.descriptor_proto.fields.iter() {
                let field = get_inner_field(field).unwrap();

                let is_optional = if is_optional(field) {
                    String::from("否")
                } else {
                    String::from("是")
                };

                let annotation = if let Some(ref annotation) = field.annotation {
                    annotation.value.clone()
                } else {
                    String::from("无")
                };

                let type_name = proto_field_type_to_proto(&field.typ);

                let type_name = if is_array(field) {
                    format!("Array&lt;{}&gt;", type_name)
                } else {
                    type_name
                };

                builder.append(format!(
                    "|{}|{}|{}|{}|\n",
                    field.name, type_name, is_optional, annotation
                ));
            }
        }
    }

    if !file_utils::is_dir_exist_sync(&output_path) {
        file_utils::create_dir_sync(&output_path);
    }

    let output_path = format!("{}/doc.md", output_path);

    file_utils::write_content_to_file(&output_path, &builder.string().unwrap())
        .await
        .unwrap();
}
