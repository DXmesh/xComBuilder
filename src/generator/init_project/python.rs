use std::collections::LinkedList;

use crate::{
    generator::{gen_sdk, serial::python},
    proto_utils,
};
use tracing::{error, info, warn};
use x_common_lib::{
    serial::api_descriptor::ServiceApiDescriptor,
    utils::{
        file_utils,
        string_utils::{self, extract_dxc_name_and_version_from_dxc_file_name},
    },
};
/**
 *
 */
async fn gen_lib_file(project_path: &str) {
    let lib_file_path = format!("{}/src/lib.py", project_path);

    if file_utils::is_file_exist(&lib_file_path) {
        return;
    }

    let is_succeed = file_utils::create_file(&lib_file_path).await;

    if !is_succeed {
        error!("创建文件 lib.py 失败...");
        return;
    }
    let lib_file = format!(
        r#"
import x_com_lib

x_com_lib.start()
"#,
    );

    file_utils::write_content_to_file(&lib_file_path, &lib_file)
        .await
        .unwrap();

    info!("生成 lib.py 文件成功...");
}

async fn gen_source_api_file(
    service_api_descriptor: &ServiceApiDescriptor,
    project_path: &str,
) -> bool {
    let source_api_file_path = format!("{}/src/x_com/source_api.py", project_path);

    let is_succeed = file_utils::create_file(&source_api_file_path).await;

    if !is_succeed {
        error!("创建文件 source_api.py 失败...");
        return false;
    }

    let mut builder = string_builder::Builder::new(1024 * 1024);

    python::gen_class_and_enum(service_api_descriptor, &mut builder);

    file_utils::write_content_to_file(&source_api_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 source_api.py 文件成功...");

    return true;
}

async fn gen_xport_core_file(
    _name: &str,
    service_api_descriptor: &ServiceApiDescriptor,
    project_path: &str,
) {
    let xport_core_file_path = format!("{}/src/x_com/xport_core.py", project_path);

    let is_succeed = file_utils::create_file(&xport_core_file_path).await;

    if !is_succeed {
        error!("创建文件 xport_core.py 失败...");
        return;
    }
    let mut builder = string_builder::Builder::new(1024 * 1024);
    //

    let xport_core_file = r#"import asyncio
import logging
import datetime
import builtins
import threading
import x_com_lib

class Context:
  request_id: int
  channel_id: int
  conn_id: int
  sender_service_key: x_com_lib.PyServiceKey
  from_addr: str

# 解析参数
class MillisecondFormatter(logging.Formatter):
    def format(self, record):
        s = super().format(record)
        old_time = self.formatTime(record, self.datefmt)
        new_time = datetime.datetime.fromtimestamp(record.created).strftime('%Y-%m-%d %H:%M:%S.{:03d}'.format(int(record.msecs)))
        return s.replace(old_time, new_time)

class XportLogHandler(logging.Handler):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    format_str = '%(asctime)s  %(levelname)s {} %(filename)s %(lineno)d: %(message)s'.format(x_com_lib.dxc_name())
    self.formatter = MillisecondFormatter(format_str)
  def emit(self, record):
    log_entry = self.format(record)
    x_com_lib.log.output(log_entry)


def init():
  TRACE_LEVEL_NUM = 5
  logging.addLevelName(TRACE_LEVEL_NUM, "TRACE")
  def trace(self, message, *args, **kws):
      if self.isEnabledFor(TRACE_LEVEL_NUM):
          self._log(TRACE_LEVEL_NUM, message, args, **kws)
  #
  logging.Logger.trace = trace
  logger = logging.getLogger('xport')
  log_level = x_com_lib.log.level().upper()
  # 
  if log_level == "TRACE":
    logger.setLevel(TRACE_LEVEL_NUM)
  elif log_level == "DEBUG":
    logger.setLevel(logging.DEBUG)
  elif log_level == "INFO":
    logger.setLevel(logging.INFO)
  elif log_level == "WARN":
    logger.setLevel(logging.WARNING)
  elif log_level == "ERROR": 
    logger.setLevel(logging.ERROR) 
  # 
  xport_log_handler = XportLogHandler()
  logger.addHandler(xport_log_handler)
  builtins.logger = logger
"#;
    builder.append(xport_core_file.as_bytes());

    if !service_api_descriptor.subscribe_topics.is_empty() {
        builder.append("  import subscribe\n");
        builder.append("  builtins.subscribe = subscribe\n");
    }

    let xport_core_file = r#"def get_logger() -> logging.Logger:
  return builtins.logger

def get_service():
  return builtins.SERVICE

def check_stop_event():
  if x_com_lib.is_exit() or builtins.is_exit:
    builtins.is_exit = True
    builtins.loop.stop()
  else:
    # 每秒检查一次
    builtins.loop.call_later(0.5, check_stop_event)

def run_loop_in_thread():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    builtins.loop = loop
    loop.call_soon(check_stop_event)
    loop.run_forever()

def dispatch_message(dxc_header, msg, param):
  builtins.loop.call_soon_threadsafe(lambda: builtins.loop.create_task(_inner_dispatch_message(dxc_header, msg, param)))

async def lanuch():
  builtins.is_exit = False
  thread = threading.Thread(target=run_loop_in_thread)
  thread.start()
  init() 
  try:
    await x_com_lib.init()
    while not builtins.is_exit:
      await asyncio.sleep(1) 
  except:
    builtins.is_exit = True

"#;
    builder.append(xport_core_file.as_bytes());
    builder.append("\n");
    builder.append("async def _inner_dispatch_message(dxc_header, msg, param):\n");
    builder.append("  ctx = Context()\n");
    builder.append("  ctx.request_id = dxc_header.request_id\n");
    builder.append("  ctx.channel_id = dxc_header.channel_id\n");
    builder.append("  ctx.conn_id = dxc_header.conn_id\n");
    builder.append("  if hasattr(dxc_header, \"from_addr\"):\n");
    builder.append("    ctx.from_addr = dxc_header.from_addr\n");
    builder.append("  ctx.sender_service_key = dxc_header.sender_key\n");

    let methods = service_api_descriptor.methods();

    builder.append("  try:\n");

    let mut space = 0;

    if !service_api_descriptor.subscribe_topics.is_empty() {
        space = 2;
    }

    if !service_api_descriptor.subscribe_topics.is_empty() {
        builder.append("      # 4 == MsgType::Stream\n");
        builder.append("      if dxc_header.msg_type == 4:\n");

        for subscribe_topic in service_api_descriptor.subscribe_topics.iter() {
            let (dxc_name, _) = if subscribe_topic.name.is_empty() {
                //
                (
                    service_api_descriptor.service_descriptor.name.clone(),
                    String::default(),
                )
            } else {
                extract_dxc_name_and_version_from_dxc_file_name(&subscribe_topic.name).unwrap()
            };
            let snake_case_dxc_name = string_utils::convert_to_snake_case(&dxc_name);

            builder.append(format!(
                "          if ctx.sender_service_key.dxc_name == \"{}\":\n",
                dxc_name
            ));
            for method in subscribe_topic.methods.iter() {
                let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

                builder.append(format!("            if msg == \"{}\":\n", method.name));

                let input_type = method.input_type.get_path();
                let is_param_empty = input_type.is_empty();

                if is_param_empty {
                    builder.append(format!(
                        "              await builtins.subscribe.{}.{}(ctx)\n",
                        snake_case_dxc_name, snake_case_method_name
                    ));
                } else {
                    builder.append(format!(
                        "              await builtins.subscribe.{}.{}(ctx, param)\n",
                        snake_case_dxc_name, snake_case_method_name
                    ));
                }
            }
        }
        builder.append("      elif msg == \"on_init\":\n");
    } else {
        builder.append("    if msg == \"on_init\":\n");
    }

    builder.append(" ".repeat(space));
    builder.append("      await builtins.SERVICE.on_init()\n");
    builder.append(" ".repeat(space));
    builder.append("      x_com_lib.xport.response_message(dxc_header.request_id, dxc_header.sender_key, msg, 0, \"\", None)\n");

    for method in methods {
        //
        builder.append(" ".repeat(space));
        builder.append(format!("    elif msg == \"{}\":\n", method.name));
        //
        let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

        let input_type = method.input_type.get_path();
        let is_param_empty = input_type.is_empty();

        if is_param_empty {
            builder.append(" ".repeat(space));
            builder.append(format!(
                "      ret = await builtins.SERVICE.{}(ctx)\n",
                snake_case_method_name
            ));
        } else {
            builder.append(" ".repeat(space));
            builder.append(format!(
                "      ret = await builtins.SERVICE.{}(ctx, param)\n",
                snake_case_method_name
            ));
        }
        builder.append(" ".repeat(space));
        builder.append("      x_com_lib.xport.response_message(dxc_header.request_id, dxc_header.sender_key, msg, 0, \"\", ret)\n");
    }
    builder.append(" ".repeat(space));
    builder.append("    elif msg == \"on_finalize\":\n");
    builder.append(" ".repeat(space));
    builder.append("      await builtins.SERVICE.on_finalize()\n");
    builder.append(" ".repeat(space));
    builder.append("      x_com_lib.xport.response_message(dxc_header.request_id, dxc_header.sender_key, msg, 0, \"\", None)\n");
    builder.append(" ".repeat(space));
    builder.append("    else:\n");
    builder.append(" ".repeat(space));
    builder.append("      builtins.logger.warn(\"无该 {} 消息的处理函数\".format(msg))\n");
    builder.append("  except Exception as e:\n");
    builder.append("    x_com_lib.xport.response_message(dxc_header.request_id, dxc_header.sender_key, msg, 1, str(e), None)\n");
    //

    file_utils::write_content_to_file(&xport_core_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 xport_core.py 文件成功...");
}
//
async fn gen_xport_init_file(
    _name: &str,
    _service_api_descriptor: &ServiceApiDescriptor,
    project_path: &str,
) {
    let xport_core_file_path = format!("{}/src/x_com/__init__.py", project_path);

    let default_value = String::default();

    file_utils::write_content_to_file(&xport_core_file_path, &default_value)
        .await
        .unwrap();

    info!("生成 __init__.py 文件成功...");
}

pub async fn gen_service_file(
    _name: &str,
    service_api_descriptor: &ServiceApiDescriptor,
    force: bool,
    project_path: &str,
) {
    let service_file_path = format!("{}/src/service.py", project_path);
    if !force {
        if file_utils::is_file_exist(&service_file_path) {
            warn!(
          "service.py 文件已经存在, 防止覆盖代码, 跳过该文件的生成, 可以使用 -f 选项强制初始化!"
      );
            return;
        }
    }

    let is_succeed = file_utils::create_file(&service_file_path).await;

    if !is_succeed {
        error!("创建文件 service.py 失败...");
        return;
    }

    let mut builder = string_builder::Builder::new(1024 * 1024);
    let service_file = r#"
from x_com import xport_core
from x_com import source_api
logger = xport_core.get_logger()

"#;

    builder.append(service_file.as_bytes());

    builder.append(format!(
        "class {}:\n",
        service_api_descriptor.service_name()
    ));
    builder.append(format!(
        r#"    
  async def on_init(self):
    logger.info("on_init...")

  async def on_finalize(self):
    logger.info("on_finalize...")
"#,
    ));
    builder.append("\n");

    let methods = service_api_descriptor.methods();

    // 生成路由
    for method in methods {
        builder.append(format!("  # {}\n", method.name));
        builder.append(" ".repeat(2));
        builder.append("async def ");
        let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);
        builder.append(snake_case_method_name.as_bytes());
        builder.append("(self, ctx: xport_core.Context");
        let input_type = method.input_type.get_path();
        let is_param_empty = input_type.is_empty();
        if !is_param_empty {
            builder.append(", ");
            builder.append(" param: source_api.");
            let python_input_type = proto_utils::protobuf_path_to_python(&input_type);
            builder.append(python_input_type.as_bytes());
        }
        builder.append(")");
        let output_type = method.output_type.get_path();

        let is_output_empty = output_type.is_empty();
        if !is_output_empty {
            builder.append(" -> source_api.");
            let python_output_type = proto_utils::protobuf_path_to_python(&output_type);
            builder.append(python_output_type.as_bytes());
        }
        builder.append(":\n");

        builder.append(" ".repeat(4));
        builder.append(format!(
            "raise NotImplementedError(\"未实现 {}\")",
            snake_case_method_name
        ));

        builder.append("\n\n");
    }

    file_utils::write_content_to_file(&service_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 service.py 文件成功...");
}

pub async fn gen_topic_file(service_api_descriptor: &ServiceApiDescriptor, project_path: &str) {
    let topic_file_path = format!("{}/src/x_com/topic.py", project_path);

    if service_api_descriptor.topic.is_none() {
        file_utils::remove_file_sync(&topic_file_path).expect("删除 topic.py 失败！");
        return;
    }

    let is_succeed = file_utils::create_file(&topic_file_path).await;

    if !is_succeed {
        error!("创建文件 topic.py 失败...");
        return;
    }
    //
    let topic = service_api_descriptor.topic.as_ref().unwrap();

    let mut builder = string_builder::Builder::new(1024 * 1024);

    builder.append("import x_com_lib\n");
    builder.append("from x_com import source_api\n");

    for method in topic.methods.iter() {
        builder.append(format!("# {}\n", method.name));
        let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

        let input_type = method.input_type.get_path();

        let is_param_empty = input_type.is_empty();

        if is_param_empty {
            builder.append(format!("def {}():\n", snake_case_method_name));
            builder.append(format!("  {}_with_tag(\"\")\n\n", snake_case_method_name));
        } else {
            let python_input_type = proto_utils::protobuf_path_to_python(&input_type);

            builder.append(format!(
                "def {}(param: source_api.{}):\n",
                snake_case_method_name, python_input_type
            ));

            builder.append(format!(
                "  {}_with_tag(param, \"\")\n\n",
                snake_case_method_name
            ));
        }

        if is_param_empty {
            builder.append(format!(
                "def {}_with_tag(tag: str):\n",
                snake_case_method_name
            ));
        } else {
            let python_input_type = proto_utils::protobuf_path_to_python(&input_type);
            builder.append(format!(
                "def {}_with_tag(param: source_api.{}, tag: str):\n",
                snake_case_method_name, python_input_type
            ));
        }

        builder.append("  publish_info = x_com_lib.PyPublishInfo()\n");
        builder.append(format!("  publish_info.topic = \"{}\"\n", method.name));
        builder.append("  publish_info.tag = tag\n");

        if is_param_empty {
            builder.append("  x_com_lib.stream.publish_message(publish_info, None)\n\n");
        } else {
            builder.append("  x_com_lib.stream.publish_message(publish_info, param)\n\n");
        }
    }

    file_utils::write_content_to_file(&topic_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 topic.py 文件成功...");
}
//

pub async fn gen_subscribe_file(
    service_api_descriptor: &ServiceApiDescriptor,
    force: bool,
    project_path: &str,
) {
    let subscribe_file_path = format!("{}/src/subscribe", project_path);

    if service_api_descriptor.subscribe_topics.is_empty() {
        file_utils::remove_all_dir_sync(subscribe_file_path).expect("删除 subscribe.rs 失败！");
        return;
    }

    if !force {
        if file_utils::is_dir_exist(&subscribe_file_path).await {
            warn!(
      "subscribe 目录已经存在, 防止覆盖代码, 跳过该目录的生成, 可以使用 -f 选项强制初始化!"
  );
            return;
        }
    }

    let is_succeed = file_utils::create_dir(&subscribe_file_path).await;

    if !is_succeed {
        error!("创建目录 subscribe 失败...");
        return;
    }

    let import_api_init_path = format!("{}/src/subscribe/__init__.py", &project_path);
    let is_succeed = file_utils::create_file(&import_api_init_path).await;

    if !is_succeed {
        error!("创建文件 subscribe/__init__.py 失败...");
        return;
    }
    let mut init_builder = string_builder::Builder::new(1024 * 1024);

    //
    for subscribe_topic in service_api_descriptor.subscribe_topics.iter() {
        let mut builder = string_builder::Builder::new(1024 * 1024);
        let (dxc_name, version) = if subscribe_topic.name.is_empty() {
            //
            (
                service_api_descriptor.service_descriptor.name.clone(),
                String::default(),
            )
        } else {
            extract_dxc_name_and_version_from_dxc_file_name(&subscribe_topic.name).unwrap()
        };

        let snake_case_subscribe_name = string_utils::convert_to_snake_case(&dxc_name);
        builder.append("from x_com import xport_core\n");
        let mut import_name = String::from("source_api");
        if subscribe_topic.name.is_empty() {
            builder.append("from x_com import source_api\n");
        } else {
            //

            // let snake_case_subscribe_name =
            //     string_utils::convert_to_snake_case(&subscribe_topic.name);


            let snake_case_subscribe_name =format!("{}_{}", snake_case_subscribe_name, version.replace(".", "_"));



            builder.append(format!(
                "from x_com.import_api import {}\n",
                snake_case_subscribe_name
            ));
            import_name = snake_case_subscribe_name;
        }

        for method in subscribe_topic.methods.iter() {
            builder.append(format!("# {}\n", method.name));

            let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

            builder.append(format!("async def {}", snake_case_method_name));

            builder.append("(ctx: xport_core.Context");

            let input_type = method.input_type.get_path();

            let is_param_empty = input_type.is_empty();

            if !is_param_empty {
                let python_input_type = proto_utils::protobuf_path_to_python(&input_type);

                builder.append(format!(", param: {}.{}", import_name, python_input_type));
            }

            builder.append("):\n");

            builder.append("  pass\n\n");
        }

        init_builder.append(format!("from . import {} \n", snake_case_subscribe_name));
        let file_path = format!("{}/{}.py", subscribe_file_path, snake_case_subscribe_name);
        file_utils::write_content_to_file(&file_path, &builder.string().unwrap())
            .await
            .unwrap();

        info!(
            "生成 subscribe/{}.py 文件成功...",
            snake_case_subscribe_name
        );
    }
    file_utils::write_content_to_file(&import_api_init_path, &init_builder.string().unwrap())
        .await
        .unwrap();
}

pub(super) async fn gen_project(
    project_path: String,
    service_name: String,
    service_api_descriptor: ServiceApiDescriptor,
    dxc_version: String,
    file_list: LinkedList<String>,
    proto_file_path: String,
    use_self: bool,
    force: bool,
) -> bool {
    gen_lib_file(&project_path).await;
    //
    gen_xport_core_file(&service_name, &service_api_descriptor, &project_path).await;

    gen_xport_init_file(&service_name, &service_api_descriptor, &project_path).await;

    gen_service_file(&service_name, &service_api_descriptor, force, &project_path).await;

    gen_topic_file(&service_api_descriptor, &project_path).await;

    let is_succeed = gen_source_api_file(&service_api_descriptor, &project_path).await;
    if !is_succeed {
        return false;
    }
    //
    let sdk_file_path = format!("{}/protos", &project_path);
    let mut init_builder = string_builder::Builder::new(1024 * 1024);
    for filename in file_list {
        if !filename.starts_with("import-api-") {
            continue;
        }
        //
        let proto_file_path = format!("{}/{}", &sdk_file_path, &filename);
        let is_succeed =
            gen_sdk::python::gen(filename, proto_file_path, &project_path, &mut init_builder).await;
        if !is_succeed {
            return false;
        }
    }
    if use_self {
        let snake_case_service_name =
            string_utils::convert_to_snake_case(&service_api_descriptor.service_name());

        let dxc_version = dxc_version.replace(".", "_");

        let output_path = format!(
            "{}/src/x_com/import_api/{}_{}.py",
            &project_path, snake_case_service_name, dxc_version,
        );

        let is_succeed = gen_sdk::python::gen_with_dxc_name_and_version(
            proto_file_path,
            output_path,
            &project_path,
            &dxc_version,
            true,
        )
        .await;
        if !is_succeed {
            return false;
        }

        init_builder.append(format!(
            "from . import {}_{}\n",
            snake_case_service_name, dxc_version
        ));
    }

    gen_subscribe_file(&service_api_descriptor, force, &project_path).await;

    // 创建 import_api __init__
    let import_api_init_path = format!("{}/src/x_com/import_api/__init__.py", &project_path);

    file_utils::create_file(&import_api_init_path).await;

    file_utils::write_content_to_file(&import_api_init_path, &init_builder.string().unwrap())
        .await
        .unwrap();

    true
}
