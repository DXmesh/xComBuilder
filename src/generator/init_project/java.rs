use string_builder::Builder;
use tracing::{error, info, warn};
use x_common_lib::{
    serial::api_descriptor::ServiceApiDescriptor,
    utils::{file_utils, string_utils},
};

use crate::{
    generator::{gen_sdk, serial::java},
    proto_utils,
};

async fn gen_lib_file(project_path: &str) {
    let lib_file_path = format!("{}/src/main/java/io/dxmesh/Lib.java", project_path);

    if file_utils::is_file_exist(&lib_file_path) {
        return;
    }

    let is_succeed = file_utils::create_file(&lib_file_path).await;

    if !is_succeed {
        error!("创建文件 Lib.java 失败...");
        return;
    }
    let lib_file = format!(
        r#"
package io.dxmesh;
import io.dxmesh.x_com.ServiceWrapper;
import org.apache.commons.cli.ParseException;
import java.io.IOException;

public class Lib {{
    public static void main(String[] args) throws ParseException {{
        XApi.init(args);
        XApi.start(new ServiceWrapper());
    }}
}}
"#,
    );

    file_utils::write_content_to_file(&lib_file_path, &lib_file)
        .await
        .unwrap();

    info!("生成 lib.java 文件成功...");
}

async fn gen_source_api_file(
    service_api_descriptor: &ServiceApiDescriptor,
    project_path: &str,
) -> bool {
    let dest_path = format!("{}/src/main/java/io/dxmesh/x_com/source_api", project_path);

    java::gen_class_and_enum(
        service_api_descriptor,
        "io.dxmesh.x_com.source_api",
        &dest_path,
    );
    true
}

async fn gen_service_file(
    service_api_descriptor: &ServiceApiDescriptor,
    force: bool,
    project_path: &str,
) {
    let service_name = service_api_descriptor.service_name();

    let service_file_path = format!(
        "{}/src/main/java/io/dxmesh/{}.java",
        project_path, service_name
    );

    if !force {
        if file_utils::is_file_exist(&service_file_path) {
            warn!(
                "{}.java 文件已经存在, 防止覆盖代码, 跳过该文件的生成, 可以使用 -f 选项强制初始化!",
                service_name
            );
            return;
        }
    }

    let is_succeed = file_utils::create_file(&service_file_path).await;

    if !is_succeed {
        error!("创建文件 Lib.java 失败...");
        return;
    }

    let mut builder = Builder::new(1024 * 1024);

    let service_file = format!(
        r#"
package io.dxmesh;
import io.dxmesh.base.XportLogger;
import io.dxmesh.x_com.source_api.*;
import org.apache.logging.log4j.Logger;
"#
    );

    builder.append(service_file);

    builder.append(format!(
        "public class {} extends XPortService {{\n",
        service_name
    ));

    builder.append(format!(
        "private static final Logger logger = XportLogger.getLogger({}.class);\n",
        service_name
    ));

    builder.append(
        r#"
    @Override
    public void onInit() {
        // todo
    }

    @Override
    public void onFinalize() {
        // todo
    }
    "#,
    );

    let methods = service_api_descriptor.methods();
    for method in methods {
        let java_method_name = string_utils::convert_to_small_camel_case(&method.name);

        let input_type = method.input_type.get_path();

        let is_param_empty = input_type.is_empty();

        let output_type = method.output_type.get_path();
        let is_output_empty = output_type.is_empty();

        let java_output_type = if is_output_empty {
            String::from("void")
        } else {
            proto_utils::protobuf_path_to_java(&output_type)
        };

        builder.append(format!("public {} {}(", java_output_type, java_method_name));

        if !is_param_empty {
            let java_input_type = proto_utils::protobuf_path_to_java(&input_type);
            builder.append(format!("{} param", java_input_type))
        }
        builder.append(") {\n");

        if !is_output_empty {
            builder.append(format!("return new {}();\n", java_output_type));
        }
        builder.append("}\n");
    }

    builder.append(format!("}}\n"));

    file_utils::write_content_to_file(&service_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 {}.java 文件成功...", service_name);
}

async fn gen_pom_file(
    service_api_descriptor: &ServiceApiDescriptor,
    project_path: &str,
    dxc_version: &str,
) {
    let service_name = service_api_descriptor.service_name();

    let service_file_path = format!("{}/pom.xml", project_path);

    let mut builder = Builder::new(1024 * 1024);

    let jar_name = string_utils::convert_to_snake_case(service_name).replace("_service", "");

    builder.append(format!(r#"<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>io.dxmesh</groupId>
    <artifactId>{}</artifactId>
    <version>{}</version>
    <properties>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>
    <dependencies>
        <dependency>
            <groupId>io.dxmesh</groupId>
            <artifactId>xComJavaLib</artifactId>
            <version>0.0.1</version>
        </dependency>
    </dependencies>
    <build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-dependency-plugin</artifactId>
            <version>3.2.0</version>
            <executions>
                <execution>
                    <id>copy-dependencies</id>
                    <phase>package</phase>
                    <goals>
                        <goal>copy-dependencies</goal>
                    </goals>
                    <configuration>
                        <outputDirectory>${{project.build.directory}}/lib</outputDirectory>
                    </configuration>
                </execution>
            </executions>
        </plugin>

        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jar-plugin</artifactId>
            <version>3.2.0</version>
            <configuration>
                <archive>
                    <manifest>
                        <addClasspath>true</addClasspath>
                        <classpathPrefix>lib/</classpathPrefix>
                        <mainClass>io.dxmesh.Lib</mainClass>
                    </manifest>
                </archive>
            </configuration>
        </plugin>
    </plugins>
</build>

</project>
    "#, jar_name, dxc_version));

    file_utils::write_content_to_file(&service_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 pom.xml 文件成功...");
}

async fn gen_service_wrapper(service_api_descriptor: &ServiceApiDescriptor, project_path: &str) {
    let service_name = service_api_descriptor.service_name();

    let service_file_path = format!(
        "{}/src/main/java/io/dxmesh/x_com/ServiceWrapper.java",
        project_path
    );

    let mut builder = string_builder::Builder::new(1024 * 1024);

    builder.append(format!(
        r#"
package io.dxmesh.x_com;
import com.google.protobuf.CodedInputStream;
import io.dxmesh.Context;
import io.dxmesh.{};
import io.dxmesh.IService;
import io.dxmesh.api.Response;
import io.dxmesh.serial.DXCProtocolHeader;
import io.dxmesh.serial.Status;
import io.dxmesh.x_com.source_api.Hello;
import io.dxmesh.x_com.source_api.HelloReply;

import java.io.IOException;
import java.nio.ByteBuffer;
    "#,
        service_name
    ));

    builder.append("public class ServiceWrapper implements IService { \n");

    let service_name = service_api_descriptor.service_name();

    builder.append(format!(
        "private final {} service = new {}(); \n",
        service_name, service_name
    ));

    builder.append(
        r#"
    @Override
    public void onInit() {
        service.onInit();
    }
    @Override
    public void onFinalize() {
        service.onFinalize();
    }
    @Override
    public Response dispatchMessage(DXCProtocolHeader dxcMsgHeader, ByteBuffer msgBody) {
"#,
    );

    builder.append(
        r#"
    Response response = new Response();
    try {
        Context ctx = new Context();
        ctx.setChannelId(dxcMsgHeader.getChannelId());
        ctx.setFromAddr(dxcMsgHeader.getFromAddress());
        ctx.setRequestId(dxcMsgHeader.getRequestId());
        ctx.setSenderServiceKey(dxcMsgHeader.getSenderKey());
        service.setContext(ctx);
        CodedInputStream inputStream = CodedInputStream.newInstance(msgBody);
        inputStream.readTag();
        String message = inputStream.readString();
"#,
    );

    let methods = service_api_descriptor.methods();

    let mut is_first_method = true;
    for method in methods {
        let java_method_name = string_utils::convert_to_small_camel_case(&method.name);

        if is_first_method {
            builder.append(format!("if (\"{}\".equals(message)) {{ \n", method.name));
            is_first_method = false;
        } else {
            builder.append(format!(
                "else if (\"{}\".equals(message)) {{ \n",
                method.name
            ));
        }

        let input_type = method.input_type.get_path();

        let is_param_empty = input_type.is_empty();

        if !is_param_empty {
            let java_input_type = proto_utils::protobuf_path_to_java(&input_type);

            builder.append(format!(
                " {} param = new {}(); \n",
                java_input_type, java_input_type
            ));

            builder.append("param.parseFromInputStream(inputStream);\n");
        }

        let output_type = method.output_type.get_path();
        let is_output_empty = output_type.is_empty();

        if !is_output_empty {
            let java_output_type = proto_utils::protobuf_path_to_java(&output_type);

            if !is_param_empty {
                builder.append(format!(
                    "{} reply = service.{}(param);\n",
                    java_output_type, java_method_name
                ));
            } else {
                builder.append(format!(
                    "{} reply = service.{}();\n",
                    java_output_type, java_method_name
                ));
            }

            builder.append("response.setRequestMessage(reply);\n");
        } else {
            if !is_param_empty {
                builder.append(format!("service.{}(param);\n", java_method_name));
            } else {
                builder.append(format!("service.{}();\n", java_method_name));
            }
        }
        builder.append("response.setStatus(Status.Success());\n");

        builder.append(format!("response.setRespMsg(\"{}Rsp\");\n", method.name));

        builder.append("} ");
    }

    if !methods.is_empty() {
        builder.append("else { \n");
        // 写入 not found的代码
        builder.append("response.setStatus(Status.Error( \"没有 \" + message + \" 处理接口\"));");

        builder.append("}\n");
    }

    builder.append(
        r#"
    } catch (IOException e) {
        response.setStatus(Status.Error(e.getMessage()));
    } finally {
        service.removeContext();
    }
    "#,
    );

    builder.append("return response;\n");

    builder.append("}\n");

    builder.append("}\n");

    file_utils::create_file_path_sync(&service_file_path);

    file_utils::write_content_to_file(&service_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 ServiceWrapper.java 文件成功...");
}

pub async fn gen_project(
    project_path: String,
    _service_name: String,
    service_api_descriptor: ServiceApiDescriptor,
    dxc_version: String,
    use_self: bool,
    force: bool,
) -> bool {
    gen_lib_file(&project_path).await;
    //
    gen_service_file(&service_api_descriptor, force, &project_path).await;

    gen_pom_file(&service_api_descriptor, &project_path, &dxc_version).await;

    gen_service_wrapper(&service_api_descriptor, &project_path).await;

    //
    let is_succeed = gen_source_api_file(&service_api_descriptor, &project_path).await;
    //
    if !is_succeed {
        return false;
    }

    let sdk_file_path = format!("{}/protos", project_path);

    let source_api_file_path = format!("{}/src/main/java/io/dxmesh/x_com/import_api", project_path);

    let dxc_name = format!(
        "{}_{}",
        service_api_descriptor.service_name(),
        dxc_version.replace(".", "_")
    );

    gen_sdk::java::gen(
        sdk_file_path,
        "component".to_string(),
        source_api_file_path,
        use_self,
        Some(&service_api_descriptor),
        Some(&dxc_name),
        Some(&dxc_version),
    )
    .await;

    true
}
