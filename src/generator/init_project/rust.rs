use crate::generator::ServiceApiDescriptor;
use crate::generator::{gen_sdk, serial::rust}; //
use crate::{file_utils, proto_utils};
use quote::quote;
use regex::Regex;
use syn::{Item, ItemMod};
use tracing::{error, info, warn};
use x_common_lib::utils::string_utils::{self, extract_dxc_name_and_version_from_dxc_file_name};

async fn gen_xcom_mod_file(project_path: &str, service_api_descriptor: &ServiceApiDescriptor) {
    let mod_file_path = format!("{}/src/x_com/mod.rs", project_path);

    let is_succeed = file_utils::create_file(&mod_file_path).await;

    if !is_succeed {
        error!("创建文件 mod.rs 失败...");
        return;
    }

    let mut builder = string_builder::Builder::new(1024 * 1024);

    builder.append("pub mod xport_core;\n");
    builder.append("pub mod import_api;\n");
    builder.append("pub mod source_api;\n");

    if service_api_descriptor.topic.is_some() {
        builder.append("pub mod topic;\n");
    }

    file_utils::write_content_to_file(&mod_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 mod.rs 文件成功...");
}
//

fn generate_code_with_line(syntax_tree: &syn::File) -> String {
    let mut code_with_newlines = String::new();
    let re = Regex::new(r"mod\s+([a-zA-Z_][a-zAT0-9_]*)\s+;").unwrap();
    for item in &syntax_tree.items {
        let item_code = quote! {#item}.to_string();
        let item_code = re.replace_all(&item_code, "mod $1;");
        code_with_newlines.push_str(&item_code);
        code_with_newlines.push('\n'); // 添加换行符
    }
    code_with_newlines
}

/**
 *
 */
async fn gen_lib_file(project_path: &str, service_api_descriptor: &ServiceApiDescriptor) {
    let lib_file_path = format!("{}/src/lib.rs", project_path);

    let mut has_event = false;
    let mut has_subscribe = false;

    if file_utils::is_file_exist(&lib_file_path) {
        let file_content = file_utils::read_file_as_string(&lib_file_path)
            .await
            .expect("读取 lib.rs 失败！");

        let mut syntax = syn::parse_file(&file_content).expect("lib.rs 语法出错！");

        syntax.items.retain(|item| {
            if let Item::Mod(item_mod) = item {
                let ident_name = item_mod.ident.to_string();
                match ident_name.as_str() {
                    "event" => {
                        has_event = true;
                        return !service_api_descriptor.events.is_empty();
                    }
                    "subscribe" => {
                        has_subscribe = true;
                        return !service_api_descriptor.subscribe_topics.is_empty();
                    }
                    _ => {}
                }
            }
            true
        });
        //
        //
        if !service_api_descriptor.events.is_empty() && !has_event {
            //
            let event_mod: ItemMod = syn::parse_quote! {
              mod event;
            };
            syntax.items.push(Item::Mod(event_mod));
        }

        if !service_api_descriptor.subscribe_topics.is_empty() && !has_subscribe {
            let subscribe_mod: ItemMod = syn::parse_quote! {
              mod subscribe;
            };
            syntax.items.push(Item::Mod(subscribe_mod));
        }

        let new_code = generate_code_with_line(&syntax);

        file_utils::write_content_to_file(&lib_file_path, &new_code)
            .await
            .unwrap();
        info!("生成 lib.rs 文件成功...");

        return;
        //
    }

    let is_succeed = file_utils::create_file(&lib_file_path).await;

    if !is_succeed {
        error!("创建文件 lib.rs 失败...");
        return;
    }

    let mut builder = string_builder::Builder::new(1024 * 1024);

    builder.append("mod service;\n");
    builder.append("mod x_com;\n");

    if !service_api_descriptor.events.is_empty() {
        builder.append("mod event;\n");
    }

    if !service_api_descriptor.subscribe_topics.is_empty() {
        builder.append("mod subscribe;\n");
    }

    file_utils::write_content_to_file(&lib_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 lib.rs 文件成功...");
}

/**
 *
 */
async fn gen_source_api_file(
    service_api_descriptor: &ServiceApiDescriptor,
    project_path: &str,
) -> bool {
    let source_api_file_path = format!("{}/src/x_com/source_api.rs", project_path);

    let is_succeed = file_utils::create_file(&source_api_file_path).await;

    if !is_succeed {
        error!("创建文件 source_api.rs 失败...");
        return false;
    }

    let mut builder = string_builder::Builder::new(1024 * 1024);

    rust::gen_struct_and_enum(service_api_descriptor, false, false, &mut builder);

    rust::gen_serial(service_api_descriptor, &mut builder);

    file_utils::write_content_to_file(&source_api_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 source_api.rs 文件成功...");

    return true;
}

/**
 *
 */
async fn gen_cargo_toml(name: &str, project_path: &str, dxc_version: &str) {
    let cargo_toml_file_path = format!("{}/Cargo.toml", project_path);

    if file_utils::is_file_exist(&cargo_toml_file_path) {
        return;
    }

    let is_succeed = file_utils::create_file(&cargo_toml_file_path).await;

    if !is_succeed {
        error!("创建文件 Cargo.toml 失败...");
        return;
    }

    let snake_name = string_utils::convert_to_snake_case(name);

    let cargo_toml = format!(
        r#"
[package]
name = "{}"
version = "{}"
edition = "2021"

[lib]
name = "{}"
path = "src/lib.rs"
crate-type=["cdylib"]

[dependencies]
# futures="0.3.28"
x-com-lib = {{path="../xComRustLib"}}"#,
        name, dxc_version, snake_name
    );

    file_utils::write_content_to_file(&cargo_toml_file_path, &cargo_toml)
        .await
        .unwrap();

    info!("生成 Cargo.toml 文件成功...");
}

//
async fn gen_xport_core_file(
    name: &str,
    service_api_descriptor: &ServiceApiDescriptor,
    project_path: &str,
) {
    let xport_core_file_path = format!("{}/src/x_com/xport_core.rs", project_path);

    let is_succeed = file_utils::create_file(&xport_core_file_path).await;

    if !is_succeed {
        error!("创建文件 xport_core.rs 失败...");
        return;
    }
    let mut builder = string_builder::Builder::new(1024 * 1024);

    builder.append("#![allow(unused_imports)]\n");
    builder.append("#![allow(static_mut_refs)]\n");

    let xport_core_file = format!(
        r#"
use crate::service::{};
use core::slice;
use x_com_lib::x_core;
use x_com_lib::x_core::xrpc;
use x_com_lib::x_core::parse_request_param;
use x_com_lib::x_core::response_msg;
use x_com_lib::x_core::response_empty_msg;
use x_com_lib::CodedInputStream;
use x_com_lib::x_core::gen_id;
use x_com_lib::x_core::set_request_id;
use x_com_lib::Status;
use x_com_lib::x_core::{{init_runtime, get_runtime,take_runtime}};
use x_com_lib::ProtocolDXCReader;
"#,
        name
    );
    if !service_api_descriptor.events.is_empty() {
        builder.append("use crate::event::XportEvent;\n");
        builder.append("use x_com_lib::x_api::xport::event;\n");
    }

    let has_topic = !service_api_descriptor.subscribe_topics.is_empty();

    if has_topic {
        builder.append("use crate::subscribe::*;\n");
    }

    builder.append(xport_core_file.as_bytes());

    builder.append(format!("pub static mut SERVICE: Option<Box<{}>> = None;\n", name).as_bytes());

    builder.append(
        format!(
            "pub fn get_service() -> &'static {} {{
        unsafe {{ SERVICE.as_ref().unwrap() }}
    }}\n",
            name
        )
        .as_bytes(),
    );

    if !service_api_descriptor.events.is_empty() {
        builder.append("pub static mut EVENT: Option<Box<XportEvent>> = None;\n");

        builder.append(
            "pub fn get_event() -> &'static XportEvent {
          unsafe { EVENT.as_ref().unwrap() }
      }\n",
        );

        builder.append("fn subscribe_event() {\n");

        let event = service_api_descriptor.events.get(0).unwrap();

        let local_service_on_event_name = String::from("LocalServiceOn");
        // &local_service_on_event_name
        if event.event_items.iter().any(|item| item.name == local_service_on_event_name) {
            //

            builder.append(
                "    event::subscribe_local_service_on(Box::new(|_ctx, service_info| {
              x_core::spawn(async move {
                  let event = get_event();
                  event.local_service_on(_ctx, service_info).await;
              });
          }));\n",
            );
        }

        //
        let look_for_dxc_event_name = String::from("LookForDXC");

        // if event.event_items.contains(&look_for_dxc_event_name) {
          if event.event_items.iter().any(|item| item.name == look_for_dxc_event_name) {
            //

            builder.append(
                "    event::subscribe_look_for_dxc(Box::new(|_ctx, service_key| {
              x_core::spawn(async move {
                  let event = get_event();
                  event.look_for_dxc(_ctx, service_key).await;
              });
          }));\n",
            );
        }

        let local_service_off_event_name = String::from("LocalServiceOff");

        if event.event_items.iter().any(|item| item.name == local_service_off_event_name) {
        // if event.event_items.contains(&local_service_off_event_name) {
            //
            builder.append(
                "    event::subscribe_local_service_off(Box::new(|_ctx, service_info| {
              x_core::spawn(async move {
                  let event = get_event();
                  event.local_service_off(_ctx, service_info).await;
              });
          }));\n",
            );
        }

        let ipc_disconnect_event_name = String::from("IPCDisconnected");

        if event.event_items.iter().any(|item| item.name == ipc_disconnect_event_name) {
        // if event.event_items.contains(&channel_connected_event_name) {
            builder.append(
                "    event::subscribe_ipc_disconnected(Box::new(|_ctx, channel_event| {
            x_core::spawn(async move {
                let event = get_event();
                event.ipc_disconnected(_ctx, channel_event).await;
            });
        }));\n",
            );
        }


        let channel_connected_event_name = String::from("ChannelConnected");

        if event.event_items.iter().any(|item| item.name == channel_connected_event_name) {
        // if event.event_items.contains(&channel_connected_event_name) {
            builder.append(
                "    event::subscribe_channel_connected(Box::new(|_ctx, channel_event| {
            x_core::spawn(async move {
                let event = get_event();
                event.channel_connected(_ctx, channel_event).await;
            });
        }));\n",
            );
        }

        let channel_disconnected_event_name = String::from("ChannelDisconnected");

        // if event.event_items.contains(&channel_disconnected_event_name) {
          if event.event_items.iter().any(|item| item.name == channel_disconnected_event_name) {
            builder.append(
                "    event::subscribe_channel_disconnected(Box::new(|_ctx, channel_event| {
              x_core::spawn(async move {
                  let event = get_event();
                  event.channel_disconnected(_ctx, channel_event).await;
              });
          }));\n",
            );
        }

        let message_in_event_name = String::from("MessageIn");

        // if event.event_items.contains(&message_in_event_name) {
          if event.event_items.iter().any(|item| item.name == message_in_event_name) {
            builder.append(
                "    event::subscribe_message_in(Box::new(|_ctx, vertify_info| {
              x_core::spawn(async move {
                  let event = get_event();
                  event.message_in(_ctx, vertify_info).await;
              });
              true
          }));\n",
            );
        }

        builder.append("}\n");
    }

    let xport_core_file = if service_api_descriptor.events.is_empty() {
        format!(
            r#"
#[no_mangle]
pub extern "C" fn init(service_id: i64, config: *const u8, config_len: u32, log_level: i32) {{
    init_runtime();
    let runtime = get_runtime();
    // 初始化日志
    runtime.block_on(async {{
      let request_id = gen_id();
      set_request_id(request_id);
    let config_str = unsafe {{
        let buffer =
        slice::from_raw_parts(config as *mut u8, config_len as usize);
        std::str::from_utf8_unchecked(buffer)
    }};
    x_core::init_app(service_id, "{}", &config_str, log_level);
    // 加载服务
    let service_ins = Box::new({}::new());
    unsafe {{
      SERVICE = Some(service_ins);
      let ret = SERVICE.as_mut().unwrap().on_init().await;
      if ret.is_err() {{
        response_empty_msg("", 0, &ret);
        return;
      }}
    }}
    set_request_id(0);
    let ok = Ok(());
    response_empty_msg("", 0, &ok);
  }});
}}"#,
            name, name
        )
    } else {
        format!(
            r#"
#[no_mangle]
pub extern "C" fn init(service_id: i64, config: *const u8, config_len: u32, log_level: i32) {{
  init_runtime();
  let runtime = get_runtime();
  // 初始化日志
  runtime.block_on(async {{
    let request_id = gen_id();
    set_request_id(request_id);
  let config_str = unsafe {{
      let buffer =
      slice::from_raw_parts(config as *mut u8, config_len as usize);
      std::str::from_utf8_unchecked(buffer)
  }};
  x_core::init_app(service_id, "{}", &config_str, log_level);
  // 加载服务
  let service_ins = Box::new({}::new());
  let event_ins = Box::new(XportEvent::new());
  unsafe {{
    SERVICE = Some(service_ins);
    EVENT = Some(event_ins);
    let ret = SERVICE.as_mut().unwrap().on_init().await;
    if ret.is_err() {{
      response_empty_msg("", 0, &ret);
      return;
    }}
    
    subscribe_event();
  }}
  set_request_id(0);
  let ok = Ok(());
  response_empty_msg("", 0, &ok);
}});
}}"#,
            name, name
        )
    };

    builder.append(xport_core_file.as_bytes());

    builder.append(
        r#"
    #[no_mangle]
    pub extern "C" fn finalize() {
      let runtime = take_runtime();
      runtime.block_on(async {
        let request_id = gen_id();
        set_request_id(request_id);
        unsafe {
            let service = SERVICE.take();
            service.unwrap().on_finalize().await;
        }
        set_request_id(0);
      });
    }
    "#,
    );
    //

    builder.append("#[no_mangle]\n");

    builder.append("pub extern \"C\" fn dispatch_message(buffer: *const u8, buffer_len: u32) {\n");

    builder.append("  let vec_buffer = unsafe { slice::from_raw_parts(buffer as *mut u8, buffer_len as usize) };\n");

    builder.append(
        format!(
            r#"
      let dxc_msg_reader = ProtocolDXCReader::new(vec_buffer);
      let msg_header = dxc_msg_reader.header();
      let msg_body = dxc_msg_reader.msg_body();
      let ctx = xrpc::Context {{
          sender_service_key: msg_header.sender_key,
          channel_id: msg_header.channel_id,
          conn_id: msg_header.conn_id,
          request_id: msg_header.request_id,
          from_addr: msg_header.from_address
      }};
    "#
        )
        .as_bytes(),
    );
    builder.append("  let mut input_stream = CodedInputStream::from_bytes(msg_body);\n");
    builder.append("  let _tag = input_stream.read_raw_tag_or_eof();\n");

    builder.append("  if let Err(_err) = _tag {\n");
    builder
        .append("      let err_status = Err(Status::error(String::from(\"数据格式出错！\")));\n");
    builder.append("      response_empty_msg(\"DATA_ERR\", msg_header.request_id, &err_status);\n");
    builder.append("      return;\n");
    builder.append("  }\n");

    builder.append("  let message = input_stream.read_string();\n");

    builder.append("  if let Err(_err) = message {\n");
    builder.append(
        "      let err_status = Err(Status::error(String::from(\"读取消息名称出错！\")));\n",
    );
    builder.append("      response_empty_msg(\"DATA_ERR\", msg_header.request_id, &err_status);\n");
    builder.append("      return;\n");
    builder.append("  }\n");

    builder.append("   let message = message.unwrap();\n");

    if has_topic {
        builder.append("   // 4 == MsgType::Stream\n");
        builder.append("   if msg_header.msg_type == 4 {\n");
        builder.append("        match ctx.sender_service_key.dxc_name.as_str() {\n");
        //

        for subscribe_topic in service_api_descriptor.subscribe_topics.iter() {
            let (dxc_name, _) = if subscribe_topic.name.is_empty() {
                //
                (
                    service_api_descriptor.service_descriptor.name.clone(),
                    String::default(),
                )
            } else {
                extract_dxc_name_and_version_from_dxc_file_name(&subscribe_topic.name).unwrap()
            };

            builder.append(format!("   \"{}\"=> {{ \n", dxc_name));

            builder.append("  match message.as_str() {\n");

            for method in subscribe_topic.methods.iter() {
                builder.append(format!("   \"{}\"=> {{ \n", method.name));
                //
                let input_type = method.input_type.get_path();
                let is_param_empty = input_type.is_empty();

                if !is_param_empty {
                    builder.append("let param = parse_request_param(&mut input_stream);\n");
                }

                builder.append("x_core::spawn_stream(async move {\n");

                let snake_subscribe_name = string_utils::convert_to_snake_case(&dxc_name);

                let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

                if is_param_empty {
                    builder.append(format!(
                        "{}::{}(ctx).await;\n",
                        snake_subscribe_name, snake_case_method_name
                    ));
                } else {
                    builder.append(format!(
                        "{}::{}(ctx, param).await;\n",
                        snake_subscribe_name, snake_case_method_name
                    ));
                }

                builder.append("});\n");

                builder.append("}\n");
            }

            builder.append("            _ => {}\n");
            builder.append("}\n");

            builder.append("}\n");
        }
        //
        builder.append("            _ => {}\n");
        builder.append("        }\n");

        builder.append("    } else {\n");
    }

    builder.append("  match message.as_str() {\n");
    let methods = service_api_descriptor.methods();
    // 生成路由
    for method in methods {
        builder.append(" ".repeat(4));

        builder.append("\"");
        builder.append(method.name.clone());
        builder.append("\" => { \n");
        builder.append(" ".repeat(8));
        let input_type = method.input_type.get_path();

        let is_param_empty = input_type.is_empty();
        if !is_param_empty {
            builder.append("let param = parse_request_param(&mut input_stream);\n");
        }

        builder.append("x_core::spawn(async move {\n");

        builder.append(" ".repeat(10));

        builder.append("let service = get_service();\n");

        builder.append(" ".repeat(10));

        builder.append("let result = service.");

        let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

        builder.append(snake_case_method_name);
        if is_param_empty {
            builder.append("(ctx).await;\n");
        } else {
            builder.append("(ctx, param).await;\n");
        }
        builder.append(" ".repeat(10));
        let output_type = method.output_type.get_path();
        let is_output_empty = output_type.is_empty();
        if is_output_empty {
            builder.append("response_empty_msg(\"");
            builder.append(method.name.clone());
            builder.append("Rsp\", msg_header.request_id, &result);\n");
        } else {
            builder.append("response_msg(\"");
            builder.append(method.name.clone());
            builder.append("Rsp\", msg_header.request_id, &result);\n");
        }

        builder.append(" ".repeat(8));
        builder.append("});\n");

        builder.append(" ".repeat(4));
        builder.append("}\n");
    }

    //
    builder.append(" ".repeat(4));
    builder.append("_ => {\n");

    builder.append("let err_status = Err(Status::error(format!(\"{} 不存在！\", message)));\n");

    builder.append("response_empty_msg(\"NOT_FOUND\", msg_header.request_id, &err_status);\n");

    builder.append("}\n");

    builder.append("  }\n");
    if has_topic {
        builder.append("}\n");
    }

    builder.append("}\n");

    file_utils::write_content_to_file(&xport_core_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 xport_core.rs 文件成功...");
}
//
//
pub async fn gen_service_file(
    name: &str,
    service_api_descriptor: &ServiceApiDescriptor,
    force: bool,
    project_path: &str,
) {
    let service_file_path = format!("{}/src/service.rs", project_path);

    if !force {
        if file_utils::is_file_exist(&service_file_path) {
            warn!(
          "service.rs 文件已经存在, 防止覆盖代码, 跳过该文件的生成, 可以使用 -f 选项强制初始化!"
      );
            return;
        }
    }

    let is_succeed = file_utils::create_file(&service_file_path).await;

    if !is_succeed {
        error!("创建文件 service.rs 失败...");
        return;
    }

    let mut builder = string_builder::Builder::new(1024 * 1024);
    let service_file = format!(
        r#"
use x_com_lib::x_core;
use x_com_lib::x_core::xrpc::Context;
use crate::x_com::{{import_api::*, source_api::*}};

pub struct {} {{}}

impl {} {{
    pub fn new() -> {} {{
      {} {{}}
    }}
"#,
        name, name, name, name
    );

    builder.append(service_file.as_bytes());

    builder.append(format!(
        r#"    
  pub async fn on_init(&mut self) -> x_core::Result<()> {{
    Ok(())
  }}

  pub async fn on_finalize(&self) {{

  }}
  "#,
    ));

    builder.append("\n");

    let methods = service_api_descriptor.methods();

    // 生成路由
    for method in methods {
        builder.append(format!("  // {}\n", method.name));

        builder.append(" ".repeat(2));

        builder.append("pub async fn ");

        let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

        builder.append(snake_case_method_name.as_bytes());

        builder.append("(&self, _ctx: Context");

        let input_type = method.input_type.get_path();

        let is_param_empty = input_type.is_empty();

        if !is_param_empty {
            builder.append(", ");
            builder.append(" _param: ");
            let rust_input_type =
                format!("Box<{}>", proto_utils::protobuf_path_to_rust(&input_type));
            builder.append(rust_input_type.as_bytes());
        }
        builder.append(")");
        let output_type = method.output_type.get_path();
        builder.append(" -> x_core::Result<");
        let is_output_empty = output_type.is_empty();
        if is_output_empty {
            builder.append("()");
        } else {
            let rust_output_type =
                format!("Box<{}>", proto_utils::protobuf_path_to_rust(&output_type));
            builder.append(rust_output_type.as_bytes());
        }
        builder.append("> {\n");
        builder.append(" ".repeat(4));
        builder.append("todo!()\n");
        builder.append(" ".repeat(2));
        builder.append("}\n\n")
    }

    builder.append("}");

    file_utils::write_content_to_file(&service_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 service.rs 文件成功...");
}
//

pub async fn gen_subscribe_file(
    service_api_descriptor: &ServiceApiDescriptor,
    _import_service_api_descriptors: Vec<ServiceApiDescriptor>,
    force: bool,
    project_path: &str,
) {
    let subscribe_file_path = format!("{}/src/subscribe.rs", project_path);
    if service_api_descriptor.subscribe_topics.is_empty() {
        file_utils::remove_file_sync(subscribe_file_path).expect("删除 subscribe.rs 失败！");
        return;
    }

    if !force {
        if file_utils::is_file_exist(&subscribe_file_path) {
            warn!(
        "subscribe.rs 文件已经存在, 防止覆盖代码, 跳过该文件的生成, 可以使用 -f 选项强制初始化!"
    );
            return;
        }
    }

    let is_succeed = file_utils::create_file(&subscribe_file_path).await;

    if !is_succeed {
        error!("创建文件 subscribe.rs 失败...");
        return;
    }

    let mut builder = string_builder::Builder::new(1024 * 1024);

    for subscribe_topic in service_api_descriptor.subscribe_topics.iter() {
        let (dxc_name, version) = if subscribe_topic.name.is_empty() {
            //
            (
                service_api_descriptor.service_descriptor.name.clone(),
                String::default(),
            )
        } else {
            extract_dxc_name_and_version_from_dxc_file_name(&subscribe_topic.name).unwrap()
        };

        let snake_case_subscribe_name = string_utils::convert_to_snake_case(&dxc_name);

        builder.append(format!("pub mod {} {{\n", snake_case_subscribe_name));

        builder.append("    use x_com_lib::x_core::xrpc::Context;\n");

        let snake_case_subscribe_name = format!(
            "{}_{}",
            snake_case_subscribe_name,
            version.replace(".", "_")
        );

        if subscribe_topic.name.is_empty() {
            builder.append("    use crate::x_com::source_api::*;\n");
        } else {
            builder.append(format!(
                "    use crate::x_com::import_api::{}::*;\n",
                snake_case_subscribe_name
            ));
        }

        for method in subscribe_topic.methods.iter() {
            builder.append(format!("    // {}\n", method.name));

            builder.append("    pub async fn ");

            let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

            builder.append(snake_case_method_name.as_bytes());

            builder.append("(_ctx: Context");

            let input_type = method.input_type.get_path();

            let is_param_empty = input_type.is_empty();

            if !is_param_empty {
                builder.append(", _param: ");
                let rust_input_type = format!("Box<{}>", proto_utils::protobuf_path_to_rust(&input_type));
                builder.append(rust_input_type.as_bytes());
            }
            builder.append(") {\n");

            builder.append("    }\n");
        }

        builder.append("}\n");
    }

    file_utils::write_content_to_file(&subscribe_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 subscribe.rs 文件成功...");
}

//
pub async fn gen_topic_file(service_api_descriptor: &ServiceApiDescriptor, project_path: &str) {
    let topic_file_path = format!("{}/src/x_com/topic.rs", project_path);

    if service_api_descriptor.topic.is_none() {
        file_utils::remove_file_sync(&topic_file_path).expect("删除 topic.rs 失败！");
        return;
    }

    let is_succeed = file_utils::create_file(&topic_file_path).await;

    if !is_succeed {
        error!("创建文件 topic.rs 失败...");
        return;
    }

    let topic = service_api_descriptor.topic.as_ref().unwrap();

    let mut builder = string_builder::Builder::new(1024 * 1024);

    //
    builder.append("#![allow(dead_code)]\n");
    builder.append("#![allow(unused_imports)]\n");

    builder.append("use x_com_lib::{x_api::xport::stream::{publish_message, publish_empty_message}, x_core::get_service_id, PublishInfo};\n");
    builder.append("use super::source_api::*;\n");

    //
    for method in topic.methods.iter() {
        //
        builder.append(format!("  // {}\n", method.name));

        let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

        let input_type = method.input_type.get_path();

        let is_param_empty = input_type.is_empty();

        if is_param_empty {
            builder.append(format!("pub fn {}() {{\n", snake_case_method_name));
            builder.append(format!(
                "    {}_with_fix_conn_id_and_tag(0, String::default());\n",
                snake_case_method_name
            ));
            builder.append("}\n");

            builder.append(format!(
                "pub fn {}_with_tag(tag: String) {{\n",
                snake_case_method_name
            ));
            builder.append(format!(
                "    {}_with_fix_conn_id_and_tag(0, tag);\n",
                snake_case_method_name
            ));
            builder.append("}\n");

            builder.append(format!(
                "pub fn {}_with_fix_conn_id(fix_conn_id: i64) {{\n",
                snake_case_method_name
            ));
            builder.append(format!(
                "    {}_with_fix_conn_id_and_tag(fix_conn_id, String::default());\n",
                snake_case_method_name
            ));
            builder.append("}\n");
        } else {
            let rust_input_type = proto_utils::protobuf_path_to_rust(&input_type);
            builder.append(format!(
                "pub fn {}(param: &Box<{}>){{\n",
                snake_case_method_name, rust_input_type
            ));
            builder.append(format!(
                "    {}_with_fix_conn_id_and_tag(param, 0, String::default());\n",
                snake_case_method_name
            ));
            builder.append("}\n");

            builder.append(format!(
                "pub fn {}_with_tag(param: &Box<{}>, tag: String){{\n",
                snake_case_method_name, rust_input_type
            ));
            builder.append(format!(
                "    {}_with_fix_conn_id_and_tag(param, 0, tag);\n",
                snake_case_method_name
            ));
            builder.append("}\n");

            builder.append(format!(
                "pub fn {}_fix_conn_id(param: &Box<{}>, fix_conn_id: i64){{\n",
                snake_case_method_name, rust_input_type
            ));
            builder.append(format!(
                "    {}_with_fix_conn_id_and_tag(param, fix_conn_id, String::default());\n",
                snake_case_method_name
            ));
            builder.append("}\n");
        }

        if is_param_empty {
            builder.append(format!(
                "pub fn {}_with_fix_conn_id_and_tag(fix_conn_id: i64, tag: String) {{\n",
                snake_case_method_name
            ));
        } else {
            let rust_input_type = proto_utils::protobuf_path_to_rust(&input_type);
            builder.append(format!(
                "pub fn {}_with_fix_conn_id_and_tag(param: &Box<{}>, fix_conn_id: i64, tag: String){{\n",
                snake_case_method_name, rust_input_type
            ));
        }

        //
        builder.append("    let mut publish_info = PublishInfo::default();\n");
        builder.append("    publish_info.service_id = get_service_id();\n");
        builder.append(format!(
            "    publish_info.topic = String::from(\"{}\");\n",
            method.name
        ));
        builder.append("    publish_info.tag = tag;\n");
        builder.append("    publish_info.fix_conn_id = fix_conn_id;\n");

        if is_param_empty {
            builder.append("    publish_empty_message(publish_info);\n");
        } else {
            builder.append("    publish_message(publish_info, param);\n");
        }

        //
        builder.append("}\n");
    }

    //

    file_utils::write_content_to_file(&topic_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 topic.rs 文件成功...");
}
//
pub async fn gen_event_file(
    _name: &str,
    service_api_descriptor: &ServiceApiDescriptor,
    force: bool,
    project_path: &str,
) {
    let event_file_path = format!("{}/src/event.rs", project_path);

    if service_api_descriptor.events.is_empty() {
        file_utils::remove_file_sync(event_file_path).expect("删除 event.rs 失败！");
        return;
    }

    if !force {
        if file_utils::is_file_exist(&event_file_path) {
            warn!(
      "event.rs 文件已经存在, 防止覆盖代码, 跳过该文件的生成, 可以使用 -f 选项强制初始化!"
  );
            return;
        }
    }

    let is_succeed = file_utils::create_file(&event_file_path).await;

    //

    if !is_succeed {
        error!("创建文件 event.rs 失败...");
        return;
    }

    let mut builder = string_builder::Builder::new(1024 * 1024);

    builder.append("use x_com_lib::{x_api::xport::event::EventContext, ChannelEvent, ServiceInfo, VerifyInfo, ServiceKey};\n");

    builder.append("pub struct XportEvent {}\n");

    builder.append("impl XportEvent {\n");

    builder.append("    pub fn new() -> Self {\n        XportEvent {}\n    }\n");

    let event = service_api_descriptor.events.get(0).unwrap();

    let local_service_on_event_name = String::from("LocalServiceOn");
    // if event.event_items.contains(&local_service_on_event_name) {
      if event.event_items.iter().any(|item| item.name == local_service_on_event_name) {
        builder.append("    // LocalServiceOn\n");
        builder.append("    pub async fn local_service_on(&self, _ctx: EventContext, service_info: Box<ServiceInfo>) {\n");
        builder.append("    }\n");
    }

    let local_service_on_event_name = String::from("LookForDXC");
    // if event.event_items.contains(&local_service_on_event_name) {
      if event.event_items.iter().any(|item| item.name == local_service_on_event_name) {
        builder.append("    // LookForDXC\n");
        builder.append(
            "    pub async fn look_for_dxc(&self, _ctx: EventContext, service_key: Box<ServiceKey>) {\n",
        );
        builder.append("    }\n");
    }

    let local_service_off_event_name = String::from("LocalServiceOff");
    // if event.event_items.contains(&local_service_off_event_name) {
      if event.event_items.iter().any(|item| item.name == local_service_off_event_name) {
        builder.append("    // LocalServiceOff\n");
        builder.append("    pub async fn local_service_off(&self, _ctx: EventContext, service_info: Box<ServiceInfo>) {\n");
        builder.append("    }\n");
    }

    let ipc_disconnected_event_name = String::from("IPCDisconnected");
    // if event.event_items.contains(&local_service_off_event_name) {
      if event.event_items.iter().any(|item| item.name == ipc_disconnected_event_name) {
        builder.append("    // IPCDisconnected\n");
        builder.append("    pub async fn ipc_disconnected(&self, _ctx: EventContext, service_info: Box<ServiceInfo>) {\n");
        builder.append("    }\n");
    }



    let channel_connected_event_name = String::from("ChannelConnected");
    // if event.event_items.contains(&channel_connected_event_name) {
      if event.event_items.iter().any(|item| item.name == channel_connected_event_name) {
        builder.append("    // ChannelConnected\n");
        builder.append("    pub async fn channel_connected(&self, _ctx: EventContext, channel_event: Box<ChannelEvent>) {\n");
        builder.append("    }\n");
    }

    let channel_disconnected_event_name = String::from("ChannelDisconnected");
    // if event.event_items.contains(&channel_disconnected_event_name) {
      if event.event_items.iter().any(|item| item.name == channel_disconnected_event_name) {
        builder.append("    // ChannelDisconnected\n");
        builder.append("    pub async fn channel_disconnected(&self, _ctx: EventContext, channel_event: Box<ChannelEvent>) {\n");
        builder.append("    }\n");
    }

    let message_in_event_name = String::from("MessageIn");
    // if event.event_items.contains(&message_in_event_name) {
      if event.event_items.iter().any(|item| item.name == message_in_event_name) {
        builder.append("    // MessageIn\n");
        builder.append(
            "    pub async fn message_in(&self, _ctx: EventContext, vertify_info: Box<VerifyInfo>) {\n",
        );
        builder.append("    }\n");
    }

    builder.append("}\n");

    file_utils::write_content_to_file(&event_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 event.rs 文件成功...");
    //
}

pub(super) async fn gen_project(
    project_path: String,
    service_name: String,
    service_api_descriptor: ServiceApiDescriptor,
    dxc_version: String,
    use_self: bool,
    force: bool,
) -> bool {
    gen_lib_file(&project_path, &service_api_descriptor).await;
    //
    gen_xport_core_file(&service_name, &service_api_descriptor, &project_path).await;
    //
    gen_xcom_mod_file(&project_path, &service_api_descriptor).await;
    //
    gen_service_file(&service_name, &service_api_descriptor, force, &project_path).await;

    gen_topic_file(&service_api_descriptor, &project_path).await;

    gen_event_file(&service_name, &service_api_descriptor, force, &project_path).await;

    //
    let is_succeed = gen_source_api_file(&service_api_descriptor, &project_path).await;
    //

    if !is_succeed {
        return false;
    }
    gen_cargo_toml(&service_name, &project_path, &dxc_version).await;
    //
    let sdk_file_path = format!("{}/protos", project_path);
    let source_api_file_path = format!("{}/src/x_com/import_api.rs", project_path);
    let dxc_name = service_api_descriptor.service_name();

    let result = gen_sdk::rust::gen(
        sdk_file_path,
        "component".to_string(),
        source_api_file_path,
        use_self,
        Some(&service_api_descriptor),
        Some(&dxc_name),
        Some(&dxc_version),
    )
    .await;

    if !result.0 {
        return false;
    }

    let import_service_api_descriptors = result.1.unwrap_or(Vec::default());

    gen_subscribe_file(
        &service_api_descriptor,
        import_service_api_descriptors,
        force,
        &project_path,
    )
    .await;

    true
}
