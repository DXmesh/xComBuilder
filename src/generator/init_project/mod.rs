pub mod java;
pub mod python;
pub mod rust;
//
use semver::Version;
use tracing::{error, info};
use x_common_lib::{
    serial::api_descriptor::ServiceApiDescriptor,
    utils::{
        file_utils,
        string_utils::{self, extract_version},
    },
};

async fn gen_gitignore(project_path: &str, dxc_name: &str) {
    let file_path = format!("{}/.gitignore", project_path);
    if file_utils::is_file_exist(&file_path) {
        return;
    }
    let content = format!(
        r#"**/target/
**/.vscode/
**/.idea/
**/log/
**/dxc/
**/*.dxc
**/Cargo.lock
**/*.exe
**/dxmesh.toml
import-api-{}-*.proto
"#,
        dxc_name
    );

    file_utils::write_content_to_file(file_path, content)
        .await
        .unwrap();
}

//
pub async fn gen(path: String, force: bool, use_self: bool, language: String) {
    //
    let language = language.to_lowercase();

    info!("开始生成代码...");
    let project_path = file_utils::get_absoule_path(&path);

    if project_path.is_none() {
        error!("路径：{} 不存在！", path);
        return;
    }
    //
    let project_path = project_path.unwrap();
    //
    let proto_path = format!("{}/protos", project_path);
    //
    let file_list = file_utils::get_file_list(&proto_path).await;
    if file_list.is_err() {
        panic!("获取路径 {} 下的 api 描述文件列表失败！", proto_path);
    }
    //
    let file_list = file_list.unwrap();
    //
    let source_file_name = file_list
        .iter()
        .enumerate()
        .find(|(_, ele)| ele.starts_with("source-api"))
        .map(|(_, ele)| ele);
    //
    if source_file_name.is_none() {
        panic!("protos 目录 缺少 source-api.proto 文件！");
    }
    //
    let source_file_name = source_file_name.unwrap();
    //
    let new_source_version = source_file_name
        .replace("source-api", "version")
        .replace(".proto", "");
    //
    let dxc_version = extract_version(&new_source_version);
    //
    if dxc_version.is_none() {
        panic!("无法识别：{} 版本号！", source_file_name);
    }
    //
    // let str_version = dxc_version.unwrap().replace("_", ".");
    let str_version = dxc_version.unwrap();

    let version = Version::parse(&str_version);

    if version.is_err() {
        panic!("无法识别：{} 版本号:{}!", source_file_name, str_version);
    }
    //
    // let dxc_version = version.unwrap().to_string();
    // 这里不正确，需要获取
    let proto_file_path = format!("{}/protos/{}", project_path, source_file_name);
    //
    let is_source_api_exist = file_utils::is_file_exist(&proto_file_path);
    //
    if !is_source_api_exist {
        error!("protos 目录 缺少 source_api.proto 文件！");
        return;
    }
    info!("开始解析描述文件...");
    // 生成
    let service_api_descriptor =
        ServiceApiDescriptor::load_with_include_path(proto_file_path.as_str(), true);
    if service_api_descriptor.is_err() {
        let err = service_api_descriptor.err().unwrap();
        error!("解析描述文件出错：{}...", err);
        return;
    }
    info!("解析文件完成，开始生成文件...");

    let service_api_descriptor = service_api_descriptor.unwrap();
    //
    let service_name = string_utils::convert_to_camel_case(service_api_descriptor.service_name());

    gen_gitignore(&project_path, service_api_descriptor.service_name()).await;

    match language.as_str() {
        "rust" | "rs" => {
            if !rust::gen_project(
                project_path,
                service_name,
                service_api_descriptor,
                str_version,
                use_self,
                force,
            )
            .await
            {
                return;
            }
        }
        "python" | "py" => {
            if !python::gen_project(
                project_path,
                service_name,
                service_api_descriptor,
                str_version,
                file_list,
                proto_file_path,
                use_self,
                force,
            )
            .await
            {
                return;
            }
        }
        "java" => {
            if !java::gen_project(
                project_path,
                service_name,
                service_api_descriptor,
                str_version,
                use_self,
                force,
            )
            .await
            {
                return;
            }
        }

        _ => {}
    }

    info!("生成成功！");
}
