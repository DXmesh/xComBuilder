use dxc_protobuf_parser::parser::model::Method;
use semver::Version;
use tracing::{error, info};
use x_common_lib::{
    serial::api_descriptor::ServiceApiDescriptor,
    utils::{
        file_utils,
        string_utils::{self, extract_dxc_name_and_version},
    },
};

use crate::{generator::gen_serial_code, proto_utils};

fn gen_method_params_and_return(builder: &mut string_builder::Builder, method: &Method) {
    let input_type = method.input_type.get_path();
    let is_param_empty = input_type.is_empty();
    if !is_param_empty {
        builder.append("param: ");
        builder.append(" &");
        let rust_input_type = format!("Box<{}>", proto_utils::protobuf_path_to_rust(&input_type));
        builder.append(rust_input_type.as_bytes());
    }
    let output_type = method.output_type.get_path();

    let is_output_empty = output_type.is_empty();

    if is_output_empty {
        builder.append(") ->  RequestEmptyFuture { \n");
    } else {
        builder.append(") ->  RequestFuture<");

        let rust_output_type = format!("Box<{}>", proto_utils::protobuf_path_to_rust(&output_type));
        builder.append(rust_output_type.as_bytes());

        builder.append(">  {\n");
    }
}


fn gen_code(
    service_api_descriptor: &ServiceApiDescriptor,
    dxc_name: &str,
    dxc_version: &str,
    is_self: bool,
    builder: &mut string_builder::Builder,
) {
    builder.append("pub mod ");
    // 获取文件名
    //   获取版本号

    // 加版本号才行
    builder.append(format!(
        "{}_{}",
        string_utils::convert_to_snake_case(&dxc_name),
        dxc_version.replace(".", "_")
    ));
    builder.append(" {\n");

    builder.append(" ".repeat(2));
    builder.append("use x_com_lib::x_api;\n");
    builder.append(" ".repeat(2));
    builder.append("use x_com_lib::ServiceKey;\n");
    builder.append(" ".repeat(2));
    builder.append("use x_com_lib::x_core::{\n");

    builder.append(" ".repeat(4));
    builder.append("add_request_handler, build_request_future, parse_response_and_wake,RequestEmptyFuture, RequestFuture, build_request_empty_future,parse_empty_response_and_wake \n");
    builder.append(" ".repeat(2));
    builder.append("};\n");

    if is_self {
        builder.append("    use x_com_lib::x_core::{serial_empty_request, serial_request};\n");
        builder.append("    use crate::x_com::source_api::*;\n");
    } else {
        gen_serial_code::gen_rust_serial_code(&service_api_descriptor, false, false, builder);
    }

    builder.append(" ".repeat(2));
    builder.append("static DXC_NAME:  &'static str = \"");
    builder.append(service_api_descriptor.service_name());
    builder.append("\";\n");

    // 获取版本号
    builder.append(" ".repeat(2));
    builder.append("static DXC_VERSION: &'static str = \"");
    builder.append(dxc_version);
    builder.append("\";\n");

    for method in service_api_descriptor.methods() {
        builder.append(" ".repeat(2));

        let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

        let input_type = method.input_type.get_path();
        let is_param_empty = input_type.is_empty();
        //

        builder.append(" ".repeat(2));
        builder.append("pub fn ");
        builder.append(snake_case_method_name.clone());
        builder.append("(");
        gen_method_params_and_return(builder, method);
        builder.append(" ".repeat(4));
        builder.append(snake_case_method_name.clone());

        if is_param_empty {
            builder.append("_with_channel_id(0)\n");
        } else {
            builder.append("_with_channel_id(0, param)\n");
        }

        builder.append("}\n");
        // //

        // builder.append(" ".repeat(2));
        // builder.append("pub fn ");
        // builder.append(snake_case_method_name.clone());
        // builder.append("_with_channel_id(channel_id: i64, ");
        // gen_method_params_and_return(builder, method);
        // builder.append(" ".repeat(4));
        // builder.append(snake_case_method_name.clone());

        // if is_param_empty {
        //     builder
        //         .append("_with_channel_id(channel_id)\n");
        // } else {
        //     builder.append(
        //         "_with_channel_id(channel_id, param)\n",
        //     );
        // }
        // builder.append("}\n");
        //
        // builder.append(" ".repeat(2));
        // builder.append("pub fn ");
        // builder.append(snake_case_method_name.clone());
        // builder.append("_with_service_name(service_name: &str, ");
        // gen_method_params_and_return(builder, method);
        // builder.append(" ".repeat(4));
        // builder.append(snake_case_method_name.clone());
        // if is_param_empty {
        //     builder.append("_with_service_name_and_channel_id(service_name, 0)\n");
        // } else {
        //     builder.append("_with_service_name_and_channel_id(service_name, 0, param)\n");
        // }
        // builder.append("}\n");
        //
        builder.append(" ".repeat(2));
        builder.append("pub fn ");
        builder.append(snake_case_method_name.clone());
        builder.append("_with_channel_id(channel_id: i64, ");
        gen_method_params_and_return(builder, method);

        builder.append(" ".repeat(4));

        let output_type = method.output_type.get_path();
        let is_output_empty = output_type.is_empty();

        if is_output_empty {
            builder
                .append("let (future, request_id, is_succeed) = build_request_empty_future();\n");
        } else {
            builder.append("let (future, request_id, is_succeed) = build_request_future();\n");
        }

        builder.append(" ".repeat(4));
        builder.append("if !is_succeed {\n");
        builder.append(" ".repeat(6));
        builder.append("return future;\n");
        builder.append(" ".repeat(4));
        builder.append("}\n");

        builder.append(" ".repeat(4));

        if is_param_empty {
            builder.append("let buffer = serial_empty_request(\"");
            builder.append(method.name.as_bytes());
            builder.append("\");\n");
        } else {
            builder.append("let buffer = serial_request(\"");
            builder.append(method.name.as_bytes());
            builder.append("\", param);\n");
        }

        builder.append(" ".repeat(4));

        builder.append("let clone_shared_state = future.shared_state.clone();\n");

        builder.append(" ".repeat(4));

        builder.append("add_request_handler(request_id, Box::new(move |buffer| {\n");

        builder.append(" ".repeat(6));

        if is_output_empty {
            builder.append("parse_empty_response_and_wake(&clone_shared_state, buffer);\n");
        } else {
            let rust_output_type = proto_utils::protobuf_path_to_rust(&output_type);
            builder.append("parse_response_and_wake::<");
            builder.append(rust_output_type.as_bytes());
            builder.append(">(&clone_shared_state, buffer);\n");
        }

        builder.append(" ".repeat(4));
        builder.append("}));\n");

        builder.append(" ".repeat(4));
        builder.append("let mut receiver = ServiceKey::default();\n");
        builder.append(" ".repeat(4));
        builder.append("receiver.dxc_name = DXC_NAME.into();\n");
        builder.append(" ".repeat(4));
        builder.append("receiver.dxc_version = DXC_VERSION.into();\n");

        builder.append(" ".repeat(4));
        builder.append("x_api::xport::send_message(request_id, receiver, channel_id, buffer);\n");

        builder.append(" ".repeat(4));
        builder.append("future\n");

        builder.append(" ".repeat(2));
        builder.append("}\n");
    }
    builder.append("}\n");
}

pub async fn gen(
    path: String,
    _sdk_type: String,
    output: String,
    use_self: bool,
    self_api_descriptor: Option<&ServiceApiDescriptor>,
    self_name: Option<&str>,
    self_verison: Option<&str>,
) -> (bool, Option<Vec<ServiceApiDescriptor>>) {
    info!("开始生成SDK代码...");
    let input_path = file_utils::get_absoule_path(&path);
    if input_path.is_none() {
        error!("文件：{} 不存在！", path);
        return (false, None);
    }

    let input_path = input_path.unwrap();

    let file_list = file_utils::get_file_list(&input_path).await;
    if let Err(err) = file_list {
        error!(
            "获取路径 {} 下的 api 描述文件列表失败: {:?}",
            input_path, err
        );
        return (false, None);
    }
    let mut builder = string_builder::Builder::new(1024 * 1024);

    builder.append("#![allow(dead_code, unused_imports)]\n");

    if use_self {
        gen_code(
            self_api_descriptor.unwrap(),
            self_name.unwrap(),
            self_verison.unwrap(),
            true,
            &mut builder,
        );
    }

    let mut import_api_service_api_descriptors = Vec::new();

    for filename in file_list.unwrap() {
        if !filename.starts_with("import-api-") {
            continue;
        }
        let sdk_file_full_path = format!("{}/{}", input_path, filename);
        let service_api_descriptor =
            ServiceApiDescriptor::load_with_include_path(sdk_file_full_path.as_str(), true);
        if service_api_descriptor.is_err() {
            let err = service_api_descriptor.err().unwrap();
            error!("解析描述文件 {} 出错：{}...", sdk_file_full_path, err);
            return (false, None);
        }
        let service_api_descriptor = service_api_descriptor.unwrap();
        let dxc_name_and_version = filename.replace("import-api-", "").replace(".proto", "");
        let result = extract_dxc_name_and_version(&dxc_name_and_version);
        if result.is_none() {
            panic!("无法识别：{} 版本号！", filename);
        }
        let (dxc_name, dxc_version) = result.unwrap();
        let version = Version::parse(&dxc_version);
        if version.is_err() {
            panic!("无法识别：{} 版本号:{}", filename, &dxc_version);
        }

        gen_code(
            &service_api_descriptor,
            &dxc_name,
            &dxc_version,
            false,
            &mut builder,
        );
        import_api_service_api_descriptors.push(service_api_descriptor);
    }

    let is_succeed = file_utils::create_file(&output).await;

    if !is_succeed {
        error!("创建文件 import_api.rs 失败...");
        return (false, None);
    }

    file_utils::write_content_to_file(&output, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 import_api.rs 文件成功...");
    (true, Some(import_api_service_api_descriptors))
    // 解析文件
}

fn gen_dqtrader_method_params_and_return(builder: &mut string_builder::Builder, method: &Method) {
  let input_type = method.input_type.get_path();
  let is_param_empty = input_type.is_empty();
  if !is_param_empty {
      builder.append("param: ");
      builder.append(" &");
      let rust_input_type = format!("Box<{}>", proto_utils::protobuf_path_to_rust(&input_type));
      builder.append(rust_input_type.as_bytes());
  }
  let output_type = method.output_type.get_path();

  let is_output_empty = output_type.is_empty();

  if is_output_empty {
      builder.append(") ->  x_api::Result<()> { \n");
  } else {
      builder.append(") ->  x_api::Result<");

      let rust_output_type = format!("Box<{}>", proto_utils::protobuf_path_to_rust(&output_type));
      builder.append(rust_output_type.as_bytes());

      builder.append(">  {\n");
  }
}

fn gen_dqtrader_code(
    service_api_descriptor: &ServiceApiDescriptor,
    dxc_name: &str,
    dxc_version: &str,
    builder: &mut string_builder::Builder,
) {
    builder.append("pub mod ");
    // 获取文件名
    //   获取版本号

    // 加版本号才行
    builder.append(format!(
        "{}_{}",
        string_utils::convert_to_snake_case(&dxc_name),
        dxc_version.replace(".", "_")
    ));
    builder.append(" {\n");

    builder.append("use x_common_lib::{\n");
    builder.append("base::status::Status, protocol::protocol_ipc::{xport::SEND_MESSAGE_API_ID, ProtocolIPCWriter},\n");
    builder.append("serial::{api_descriptor::extract_wire_type_from_tag, request_message::RequestMessage},\n");
    builder.append("service::sys_service_api::ServiceKey,\n");
    builder.append(" ".repeat(2));
    builder.append("};\n");

    builder.append("use crate::{ \n");
    builder.append("gen_id,\n");
    builder.append("x_api::{ \n");
    builder.append("self, compute_serial_empty_request_size, compute_serial_request_size, ipc,\n");
    builder.append("xport_future::{\n");
    builder.append("add_request_empty_handler, add_request_handler, add_request_result_handler,\n");
    builder.append("build_request_empty_future, build_result_api_future, RequestEmptyFuture,notice_send_fail,\n");
    builder.append("},\n");
    builder.append("},x_com_lib,\n");
    builder.append("};\n");

    gen_serial_code::gen_rust_serial_code(&service_api_descriptor, true, false, builder);
    

    builder.append(" ".repeat(2));
    builder.append("static DXC_NAME:  &'static str = \"");
    builder.append(service_api_descriptor.service_name());
    builder.append("\";\n");

    // 获取版本号
    builder.append(" ".repeat(2));
    builder.append("static DXC_VERSION: &'static str = \"");
    builder.append(dxc_version);
    builder.append("\";\n");

    for method in service_api_descriptor.methods() {
        builder.append(" ".repeat(2));
        let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);
        let input_type = method.input_type.get_path();
        let is_param_empty = input_type.is_empty();
        //
        builder.append(" ".repeat(2));
        builder.append("pub async fn ");
        builder.append(snake_case_method_name.clone());
        builder.append("(");
        gen_dqtrader_method_params_and_return(builder, method);
        builder.append(" ".repeat(4));
        builder.append(snake_case_method_name.clone());

        if is_param_empty {
            builder.append("_with_channel_id(0).await\n");
        } else {
            builder.append("_with_channel_id(0, param).await\n");
        }

        builder.append("}\n");

        builder.append(" ".repeat(2));
        builder.append("pub async fn ");
        builder.append(snake_case_method_name.clone());
        builder.append("_with_channel_id(channel_id: i64, ");
        gen_dqtrader_method_params_and_return(builder, method);
        builder.append(" ".repeat(4));
        let output_type = method.output_type.get_path();
        let is_output_empty = output_type.is_empty();
        builder.append("let mut receiver = ServiceKey::default();\n");
        builder.append("receiver.dxc_name = DXC_NAME.into();\n");
        builder.append("receiver.dxc_version = DXC_VERSION.into();\n");
        if is_param_empty {
          builder.append(format!("let msg_size = compute_serial_empty_request_size(&receiver, \"{}\");\n", method.name));
        } else {
          builder.append(format!("let msg_size = compute_serial_request_size(&receiver, \"{}\", &param);\n", method.name));
        }

        builder.append("let request_id = gen_id();\n");

        builder.append("let mut ipc_writer = ProtocolIPCWriter::new(msg_size as u32, SEND_MESSAGE_API_ID, request_id);\n");

        builder.append("ipc_writer.write_channel_id(channel_id);\n");
        builder.append("let msg_body = ipc_writer.msg_body_buffer();\n");

        builder.append("let mut os = protobuf::CodedOutputStream::bytes(msg_body);\n");
        builder.append("receiver.serial_with_tag_and_len(&mut os);\n");
        builder.append(format!("os.write_string(1, \"{}\").unwrap();\n",method.name));

        if !is_param_empty {
          builder.append("param.serial_with_output_stream(&mut os).unwrap();\n");
        } 
        builder.append("drop(os);\n");

        if is_output_empty {
            builder.append("let future = build_request_empty_future();\n");
        } else {
          let rust_output_type = proto_utils::protobuf_path_to_rust(&output_type);
            builder.append(format!("let future = build_result_api_future::<{}>();\n", rust_output_type));
        }
        builder.append("let clone_shared_state = future.shared_state.clone();\n");

        if is_output_empty {
          builder.append("add_request_empty_handler(request_id, clone_shared_state);\n");
        } else {
          builder.append("add_request_result_handler(request_id, clone_shared_state);\n");
        }
        builder.append("let buffer = ipc_writer.msg_buffer;\n");

        builder.append("let ret = ipc::send_message(&buffer).await;\n");
        builder.append("if !ret {\nnotice_send_fail(request_id);\n}");

        builder.append("future.await\n");
    
        builder.append("}\n");
    }
    builder.append("}\n");
}

pub async fn gen_dqtrader(path: String) {
    //
    info!("开始生成 dqtrader SDK代码...");
    let input_path = file_utils::get_absoule_path(&path);
    if input_path.is_none() {
        error!("文件：{} 不存在！", path);
        return;
    }
    //
    let input_path = input_path.unwrap();
    //
    let file_list = file_utils::get_file_list(&input_path).await;
    if let Err(err) = file_list {
        error!(
            "获取路径 {} 下的 api 描述文件列表失败: {:?}",
            input_path, err
        );
        return;
    }
    //
    let mut builder = string_builder::Builder::new(1024 * 1024);
    builder.append("#![allow(dead_code, unused_imports)]\n");

    for filename in file_list.unwrap() {
        if !filename.starts_with("import-api-") {
            continue;
        }
        //
        let sdk_file_full_path = format!("{}/{}", input_path, filename);
        let service_api_descriptor =
            ServiceApiDescriptor::load_with_include_path(sdk_file_full_path.as_str(), true);
        if service_api_descriptor.is_err() {
            let err = service_api_descriptor.err().unwrap();
            error!("解析描述文件 {} 出错：{}...", sdk_file_full_path, err);
            return;
        }
        let service_api_descriptor = service_api_descriptor.unwrap();
        let dxc_name_and_version = filename.replace("import-api-", "").replace(".proto", "");
        let result = extract_dxc_name_and_version(&dxc_name_and_version);
        if result.is_none() {
            panic!("无法识别：{} 版本号！", filename);
        }
        let (dxc_name, dxc_version) = result.unwrap();
        let version = Version::parse(&dxc_version);
        if version.is_err() {
            panic!("无法识别：{} 版本号:{}", filename, &dxc_version);
        }
        gen_dqtrader_code(
            &service_api_descriptor,
            &dxc_name,
            &dxc_version,
            &mut builder,
        );
    }
    let output = "./src/x_com/import_api.rs";
    let is_succeed = file_utils::create_file(output).await;
    if !is_succeed {
        error!("创建文件 import_api.rs 失败...");
        return ;
    }
    file_utils::write_content_to_file(output, &builder.string().unwrap())
        .await
        .unwrap();

    info!("生成 import_api.rs 文件成功...");
}


