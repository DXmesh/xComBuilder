use semver::Version;
use string_builder::Builder;
use tracing::error;
use x_common_lib::{
    serial::api_descriptor::ServiceApiDescriptor,
    utils::{
        file_utils,
        string_utils::{self, extract_dxc_name_and_version},
    },
};

use crate::{generator::serial::python::gen_class_and_enum, proto_utils};

pub async fn gen(
    filename: String,
    proto_file_path: String,
    project_path: &str,
    init_builder: &mut Builder,
) -> bool {
    let dxc_name_and_version = filename.replace("import-api-", "").replace(".proto", "");

    //
    let result = extract_dxc_name_and_version(&dxc_name_and_version);
    if result.is_none() {
        panic!("无法识别：{} 版本号！", filename);
    }

    let (dxc_name, dxc_version) = result.unwrap();

    // let dxc_version = dxc_version.unwrap();//.replace("_", ".");
    let version = Version::parse(&dxc_version);

    if version.is_err() {
        panic!("无法识别：{} 版本号:{}", filename, &dxc_version);
    }

    let dxc_line_version = dxc_version.replace(".", "_");

    let snake_case_dxc_name_and_version = format!("{}_{}", string_utils::convert_to_snake_case(&dxc_name), dxc_line_version);
      
    let output_path = format!(
        "{}/src/x_com/import_api/{}.py",
        &project_path, &snake_case_dxc_name_and_version
    );

    init_builder.append(format!(
        "from . import {}\n",
        snake_case_dxc_name_and_version
    ));

    gen_with_dxc_name_and_version(
        proto_file_path,
        output_path,
        project_path,
        &dxc_version,
        false,
    )
    .await
}

pub async fn gen_with_dxc_name_and_version(
    proto_file_path: String,
    output: String,
    _project_path: &str,
    dxc_version: &str,
    is_self: bool,
) -> bool {
    let mut builder = string_builder::Builder::new(1024 * 1024);

    let service_api_descriptor =
        ServiceApiDescriptor::load_with_include_path(proto_file_path.as_str(), true);
    //

    if service_api_descriptor.is_err() {
        let err = service_api_descriptor.err().unwrap();
        error!("解析描述文件 {} 出错：{}...", proto_file_path, err);
        return false;
    }
    let service_api_descriptor = service_api_descriptor.unwrap();
    //

    if is_self {
        builder.append("import source_api\n");
    } else {
        gen_class_and_enum(&service_api_descriptor, &mut builder);
    }

    builder.append("import x_com_lib \n\n");

    builder.append(format!(
        "DXC_NAME = \"{}\" \n",
        service_api_descriptor.service_name()
    ));
    builder.append(format!("DXC_VERSION = \"{}\" \n", dxc_version));

    //
    let methods = service_api_descriptor.methods();

    let type_scope = if is_self {
        String::from("source_api.")
    } else {
        String::default()
    };

    for method in methods {
        let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

        builder.append(format!("async def {}(", &snake_case_method_name));

        let input_type = method.input_type.get_path();
        let is_param_empty = input_type.is_empty();
        //
        if !is_param_empty {
            let python_input_type = proto_utils::protobuf_path_to_python(&input_type);
            builder.append(format!("param: {}{}", type_scope, python_input_type));
        }
        builder.append(")");

        let output_type = method.output_type.get_path();
        let is_output_empty = output_type.is_empty();
        if !is_output_empty {
            let python_output_type = proto_utils::protobuf_path_to_python(&output_type);
            builder.append(format!(" -> {}{}", type_scope, python_output_type));
        }
        builder.append(":\n");

        if is_output_empty {
            builder.append(format!(
                "  await {}_with_channel_id(0",
                &snake_case_method_name
            ));
        } else {
            builder.append(format!(
                "  return await {}_with_channel_id(0",
                &snake_case_method_name
            ));
        }
        //
        if !is_param_empty {
            builder.append(", param")
        }
        builder.append(")\n\n");

        builder.append(format!(
            "async def {}_with_channel_id(channel_id: int",
            &snake_case_method_name
        ));

        if !is_param_empty {
            let python_input_type = proto_utils::protobuf_path_to_python(&input_type);
            builder.append(format!(", param: {}{}", type_scope, python_input_type));
        }

        builder.append(")");
        if !is_output_empty {
            let python_output_type = proto_utils::protobuf_path_to_python(&output_type);
            builder.append(format!(" -> {}{}", type_scope, python_output_type));
        }
        builder.append(":\n");

        //
        if is_param_empty {
            if is_output_empty {
                builder.append(format!(
                "  await x_com_lib.xport.send_message(DXC_NAME, DXC_VERSION,channel_id, \"{}\", None)",
                method.name
            ));
            } else {
                builder.append(format!(
                "  return await x_com_lib.xport.send_message(DXC_NAME, DXC_VERSION, channel_id, \"{}\", None)",
                method.name
            ));
            }
        } else {
            if is_output_empty {
                builder.append(format!(
              "  await x_com_lib.xport.send_message(DXC_NAME, DXC_VERSION, channel_id, \"{}\", param)",
              method.name
          ));
            } else {
                builder.append(format!(
              "  return await x_com_lib.xport.send_message(DXC_NAME, DXC_VERSION, channel_id, \"{}\", param)",
              method.name
          ));
            }
        }

        builder.append("\n\n");
    }

    file_utils::create_file_path_sync(&output);

    file_utils::write_content_to_file(&output, &builder.string().unwrap())
        .await
        .unwrap();

    true
}
