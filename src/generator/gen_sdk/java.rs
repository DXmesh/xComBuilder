use semver::Version;
use string_builder::Builder;
use tracing::{error, info};
use x_common_lib::{
    serial::api_descriptor::ServiceApiDescriptor,
    utils::{
        file_utils,
        string_utils::{self, extract_dxc_name_and_version},
    },
};

use crate::{generator::serial::java, proto_utils};

async fn gen_code(
    service_api_descriptor: &ServiceApiDescriptor,
    dxc_name: &str,
    dxc_version: &str,
    output: &str,
) {
    let mut builder = Builder::new(1024 * 1024);

    let service_name = service_api_descriptor.service_name();

    let file_name = format!(
        "{}_{}",
        string_utils::convert_to_snake_case(&dxc_name),
        dxc_version.replace(".", "_")
    );

    builder.append(format!(
        r#"package io.dxmesh.x_com.import_api.{};
import io.dxmesh.api.Xport;
import io.dxmesh.serial.ServiceKey;
import java.nio.ByteBuffer;
        "#,
        file_name
    ));

    let service_file_path = format!("{}/{}/{}.java", output, file_name, service_name);

    let service_name = service_api_descriptor.service_name();

    builder.append(format!("public class {} {{", service_name));

    builder.append("public static ServiceKey defaultServiceKey;\n");

    builder.append(format!(
        r#"
    static {{
        defaultServiceKey = new ServiceKey();
        defaultServiceKey.setDxcName("{}");
        defaultServiceKey.setDxcVersion("{}");
    }}
    "#,
        service_name, dxc_version
    ));

    for method in service_api_descriptor.methods() {
        let java_method_name = string_utils::convert_to_small_camel_case(&method.name);

        let input_type = method.input_type.get_path();
        let is_param_empty = input_type.is_empty();

        let output_type = method.output_type.get_path();
        let is_output_empty = output_type.is_empty();

        let java_output_type = if is_output_empty {
            String::from("void")
        } else {
            proto_utils::protobuf_path_to_java(&output_type)
        };

        builder.append(format!(
            "public static {} {}(",
            java_output_type, java_method_name
        ));

        let java_input_type = proto_utils::protobuf_path_to_java(&input_type);

        if !is_param_empty {
            builder.append(format!("{} param", java_input_type));
        }

        builder.append(") {\n");

        if is_param_empty {
            if is_output_empty {
                builder.append(format!("{}WithChannelId(0);\n", java_method_name));
            } else {
                builder.append(format!("return {}WithChannelId(0);\n", java_method_name));
            }
        } else {
            if is_output_empty {
                builder.append(format!("{}WithChannelId(0, param);\n", java_method_name));
            } else {
                builder.append(format!(
                    "return {}WithChannelId(0, param);\n",
                    java_method_name
                ));
            }
        }

        builder.append("}\n");

        builder.append(format!(
            "public static {} {}WithChannelId(long channelId",
            java_output_type, java_method_name
        ));

        if !is_param_empty {
            builder.append(format!(", {} param", java_input_type));
        }
        builder.append(") {\n");

        builder.append("try {\n");

        if !is_param_empty {
            builder.append(format!("ByteBuffer buffer = Xport.sendMessage(defaultServiceKey, channelId, \"{}\", param);\n", method.name));
        } else {
            builder.append(format!("ByteBuffer buffer = Xport.sendMessage(defaultServiceKey, channelId, \"{}\", null);\n", method.name));
        }

        if is_output_empty {
            builder.append("Xport.ResultVoid result = Xport.parseRawVoidBufferResult(buffer);\n");
        } else {
            builder.append(format!(
                "Xport.Result<{}> result = Xport.parseRawBufferResult(buffer, new {}());\n",
                java_output_type, java_output_type
            ));

            builder.append("return result.data;\n");
        }

        builder.append(
            r#"
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        "#,
        );

        builder.append("}\n");
    }

    builder.append("}\n");

    file_utils::create_file_path_sync(&service_file_path);
    file_utils::write_content_to_file(&service_file_path, &builder.string().unwrap())
        .await
        .unwrap();

    let dest_path = format!("{}/{}", output, file_name);

    let package_name = format!("io.dxmesh.x_com.import_api.{}", file_name);

    java::gen_class_and_enum(service_api_descriptor, &package_name, &dest_path);

    info!("生成 {}.java  文件成功...", service_name);
}

pub async fn gen(
    path: String,
    _sdk_type: String,
    output: String,
    use_self: bool,
    self_api_descriptor: Option<&ServiceApiDescriptor>,
    self_name: Option<&str>,
    self_verison: Option<&str>,
) -> bool {
    info!("开始生成SDK代码...");
    let input_path = file_utils::get_absoule_path(&path);
    if input_path.is_none() {
        error!("文件：{} 不存在！", path);
        return false;
    }

    let input_path = input_path.unwrap();
    let file_list = file_utils::get_file_list(&input_path).await;
    if file_list.is_err() {
        error!("获取路径 {} 下的 api 描述文件列表失败！", input_path);
        return false;
    }

    //
    if use_self {
        gen_code(
            self_api_descriptor.unwrap(),
            self_name.unwrap(),
            self_verison.unwrap(),
            &output,
        )
        .await;
    }

    for filename in file_list.unwrap() {
        if !filename.starts_with("import-api-") {
            continue;
        }
        let sdk_file_full_path = format!("{}/{}", input_path, filename);
        let service_api_descriptor =
            ServiceApiDescriptor::load_with_include_path(sdk_file_full_path.as_str(), true);
        if service_api_descriptor.is_err() {
            let err = service_api_descriptor.err().unwrap();
            error!("解析描述文件 {} 出错：{}...", sdk_file_full_path, err);

            return false;
        }

        let service_api_descriptor = service_api_descriptor.unwrap();

        let dxc_name_with_version = filename.replace("import-api-", "").replace(".proto", "");

        let result = extract_dxc_name_and_version(&dxc_name_with_version);
        if result.is_none() {
            panic!("无法识别：{} 版本号！", filename);
        }
        let (dxc_name, dxc_version) = result.unwrap();

        // let dxc_version = dxc_version.unwrap();//.replace("_", ".");
        let version = Version::parse(&dxc_version);

        if version.is_err() {
            panic!("无法识别：{} 版本号:{}", filename, &dxc_version);
        }

        gen_code(&service_api_descriptor, &dxc_name, &dxc_version, &output).await;
    }
    true
}
