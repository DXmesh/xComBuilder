use x_common_lib::{serial::api_descriptor::ServiceApiDescriptor, utils::string_utils};
use string_builder::Builder;

use crate::proto_utils;

#[allow(dead_code)]
pub fn gen_service(service_api_descriptor: &ServiceApiDescriptor, builder: &mut Builder) -> bool {
    let service_name = service_api_descriptor.service_name();

    let methods = service_api_descriptor.methods();

    builder.append("#[x_com_lib::async_trait]\n");

    builder.append("pub trait ");
    builder.append(service_name);

    builder.append(" : Send + Sync {\n");

    builder.append(" ".repeat(2));

    builder.append("fn on_init(&self);\n");

    builder.append(" ".repeat(2));

    builder.append("fn on_finalize(&self);\n");

    for method in methods {
        builder.append(" ".repeat(2));

        builder.append("async fn ");

  
        let snake_case_method_name = string_utils::convert_to_snake_case(&method.name);

        builder.append(snake_case_method_name.as_bytes());

        builder.append("(&self");

        let input_type = method.input_type.get_path();

        let is_param_empty = input_type.is_empty();
        if !is_param_empty {
          builder.append(", ");
          let rust_input_type = proto_utils::protobuf_path_to_rust(&input_type);

          builder.append(" _: &");

          builder.append(rust_input_type.as_bytes());

        }

        builder.append(")");

        let output_type = method.output_type.get_path();

        let is_output_empty = output_type.is_empty();
               

        builder.append(" -> x_core::Result<");

        if is_output_empty {
          builder.append("()");
        } else {
          let rust_output_type = proto_utils::protobuf_path_to_rust(&output_type);
          builder.append(rust_output_type.as_bytes());
  
        }
        builder.append(">;\n")
    }

    builder.append("} \n");

    true
}
