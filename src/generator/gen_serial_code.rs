use x_common_lib::serial::api_descriptor::ServiceApiDescriptor;

use crate::generator::serial::rust;

use string_builder::Builder;
use tracing::{error, info};
use x_common_lib::utils::file_utils;

pub fn gen_rust_serial_code(
    service_api_descriptor: &ServiceApiDescriptor,
    is_dqtrader: bool,
    is_sys: bool,
    builder: &mut Builder,
) {
    rust::gen_struct_and_enum(&service_api_descriptor, is_dqtrader, is_sys, builder);

    rust::gen_serial(&service_api_descriptor, builder);
}

pub async fn gen_rust(path: String, output: String, name: String, is_sys: bool) {
    info!("开始生成序列化代码...");

    let input_path = file_utils::get_absoule_path(&path);

    if input_path.is_none() {
        error!("文件：{} 不存在！", path);
        return;
    }

    let input_path = input_path.unwrap();

    if !file_utils::is_file_exist(&input_path) {
        error!("文件：{} 不存在！", input_path);
        return;
    }

    let service_api_descriptor =
        ServiceApiDescriptor::load_with_include_path(input_path.as_str(), false);

    if service_api_descriptor.is_err() {
        let err = service_api_descriptor.err().unwrap();

        error!("解析描述文件出错：{}...", err);

        return;
    }

    info!("解析文件完成...");
    let service_api_descriptor = service_api_descriptor.unwrap();
    let mut builder = string_builder::Builder::new(1024 * 1024);

    gen_rust_serial_code(&service_api_descriptor, false, is_sys, &mut builder);

    let output = file_utils::get_absoule_path(&output);

    if output.is_none() {
        error!("文件：{} 不存在！", path);
        return;
    }

    let output = output.unwrap();

    let name = if name.is_empty() {
        file_utils::get_file_name_from_path(&input_path).unwrap()
    } else {
        name
    };

    let name = name
        .replace(".proto", "")
        .replace(".", "_")
        .replace("-", "_");

    let output_file_path = format!("{}/{}.rs", output, name);

    if !file_utils::is_file_exist(&output_file_path) {
        let succeed = file_utils::create_file(&output_file_path).await;
        if !succeed {
            error!("创建输出文件：{} 失败！", output_file_path);
            return;
        }
    }

    let mut content = builder.string().unwrap();

    if is_sys {
      content = content.replace("x_com_lib::", "protobuf::");
    }

    file_utils::write_content_to_file(&output_file_path, &content)
        .await
        .unwrap();

    info!("生成序列化代码成功！");
}

pub async fn gen_sys() {
    let file_path = "protos/sys_service_api.proto".to_owned();
    let output_path = "src/service/".to_owned();
   

    gen_rust(file_path, output_path, String::default(), true).await;

    // info!("开始生成序列化代码...");
    // let file_path = "protos/sys.proto";
    // let input_path = file_utils::get_absoule_path(file_path);
    // if input_path.is_none() {
    //     error!("文件: protos/sys.proto 不存在！");
    //     return;
    // }
    // //
    // let input_path = input_path.unwrap();
    // if !file_utils::is_file_exist(&input_path) {
    //     error!("文件: protos/sys.proto 不存在！");
    //     return;
    // }
    // //
    // let service_api_descriptor =
    //     ServiceApiDescriptor::load_with_include_path(input_path.as_str(), false);
    // if service_api_descriptor.is_err() {
    //     let err = service_api_descriptor.err().unwrap();
    //     error!("解析描述文件出错：{}...", err);
    //     return;
    // }
    // info!("解析文件完成...");
}
