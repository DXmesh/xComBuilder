use rustpython_parser::{
    ast::{self, Stmt},
    Parse,
};

use std::{
    collections::{HashMap, LinkedList},
    fs::File,
    io::Write,
    path::Path,
    process::Command,
};
use tracing::info;
use x_common_lib::{
    base::dxc::Manifest,
    utils::{env_utils, file_utils, string_utils},
};
use zip::{write::SimpleFileOptions, CompressionMethod, ZipWriter};

use crate::generator::{get_dxc_name_and_version_source_api, get_source_proto_file_name};

fn zip_string_as_file(zip_writer: &mut ZipWriter<File>, file_name: &str, content: &str) {
    let options = SimpleFileOptions::default()
        .compression_method(CompressionMethod::Deflated)
        .unix_permissions(0o755);

    zip_writer
        .start_file(file_name, options)
        .expect(&format!("压缩文件 {} 失败", file_name));

    zip_writer
        .write(content.as_bytes())
        .expect(&format!("压缩文件 {} 失败", file_name));
}

fn zip_file(zip_writer: &mut ZipWriter<File>, file_path: &str, file_name: String) {
    let mut file = File::open(file_path).expect(&format!("读取文件 {} 失败", file_path));

    let options = SimpleFileOptions::default()
        .compression_method(CompressionMethod::Deflated)
        .unix_permissions(0o755);

    let file_name = if file_name.is_empty() {
        file_utils::get_file_name_from_path(file_path).unwrap()
    } else {
        file_name
    };

    zip_writer
        .start_file(file_name, options)
        .expect(&format!("压缩文件 {} 失败", file_path));

    std::io::copy(&mut file, zip_writer).expect(&format!("压缩文件 {} 失败", file_path));
}

fn get_python_pip_packlist_recursively(
    py_stmts: &Vec<Stmt>,
    pip_package_map: &mut HashMap<String, String>,
) {
    for stmt in py_stmts {
        if stmt.is_import_stmt() {
            let import_stmt: &ast::StmtImport = stmt.as_import_stmt().unwrap();
            for name in &import_stmt.names {
                let identifier = &name.name;
                pip_package_map.insert(identifier.to_string(), String::default());
            }
        } else if stmt.is_import_from_stmt() {
            let import_from_stmt = stmt.as_import_from_stmt().unwrap();
            if let Some(identifier) = &import_from_stmt.module {
                pip_package_map.insert(identifier.to_string(), String::default());
            }
        } else if stmt.is_function_def_stmt() {
            let def_stmt: &ast::StmtFunctionDef = stmt.as_function_def_stmt().unwrap();
            get_python_pip_packlist_recursively(&def_stmt.body, pip_package_map);
        } else if stmt.is_async_function_def_stmt() {
            let async_def_stmt: &ast::StmtAsyncFunctionDef =
                stmt.as_async_function_def_stmt().unwrap();
            get_python_pip_packlist_recursively(&async_def_stmt.body, pip_package_map);
        } else if stmt.is_class_def_stmt() {
            let class_stmt: &ast::StmtClassDef = stmt.as_class_def_stmt().unwrap();
            get_python_pip_packlist_recursively(&class_stmt.body, pip_package_map);
        } else if stmt.is_if_stmt() {
            //
            let if_stmt: &ast::StmtIf = stmt.as_if_stmt().unwrap();
            get_python_pip_packlist_recursively(&if_stmt.body, pip_package_map);
            get_python_pip_packlist_recursively(&if_stmt.orelse, pip_package_map);
        } else if stmt.is_for_stmt() {
            let for_stmt: &ast::StmtFor = stmt.as_for_stmt().unwrap();
            get_python_pip_packlist_recursively(&for_stmt.body, pip_package_map);
            get_python_pip_packlist_recursively(&for_stmt.orelse, pip_package_map);
        } else if stmt.is_while_stmt() {
            let while_stmt: &ast::StmtWhile = stmt.as_while_stmt().unwrap();
            get_python_pip_packlist_recursively(&while_stmt.body, pip_package_map);

            get_python_pip_packlist_recursively(&while_stmt.orelse, pip_package_map);
        } else if stmt.is_try_stmt() {
            let try_stmt: &ast::StmtTry = stmt.as_try_stmt().unwrap();
            get_python_pip_packlist_recursively(&try_stmt.body, pip_package_map);

            get_python_pip_packlist_recursively(&try_stmt.orelse, pip_package_map);
            get_python_pip_packlist_recursively(&try_stmt.body, pip_package_map);
            get_python_pip_packlist_recursively(&try_stmt.finalbody, pip_package_map);
        } else if stmt.is_async_for_stmt() {
            let async_for_stmt: &ast::StmtAsyncFor = stmt.as_async_for_stmt().unwrap();
            get_python_pip_packlist_recursively(&async_for_stmt.body, pip_package_map);
            get_python_pip_packlist_recursively(&async_for_stmt.orelse, pip_package_map);
        } else if stmt.is_with_stmt() {
            let with_stmt: &ast::StmtWith = stmt.as_with_stmt().unwrap();
            get_python_pip_packlist_recursively(&with_stmt.body, pip_package_map);
        } else if stmt.is_async_with_stmt() {
            let async_with_stmt: &ast::StmtAsyncWith = stmt.as_async_with_stmt().unwrap();
            get_python_pip_packlist_recursively(&async_with_stmt.body, pip_package_map);
        } else if stmt.is_try_star_stmt() {
            let try_star_stmt: &ast::StmtTryStar = stmt.as_try_star_stmt().unwrap();
            get_python_pip_packlist_recursively(&try_star_stmt.body, pip_package_map);
            get_python_pip_packlist_recursively(&try_star_stmt.orelse, pip_package_map);
            get_python_pip_packlist_recursively(&try_star_stmt.finalbody, pip_package_map);
        } else if stmt.is_match_stmt() {
            //
            let match_stmt: &ast::StmtMatch = stmt.as_match_stmt().unwrap();
            for case in &match_stmt.cases {
                get_python_pip_packlist_recursively(&case.body, pip_package_map);
            }
        }
    }
}

async fn get_python_pip_packlist(
    path: &str,
    file_list: &LinkedList<&String>,
) -> HashMap<String, String> {
    let mut pip_package_map = HashMap::new();
    for &file_name in file_list.iter() {
        let file_path = format!("{}src/{}", path, file_name);

        let file_content = file_utils::read_file_as_string(&file_path).await.unwrap();
        let py_stmt = ast::Suite::parse_without_path(&file_content).unwrap(); // statements
        get_python_pip_packlist_recursively(&py_stmt, &mut pip_package_map);
    }

    //
    let output = Command::new("pip")
        .arg("list")
        .output()
        .expect("获取 pip 包版本号失败");
    //

    let output_str = String::from_utf8_lossy(&output.stdout);
    for line in output_str.lines().skip(2) {
        // 跳过前两行，因为它们是标题和分隔符
        let parts: Vec<&str> = line.split_whitespace().collect();
        if parts.len() >= 2 {
            let dxc_name: &str = parts[0];
            let version = parts[1];
            let value = pip_package_map.get_mut(dxc_name);
            if let Some(value) = value {
                *value = version.into();
            }
        }
    }
    pip_package_map
}

pub async fn pack_python(path: String, output: String, mode: String) {
    let source_file_name = get_source_proto_file_name(&path).await;

    let dsl_path = format!("{}/protos/{}", path, source_file_name);
    if !file_utils::is_file_exist(&dsl_path) {
        panic!("缺少 protos/source-api proto 描述文件！");
    }
    let (dxc_name, dxc_version) = get_dxc_name_and_version_source_api(&path, &source_file_name);
    //
    //
    let py_service_path = format!("{}/src/service.py", path);

    if !file_utils::is_file_exist(&py_service_path) {
        panic!("缺少 src/service.py 文件！");
    }
    //
    let src_path = format!("{}/src", path);

    let py_file_list = file_utils::get_file_list_recursively(&src_path, true)
        .await
        .unwrap();

    let x_com_fle_list: LinkedList<_> = py_file_list
        .iter()
        .filter(|&file| file.contains("x_com") && !file.contains("__pycache__"))
        .clone()
        .collect();

    let py_file_list: LinkedList<_> = py_file_list
        .iter()
        .filter(|&file| !file.contains("x_com") && !file.contains("__pycache__"))
        .clone()
        .collect();

    //
    let pip_map = get_python_pip_packlist(&path, &py_file_list).await;
    //

    let dxc_path = format!("{}/{}-{}-none-none.dxc", output, dxc_name, dxc_version);

    let output_dsl_path = format!("{}/import-api-{}-{}.proto", output, dxc_name, dxc_version);

    let manifest = Manifest {
        name: dxc_name,
        version: dxc_version,
        mode: String::from(mode),
        language: String::from("python"),
        system: String::from("none"),
        cpu: String::from("none"),
        depend_package: pip_map,
    };
    let manifest_json = serde_json::to_string(&manifest).unwrap();
    // 压缩文件
    std::fs::create_dir_all(&output).expect("创建文件失败！");
    let output_file = File::create(dxc_path).expect("创建文件失败！");
    //
    let mut zip_writer = ZipWriter::new(output_file);

    info!("打包元信息...");
    zip_string_as_file(&mut zip_writer, "manifest.json", &manifest_json);
    info!("打包描述文件...");

    let protos_path = format!("{}/protos", path);

    zip_dir(&mut zip_writer, &protos_path, "protos", ".proto").await;

    let ui_path = format!("{}/ui", path);
    if file_utils::is_dir_exist_sync(&ui_path) {
        info!("打包ui文件...");
        zip_dir(&mut zip_writer, &ui_path, "ui", "").await;
    }

    info!("打包程序文件...");

    for file_name in py_file_list {
        let real_file_path = format!("{}/src/{}", &path, file_name);
        zip_file(
            &mut zip_writer,
            &real_file_path,
            format!("src/{}", file_name),
        );
    }
    //
    for x_com_file_name in x_com_fle_list {
        let real_file_path = format!("{}/src/{}", &path, x_com_file_name);
        zip_file(
            &mut zip_writer,
            &real_file_path,
            format!("src/{}", x_com_file_name),
        );
    }

    zip_writer.finish().expect("生成 DXC 文件失败");
    info!("打包成功！");
    //

    file_utils::copy_file(&dsl_path, &output_dsl_path)
        .await
        .expect("生成导出 Api 描述文件失败！");
}

pub async fn zip_dir(
    zip_writer: &mut ZipWriter<File>,
    dir_path: &str,
    folder_name: &str,
    file_type: &str,
) {
    if !file_utils::is_dir_exist(dir_path).await {
        return;
    }
    let file_list = file_utils::get_file_list(dir_path).await.unwrap();
    for file in file_list {
        if file_type.len() != 0 && !file.ends_with(file_type) {
            continue;
        }
        let real_file_path = format!("{}/{}", &dir_path, &file);
        if file_utils::is_dir_exist_sync(&real_file_path) {
            let sub_folder_name = format!("{}/{}", folder_name, file);
            let boxed_future = Box::pin(zip_dir(
                zip_writer,
                &real_file_path,
                &sub_folder_name,
                file_type,
            ));
            boxed_future.await;
        } else {
            zip_file(
                zip_writer,
                &real_file_path,
                format!("{}/{}", folder_name, &file),
            )
        }
    }
}

//
pub async fn pack_rust(path: String, output: String, mode: String) {
    let mode = if mode.is_empty() {
        String::from("debug")
    } else {
        mode
    };

    let source_file_name = get_source_proto_file_name(&path).await;

    let dsl_path = format!("{}/protos/{}", path, source_file_name);
    if !file_utils::is_file_exist(&dsl_path) {
        panic!("缺少 protos/source-api proto 描述文件！");
    }
    //
    let (dxc_name, dxc_version) = get_dxc_name_and_version_source_api(&path, &source_file_name);
    //

    let snake_dxc_name = string_utils::convert_to_snake_case(&dxc_name);

    let proto_path = format!("{}/protos", path);

    let file_list = file_utils::get_file_list(&proto_path).await;
    if file_list.is_err() {
        panic!("获取路径 {} 下的 api 描述文件列表失败！", proto_path);
    }

    let file_list = file_list.unwrap();
    let dsl_path = file_list
        .iter()
        .enumerate()
        .find(|(_, ele)| ele.starts_with("source-api"))
        .map(|(_, ele)| ele);

    if dsl_path.is_none() {
        panic!("缺少 protos/source-api proto 描述文件！");
    }

    let dsl_path = dsl_path.unwrap();

    let dsl_path = format!("{}/protos/{}", path, dsl_path);
    //
    if !file_utils::is_file_exist(&dsl_path) {
        panic!("缺少 protos/source-api proto 描述文件！");
    }

    #[cfg(windows)]
    let binary_path = format!("{}/target/{}/{}.dll", path, mode, &snake_dxc_name);

    #[cfg(unix)]
    let binary_path = format!("{}/target/{}/lib{}.so", path, mode, &snake_dxc_name);
    //
    if !file_utils::is_file_exist(&binary_path) {
        panic!("{} 文件不存在，请编译工程", binary_path);
    }

    let cpu_info = env_utils::get_system_cpu_info().unwrap();

    let dxc_path = format!("{}/{}-{}-{}.dxc", output, dxc_name, dxc_version, cpu_info);

    let output_dsl_path = format!("{}/import-api-{}-{}.proto", output, &dxc_name, &dxc_version);

    let manifest = Manifest {
        name: dxc_name,
        version: dxc_version,
        mode: String::from(mode),
        language: String::from("rust"),
        system: env_utils::get_system().unwrap(),
        cpu: env_utils::get_cpu().unwrap(),
        depend_package: HashMap::default(),
    };
    let manifest_json = serde_json::to_string(&manifest).unwrap();

    // 压缩文件

    std::fs::create_dir_all(&output).expect("创建文件失败！");

    let output_file = File::create(dxc_path).expect("创建文件失败！");

    let mut zip_writer = ZipWriter::new(output_file);

    info!("打包元信息...");
    zip_string_as_file(&mut zip_writer, "manifest.json", &manifest_json);

    info!("打包描述文件...");
    //

    let protos_path = format!("{}/protos", path);

    zip_dir(&mut zip_writer, &protos_path, "protos", ".proto").await;

    let depends_path = format!("{}/depends", path);

    zip_dir(&mut zip_writer, &depends_path, "depends", "").await;

    let ui_path = format!("{}/ui", path);

    if file_utils::is_dir_exist_sync(&ui_path) {
        info!("打包ui文件...");
        zip_dir(&mut zip_writer, &ui_path, "ui", "").await;
    }

    info!("打程序文件...");
    zip_file(&mut zip_writer, &binary_path, String::default());

    zip_writer.finish().expect("生成 DXC 文件失败");
    info!("打包成功！");
    //

    file_utils::copy_file(&dsl_path, &output_dsl_path)
        .await
        .expect("生成导出 Api 描述文件失败！");
    //
}

pub async fn pack_java(path: String, output: String, _mode: String) {
    // let mode = if mode.is_empty() {
    //     String::from("debug")
    // } else {
    //     mode
    // };

    let source_file_name = get_source_proto_file_name(&path).await;

    let dsl_path = format!("{}/protos/{}", path, source_file_name);
    if !file_utils::is_file_exist(&dsl_path) {
        panic!("缺少 protos/source-api proto 描述文件！");
    }
    let (dxc_name, dxc_version) = get_dxc_name_and_version_source_api(&path, &source_file_name);
    //
    let proto_path = format!("{}/protos", path);

    let file_list = file_utils::get_file_list(&proto_path).await;
    if file_list.is_err() {
        panic!("获取路径 {} 下的 api 描述文件列表失败！", proto_path);
    }

    let file_list = file_list.unwrap();
    let dsl_path = file_list
        .iter()
        .enumerate()
        .find(|(_, ele)| ele.starts_with("source-api"))
        .map(|(_, ele)| ele);

    if dsl_path.is_none() {
        panic!("缺少 protos/source-api proto 描述文件！");
    }
    //
    let dsl_path = dsl_path.unwrap();

    let dsl_path = format!("{}/protos/{}", path, dsl_path);
    //
    if !file_utils::is_file_exist(&dsl_path) {
        panic!("缺少 protos/source-api proto 描述文件！");
    }

    let jar_mame = string_utils::convert_to_snake_case(&dxc_name);

    let jar_mame = jar_mame.replace("_service", "");
    let jar_path = format!("{}target/{}-{}.jar", path, jar_mame, dxc_version);

    if !file_utils::is_file_exist(&jar_path) {
        panic!("{} 文件不存在，请编译工程", jar_path);
    }

    let dxc_path = format!("{}/{}-{}-none-none.dxc", output, dxc_name, dxc_version);

    let output_dsl_path = format!("{}/import_api-{}-{}.proto", output, dxc_name, dxc_version);

    let manifest = Manifest {
        name: dxc_name,
        version: dxc_version,
        mode: String::from("release"),
        language: String::from("java"),
        system: String::from("none"),
        cpu: String::from("none"),
        depend_package: HashMap::default(),
    };
    let manifest_json = serde_json::to_string(&manifest).unwrap();

    // 压缩文件

    std::fs::create_dir_all(&output).expect("创建文件失败！");

    let output_file = File::create(dxc_path).expect("创建文件失败！");

    let mut zip_writer = ZipWriter::new(output_file);

    info!("打包元信息...");
    zip_string_as_file(&mut zip_writer, "manifest.json", &manifest_json);

    info!("打包描述文件...");

    let protos_path = format!("{}/protos", path);

    zip_dir(&mut zip_writer, &protos_path, "protos", ".proto").await;

    let ui_path = format!("{}/ui", path);
    if file_utils::is_dir_exist_sync(&ui_path) {
        info!("打包ui文件...");
        zip_dir(&mut zip_writer, &ui_path, "ui", "").await;
    }

    let lib_path = format!("{}/target/lib", path);

    zip_dir(&mut zip_writer, &lib_path, "lib", "").await;

    info!("打程序文件...");
    zip_file(&mut zip_writer, &jar_path, String::default());

    zip_writer.finish().expect("生成 DXC 文件失败");
    info!("打包成功！");
    //

    file_utils::copy_file(&dsl_path, &output_dsl_path)
        .await
        .expect("生成导出 Api 描述文件失败！");
    //
}
pub async fn unpack(path: String, output: String) {
    let input = File::open(&path).expect(&format!("打开文件失败，请检查文件{}是否存在", &path));

    let dxc_name = file_utils::get_file_name_from_path(&path)
        .unwrap()
        .to_ascii_lowercase()
        .replace(".dxc", "");

    let output = format!("{}/{}", output, dxc_name);

    std::fs::create_dir_all(&output).expect(&format!("创建解压文件路径 {} 失败！", &output));

    let mut zip = zip::ZipArchive::new(input).expect("打开文件失败，请检查文件是否是 DXC 文件");

    for i in 0..zip.len() {
        let mut file = zip.by_index(i).expect("读取文件失败！");

        let file_name = file.mangled_name();

        // 创建文件的完整路径
        let output_path = Path::new(&output).join(file_name);

        let mut output_file = File::create(&output_path)
            .expect(&format!("创建文件 {} 失败", &output_path.to_str().unwrap()));

        std::io::copy(&mut file, &mut output_file)
            .expect(&format!("写入文件 {} 失败", &output_path.to_str().unwrap()));
    }

    info!("解压成功！");
}
