use x_common_lib::utils::{file_utils, string_utils};
use tracing::{error, info};

pub async fn new(path: String, name: String, version: String) {
    let version = semver::Version::parse(&version).expect("版本号格式错误！");

    let version = version.to_string();//.replace(".", "_");

    info!("开始生成...");

    let project_path = file_utils::get_absoule_path(&path);
    if project_path.is_none() {
        error!("文件：{} 不存在！", path);
    }

    let project_path = project_path.unwrap();

    if !file_utils::is_dir_exist(&project_path).await {
        error!("文件：{} 不存在！", project_path);
        return;
    }

    let full_path = format!("{}/protos/source-api-{}.proto", project_path, version);

    if !file_utils::create_file(&full_path).await {
        error!("生成失败: 创建文件 {} 失败！", full_path);
        return;
    }

    let content = format!(
        r#"

message Hello {{
  string name = 1;
}}

message HelloReply {{
  string greeting = 1;
}}

service {} {{
    rpc SayHello(Hello) returns (HelloReply);

    rpc EmptyParamAndReply();
}}

    "#,
        string_utils::convert_to_camel_case(name.as_ref())
    );

    file_utils::write_content_to_file(&full_path, &content)
        .await
        .unwrap();

    info!("生成项目成功，路径 = {}！", full_path);
}
