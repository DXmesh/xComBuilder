use std::collections::HashMap;

use dxc_protobuf_parser::parser::model::{Enumeration, Field, FieldType, Message};
use string_builder::Builder;

use x_common_lib::serial::api_descriptor::{
    gen_map_key_value_field, get_inner_field, get_wire_type, is_array, is_optional, make_tag,
    DescriptorProtoHolder, ServiceApiDescriptor,
};

use crate::{
    generator::{put_message_in_nest_map, NestMessage},
    proto_utils,
};

fn gen_enum(enum_info: &Enumeration, builder: &mut Builder) {
    let enum_name = enum_info.name.replace(".", "");

    builder.append("#[derive(Clone, Copy, Debug, PartialEq, Eq)]\n");

    builder.append("pub enum ");
    builder.append(enum_name.as_bytes());
    builder.append(" { \n");

    for field in &enum_info.values {
        builder.append(field.name.as_bytes());
        builder.append(" = ");
        builder.append(field.number.to_string().as_bytes());
        builder.append(", \n");
    }
    builder.append("}\n\n");
}

fn gen_struct(struct_info: &DescriptorProtoHolder, builder: &mut Builder) {
    let struct_info = &struct_info.descriptor_proto;
    // 是map 字段 不生成结构体

    let struct_name = struct_info.name.replace(".", "");
    builder.append("#[derive(Clone)]\n");
    builder.append("pub struct ");
    builder.append(struct_name.as_bytes());
    builder.append(" { \n");
    //
    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();
        builder.append("pub ");
        builder.append(field.name.as_bytes());
        builder.append(": ");

        builder.append(proto_utils::protobuf_type_to_rust(field));
        builder.append(", \n");
    }

    builder.append("pub(crate) cached_size: x_com_lib::rt::CachedSize, \n");
    

    builder.append("}\n\n");
}

fn gen_struct_and_enum_2(
    nest_message: &NestMessage,
    builder: &mut Builder,
    service_api_descriptor: &ServiceApiDescriptor,
) {
    //
    for (key, value) in &nest_message.sub_message {
        if value.is_enum {
            gen_enum(
                service_api_descriptor
                    .enum_descriptor_map
                    .get(&value.message_name)
                    .unwrap(),
                builder,
            );
        } else {
            //
            gen_struct(
                service_api_descriptor
                    .message_descriptor_holder
                    .get(&value.message_name)
                    .unwrap(),
                builder,
            );
        }

        if !value.sub_message.is_empty() {
            builder.append("pub mod ");
            builder.append(key.as_bytes());
            builder.append(" { \n");
            //
            gen_struct_and_enum_2(&value, builder, service_api_descriptor);
            builder.append("} \n\n");
        }
    }
}

/**
 * 生成从 api 生成的
 */
pub fn gen_struct_and_enum(
    service_api_descriptor: &ServiceApiDescriptor,
    is_dqtrader: bool,
    is_sys: bool,
    builder: &mut Builder,
) {
    if is_sys {
        builder.append("#![allow(unused_imports)]\n\n");

        builder.append(
    "use crate::{base::status::Status, serial::{api_descriptor::extract_wire_type_from_tag, request_message::RequestMessage}};\n\n",
);
    } else if !is_dqtrader {
        builder.append("#[allow(unused_imports)]\n");

        builder.append(
        "use x_com_lib::{extract_wire_type_from_tag, x_core::{self,serial_empty_request,serial_request}, RequestMessage,\nCodedInputStream, CodedOutputStream, Status};\n\n",
    );
    }
    let mut nest_message = NestMessage {
        message_name: String::new(),
        sub_message: HashMap::new(),
        is_enum: false,
    };

    // 放进来
    for message_name in service_api_descriptor.message_descriptor_holder.keys() {
        put_message_in_nest_map(message_name, &mut nest_message, false);
    }
    //
    for enum_name in service_api_descriptor.enum_descriptor_map.keys() {
        // 排序
        put_message_in_nest_map(enum_name, &mut nest_message, true);
    }

    gen_struct_and_enum_2(&nest_message, builder, service_api_descriptor);
}

fn gen_enum_serial(enum_name: &str, enum_info: &Enumeration, builder: &mut Builder) {
    let enum_name = proto_utils::protobuf_path_to_rust(enum_name);

    builder.append("impl ");
    builder.append(enum_name.as_bytes());
    builder.append(" { \n");

    builder.append("pub fn from_i32(value: i32) -> Option<");
    builder.append(enum_name.as_bytes());
    builder.append("> { \n");

    builder.append(" match value { \n");

    for field in &enum_info.values {
        builder.append(field.number.to_string().as_bytes());
        builder.append(" => Some(Self::");
        builder.append(field.name.as_bytes());
        builder.append("), \n");
    }

    builder.append("_ => None \n");

    builder.append("} \n");

    builder.append("}\n\n");

    builder.append("} \n\n");
}

fn gen_compute_size_fn_inner(
    field: &Field,
    is_gen_map_type_size: bool,
    is_key: bool,
    builder: &mut Builder,
) {
    let is_array = is_array(field);
    let is_optional = is_optional(field);
    let number = field.number as u32;
    let tag = make_tag(number, 2);
    let size_len = ::protobuf::rt::compute_raw_varint64_size(tag as u64);

    match &field.typ {
        FieldType::Double => {
            if is_gen_map_type_size {
                builder.append(format!("entry_size += {};\n", size_len + 8));
            } else {
                if is_array {
                    builder.append(format!(
                        "my_size += {} * self.{}.len() as u64;\n",
                        size_len + 8,
                        field.name
                    ));
                } else if is_optional {
                    builder.append(format!("if let Some(_v) = self.{} {{ \n", field.name));
                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if self.{}  != 0.0 {{ \n", field.name));
                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Float => {
            if is_gen_map_type_size {
                builder.append(format!("entry_size += {};\n", size_len + 4));
            } else {
                if is_array {
                    builder.append(format!(
                        "my_size += {} * self.{}.len() as u64;\n",
                        size_len + 4,
                        field.name
                    ));
                } else if is_optional {
                    builder.append(format!("if let Some(_v) = self.{} {{ \n", field.name));
                    builder.append(format!("my_size += {};\n", size_len + 4));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if self.{} != 0.0 {{ \n", field.name));
                    builder.append(format!("my_size += {};\n", size_len + 4));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Int64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entry_size += x_com_lib::rt::int64_size(1, *k);\n");
                } else {
                    builder.append("entry_size += x_com_lib::rt::int64_size(2, *v);\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append("for value in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("my_size += x_com_lib::rt::int64_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *value);\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("my_size += x_com_lib::rt::int64_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v);\n");
                    builder.append("}\n");
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("my_size += x_com_lib::rt::int64_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(");\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Uint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entry_size += x_com_lib::rt::uint64_size(1, *k);\n");
                } else {
                    builder.append("entry_size += x_com_lib::rt::uint64_size(2, *v);\n");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for value in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("my_size += x_com_lib::rt::uint64_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *value);\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("my_size += x_com_lib::rt::uint64_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v);\n");
                    builder.append("}\n");
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("my_size += x_com_lib::rt::uint64_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(");\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Int32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entry_size += x_com_lib::rt::int32_size(1, *k);\n");
                } else {
                    builder.append("entry_size += x_com_lib::rt::int32_size(2, *v);\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append("for value in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("my_size += x_com_lib::rt::int32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *value);\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("my_size += x_com_lib::rt::int32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v);\n");
                    builder.append("}\n");
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("my_size += x_com_lib::rt::int32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(");\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Fixed64 => {
            if is_gen_map_type_size {
                builder.append(format!("entry_size += {};\n", size_len + 8));
            } else {
                if is_array {
                    builder.append(format!(
                        "my_size += {} * self.{}.len() as u64;\n",
                        size_len + 8,
                        field.name
                    ));
                } else if is_optional {
                    builder.append("if let Some(_v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");

                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Fixed32 => {
            if is_gen_map_type_size {
                builder.append(format!("entry_size += {};\n", size_len + 4));
            } else {
                if is_array {
                    builder.append(format!(
                        "my_size += {} * self.{}.len() as u64;\n",
                        size_len + 4,
                        field.name
                    ));
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append(format!("my_size += {} ;\n", size_len + 4));
                    builder.append("}\n");
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append(format!("my_size += {};\n", size_len + 4));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Uint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entry_size += x_com_lib::rt::uint32_size(1, *k);\n");
                } else {
                    builder.append("entry_size += x_com_lib::rt::uint32_size(2, *v);\n");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for value in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("my_size += x_com_lib::rt::uint32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *value);\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("my_size += x_com_lib::rt::uint32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v);\n");
                    builder.append("}\n");
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("my_size += x_com_lib::rt::uint32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(");\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sfixed32 => {
            if is_gen_map_type_size {
                builder.append(format!("entry_size += {};\n", size_len + 4));
            } else {
                if is_array {
                    builder.append(format!(
                        "my_size += {} * self.{}.len() as u64;\n",
                        size_len + 4,
                        field.name
                    ));
                } else if is_optional {
                    builder.append("if let Some(_v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append(format!("my_size += {} \n", size_len + 4));
                    builder.append("}\n");
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append(format!("my_size += {} ;\n", size_len + 4));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sfixed64 => {
            if is_gen_map_type_size {
                builder.append(format!("entry_size += {};\n", size_len + 8));
            } else {
                if is_array {
                    builder.append(format!(
                        "my_size += {} * self.{}.len() as u64;\n",
                        size_len + 8,
                        field.name
                    ));
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");

                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entry_size += x_com_lib::rt::sint32_size(1, *k);\n");
                } else {
                    builder.append("entry_size += x_com_lib::rt::sint32_size(2, *v);\n");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for value in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("my_size += x_com_lib::rt::sint32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *value);\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("my_size += x_com_lib::rt::sint32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v);\n");
                    builder.append("}\n");
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("my_size += x_com_lib::rt::sint32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(");\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entry_size += x_com_lib::rt::sint64_size(1, *k);\n");
                } else {
                    builder.append("entry_size += x_com_lib::rt::sint64_size(2, *v);\n");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for value in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("my_size += x_com_lib::rt::sint64_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *value);\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("my_size += x_com_lib::rt::sint64_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v);\n");
                    builder.append("}\n");
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("my_size += x_com_lib::rt::sint64_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(");\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Bool => {
            if is_gen_map_type_size {
                builder.append(format!("entry_size += {};\n", size_len + 1));
            } else {
                if is_array {
                    builder.append(format!(
                        "my_size += {} * self.{}.len() as u64;\n",
                        size_len + 1,
                        field.name
                    ));
                } else if is_optional {
                    builder.append("if let Some(_v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append(format!("my_size += {} ;\n", size_len + 1));
                    builder.append("}\n");
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != false { \n");
                    builder.append(format!("my_size += {};\n", size_len + 1));
                    builder.append("}\n");
                }
            }
        }
        FieldType::String => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entry_size += x_com_lib::rt::string_size(1, &k);\n");
                } else {
                    builder.append("entry_size += x_com_lib::rt::string_size(2, &v);\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append("for value in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("my_size += x_com_lib::rt::string_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", &value);\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(".as_ref() { \n");
                    builder.append("my_size += x_com_lib::rt::string_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", &v);\n");
                    builder.append("}\n");
                } else {
                    builder.append("if !self.");
                    builder.append(field.name.clone());
                    builder.append(".is_empty() { \n");
                    builder.append("my_size += x_com_lib::rt::string_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", &self.");
                    builder.append(field.name.clone());
                    builder.append(");\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Map(_map_type) => {
            if is_gen_map_type_size {
                builder.append("let len = v.cached_size.get() as u64;\n");

                let full_code = format!(
                    "entry_size += {} + x_com_lib::rt::compute_raw_varint64_size(len) + len;\n",
                    size_len
                );
                builder.append(full_code);
            } else {
                let (key_field, value_field) = gen_map_key_value_field(&field.typ).unwrap();

                builder.append("for (k, v) in &self.");
                builder.append(field.name.clone());
                builder.append(" {\n");
                builder.append("let mut entry_size = 0; \n");
                gen_compute_size_fn_inner(&key_field, true, true, builder);
                builder.append("\n");
                //
                gen_compute_size_fn_inner(&value_field, true, false, builder);
                builder.append("\n");
                let full_code = format!(
                "my_size += {} + x_com_lib::rt::compute_raw_varint64_size(entry_size) + entry_size; \n",
                size_len
            );

                builder.append(full_code);
                builder.append("}\n");
            }
        }
        FieldType::Message(_) => {
            if is_gen_map_type_size {
                builder.append("let len = v.cached_size.get() as u64;\n");

                let full_code = format!(
                    "entry_size += {} + x_com_lib::rt::compute_raw_varint64_size(len) + len;\n",
                    size_len
                );
                builder.append(full_code);
            } else {
                if is_array {
                    builder.append("for value in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("let len = value.compute_size();\n");

                    let full_code = format!(
                        "my_size += {} + x_com_lib::rt::compute_raw_varint64_size(len) + len;\n",
                        size_len
                    );

                    builder.append(full_code);
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(".as_ref() { \n");

                    builder.append("let len = v.compute_size();\n");

                    let full_code = format!(
                        "my_size += {} + x_com_lib::rt::compute_raw_varint64_size(len) + len;\n",
                        size_len
                    );

                    builder.append(full_code);
                    builder.append("}\n");
                } else {
                    builder.append("{ \n");

                    builder.append("let len = self.");
                    builder.append(field.name.clone());
                    builder.append(".compute_size();\n");

                    let full_code = format!(
                        "my_size += {} + x_com_lib::rt::compute_raw_varint64_size(len) + len;\n",
                        size_len
                    );
                    builder.append(full_code);
                    builder.append("}\n");
                }
            }
        }
        FieldType::Enum(_) => {
            if is_gen_map_type_size {
                builder.append("entry_size += x_com_lib::rt::int32_size(2, v.unwrap() as i32);\n");
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for value in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("my_size += x_com_lib::rt::int32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", value.unwrap() as i32);\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(".as_ref() { \n");
                    builder.append("my_size += x_com_lib::rt::int32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v as i32);\n");
                    builder.append("}\n");
                } else {
                    builder.append("if let Some(value) = self.");
                    builder.append(field.name.clone());
                    builder.append(".as_ref() { \n");
                    builder.append("my_size += x_com_lib::rt::int32_size(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *value as i32);\n");
                    builder.append("}\n");
                }
            }
        }
        _ => {}
    }
}

fn gen_compute_size_fn(struct_info: &Message, builder: &mut Builder) {
    builder.append("fn compute_size(&self) -> u64 { \n");
    builder.append("let mut my_size = 0;\n");
    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();
        gen_compute_size_fn_inner(field, false, false, builder);
    }
    builder.append("self.cached_size.set(my_size as u32);\n");
    // cached_size: CachedSize,
    builder.append("my_size\n");
    builder.append("}\n\n");
}

fn gen_serial_fn_inner(
    field: &Field,
    is_gen_map_type_size: bool,
    is_key: bool,
    builder: &mut Builder,
) {
    let is_array = is_array(field);
    let is_optional = is_optional(field);

    match &field.typ {
        FieldType::Double => {
            if is_gen_map_type_size {
                builder.append("os.write_double(2, *v).unwrap();");
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_double(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_double(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0.0 { \n");

                    builder.append("os.write_double(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Float => {
            if is_gen_map_type_size {
                builder.append("os.write_float(2, *v).unwrap();");
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_float(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_float(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0.0 { \n");
                    builder.append("os.write_float(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Int64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_int64(1, *k).unwrap();");
                } else {
                    builder.append("os.write_int64(2, *v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_int64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_int64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("os.write_int64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Int32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_int32(1, *k).unwrap();");
                } else {
                    builder.append("os.write_int32(2, *v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_int32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_int32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("os.write_int32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Uint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_uint64(1, *k).unwrap();");
                } else {
                    builder.append("os.write_uint64(2, *v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_uint64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_uint64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("os.write_uint64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");

                    builder.append("}\n");
                }
            }
        }
        FieldType::Uint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_uint32(1, *k).unwrap();");
                } else {
                    builder.append("os.write_uint32(2, *v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_uint32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_uint32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("os.write_uint32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Fixed64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_fixed64(1, *k).unwrap();");
                } else {
                    builder.append("os.write_fixed64(2, *v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_fixed64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_fixed64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("os.write_fixed64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Fixed32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_fixed32(1, *k).unwrap();");
                } else {
                    builder.append("os.write_fixed32(2, *v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_fixed32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_fixed32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("os.write_fixed32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sfixed32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_sfixed32(1, *k).unwrap();");
                } else {
                    builder.append("os.write_sfixed32(2, *v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_sfixed32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_sfixed32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("os.write_sfixed32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sfixed64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_sfixed64(1, *k).unwrap();");
                } else {
                    builder.append("os.write_sfixed64(2, *v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_sfixed64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_sfixed64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("os.write_sfixed64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_sint32(1, *k).unwrap();");
                } else {
                    builder.append("os.write_sint32(2, *v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_sint32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_sint32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("os.write_sint32(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_sint64(1, *k).unwrap();");
                } else {
                    builder.append("os.write_sint64(2, *v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_sint64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_sint64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != 0 { \n");
                    builder.append("os.write_sint64(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Bool => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_bool(1, *k).unwrap();");
                } else {
                    builder.append("os.write_bool(2, *v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_bool(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(" { \n");
                    builder.append("os.write_bool(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if self.");
                    builder.append(field.name.clone());
                    builder.append(" != false { \n");
                    builder.append("os.write_bool(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::String => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.write_string(1, &k).unwrap();");
                } else {
                    builder.append("os.write_string(2, &v).unwrap();");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_string(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", &v).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(".as_ref() { \n");
                    builder.append("os.write_string(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append("if !self.");
                    builder.append(field.name.clone());
                    builder.append(".is_empty() { \n");
                    builder.append("os.write_string(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", &self.");
                    builder.append(field.name.clone());
                    builder.append(").unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        FieldType::Map(_map_type) => {
            if is_gen_map_type_size {
                builder.append(
                    "os.write_tag(2, x_com_lib::rt::WireType::LengthDelimited).unwrap();\n",
                );
                builder.append("os.write_raw_varint32(v.cached_size.get() as u32).unwrap();\n");
                builder.append("v.serial_with_output_stream(os).unwrap();\n");
            } else {
                let (key_field, value_field) = gen_map_key_value_field(&field.typ).unwrap();

                builder.append("for (k, v) in &self.");
                builder.append(field.name.clone());
                builder.append(" {\n");

                builder.append("let mut entry_size = 0; \n");
                gen_compute_size_fn_inner(&key_field, true, true, builder);
                builder.append("\n");
                gen_compute_size_fn_inner(&value_field, true, false, builder);
                builder.append("\n");

                let wire_type = get_wire_type(field);
                let tag = make_tag(field.number as u32, wire_type);
                builder.append("os.write_raw_varint32(");
                builder.append(tag.to_string().as_bytes());
                builder.append(").unwrap();\n"); //
                                                 //
                builder.append("os.write_raw_varint32(entry_size as u32).unwrap();\n");
                gen_serial_fn_inner(&key_field, true, true, builder);
                builder.append("\n");
                gen_serial_fn_inner(&value_field, true, false, builder);
                builder.append("\n");
                builder.append("}\n");
            }
        }
        FieldType::Message(_) => {
            if is_gen_map_type_size {
                builder.append(
                    "os.write_tag(2, x_com_lib::rt::WireType::LengthDelimited).unwrap();\n",
                );
                builder.append("os.write_raw_varint32(v.cached_size.get() as u32).unwrap();\n");
                builder.append("v.serial_with_output_stream(os).unwrap();\n");
            } else {
                let number = field.number;

                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_tag(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", x_com_lib::rt::WireType::LengthDelimited).unwrap();\n");
                    builder.append("os.write_raw_varint32(v.cached_size.get() as u32).unwrap();\n");
                    //
                    builder.append("v.serial_with_output_stream(os).unwrap();\n");

                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(".as_ref() { \n");

                    builder.append("os.write_tag(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", x_com_lib::rt::WireType::LengthDelimited).unwrap();\n");

                    builder.append("os.write_raw_varint32(v.cached_size.get() as u32).unwrap();\n");
                    //
                    builder.append("v.serial_with_output_stream(os).unwrap();\n");

                    builder.append("}\n")
                } else {
                    builder.append("{ \n");

                    builder.append("os.write_tag(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", x_com_lib::rt::WireType::LengthDelimited).unwrap();\n");

                    builder.append("os.write_raw_varint32(self.");
                    builder.append(field.name.clone());
                    builder.append(".cached_size.get() as u32).unwrap();\n");
                    //
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".serial_with_output_stream(os).unwrap();\n");

                    builder.append("}\n");
                }
            }
        }
        FieldType::Enum(_) => {
            if is_gen_map_type_size {
                builder.append("os.write_enum(2, v.unwrap() as i32).unwrap();");
            } else {
                let number = field.number;
                if is_array {
                    builder.append("for v in &self.");
                    builder.append(field.name.clone());
                    builder.append(" {\n");
                    builder.append("os.write_enum(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", v.unwrap() as i32).unwrap();\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append("if let Some(v) = self.");
                    builder.append(field.name.clone());
                    builder.append(".as_ref() { \n");
                    builder.append("os.write_enum(");
                    builder.append(number.to_string().as_bytes());
                    builder.append(", *v as i32).unwrap();\n");
                    builder.append("}\n")
                } else {
                    builder.append(" if let Some(value) = self.");
                    builder.append(field.name.clone());
                    builder.append(".as_ref() { \n");
                    builder.append("os.write_enum(");
                    builder.append(number.to_string().as_bytes());
                    // builder.append(", self.");
                    // builder.append(field.name.clone());
                    // builder.append(" as i32).unwrap();\n");

                    builder.append(", *value as i32).unwrap();\n");
                    builder.append("}\n");
                }
            }
        }
        _ => {}
    }
}

fn gen_serial_fn(struct_info: &Message, builder: &mut Builder) {
    builder.append("fn serial_with_output_stream(&self,  os: &mut x_com_lib::CodedOutputStream<'_>) -> Result<(), Status> { \n");

    //
    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();
        gen_serial_fn_inner(field, false, false, builder);
    }

    builder.append("Ok(())\n");

    builder.append("}\n\n");
}

fn gen_parse_fn_inner(
    field: &Field,
    is_gen_map_type_size: bool,
    is_key: bool,
    builder: &mut Builder,
) {
    let is_array = is_array(field);
    let is_optional = is_optional(field);
    let number = field.number;

    let wire_type = get_wire_type(field);
    let tag = make_tag(number as u32, wire_type);

    builder.append(tag.to_string().as_bytes());
    builder.append(" => {\n");

    match &field.typ {
        FieldType::Double => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_double().unwrap();\n");
                } else {
                    builder.append("value = is.read_double().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_double().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_double().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_double().unwrap();\n");
                }
            }
        }
        FieldType::Float => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_float().unwrap();\n");
                } else {
                    builder.append("value = is.read_float().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_float().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_float().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_float().unwrap();\n");
                }
            }
        }
        FieldType::Int64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_int64().unwrap();\n");
                } else {
                    builder.append("value = is.read_int64().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_int64().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_int64().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_int64().unwrap();\n");
                }
            }
        }
        FieldType::Int32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_int32().unwrap();\n");
                } else {
                    builder.append("value = is.read_int32().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_int32().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_int32().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_int32().unwrap();\n");
                }
            }
        }
        FieldType::Uint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_uint64().unwrap();\n");
                } else {
                    builder.append("value = is.read_uint64().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_uint64().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_uint64().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_uint64().unwrap();\n");
                }
            }
        }
        FieldType::Uint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_uint32().unwrap();\n");
                } else {
                    builder.append("value = is.read_uint32().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_uint32().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_uint32().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_uint32().unwrap();\n");
                }
            }
        }
        FieldType::Fixed64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_fixed64().unwrap();\n");
                } else {
                    builder.append("value = is.read_fixed64().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_fixed64().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_fixed64().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_fixed64().unwrap();\n");
                }
            }
        }
        FieldType::Fixed32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_fixed32().unwrap();\n");
                } else {
                    builder.append("value = is.read_fixed32().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_fixed32().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_fixed32().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_fixed32().unwrap();\n");
                }
            }
        }
        FieldType::Sfixed32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_sfixed32().unwrap();\n");
                } else {
                    builder.append("value = is.read_sfixed32().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_sfixed32().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_sfixed32().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_sfixed32().unwrap();\n");
                }
            }
        }
        FieldType::Sfixed64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_sfixed64().unwrap();\n");
                } else {
                    builder.append("value = is.read_sfixed64().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_sfixed64().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_sfixed64().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_sfixed64().unwrap();\n");
                }
            }
        }
        FieldType::Sint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_sint32().unwrap();\n");
                } else {
                    builder.append("value = is.read_sint32().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_sint32().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_sint32().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_sint32().unwrap();\n");
                }
            }
        }
        FieldType::Sint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_sint64().unwrap();\n");
                } else {
                    builder.append("value = is.read_sint64().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_sint64().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_sint64().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_sint64().unwrap();\n");
                }
            }
        }
        FieldType::Bool => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_bool().unwrap();\n");
                } else {
                    builder.append("value = is.read_bool().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_bool().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_bool().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_bool().unwrap();\n");
                }
            }
        }
        FieldType::String => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.read_string().unwrap();\n");
                } else {
                    builder.append("value = is.read_string().unwrap();\n");
                }
            } else {
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(is.read_string().unwrap());\n");
                } else if is_optional {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(is.read_string().unwrap());\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = is.read_string().unwrap();\n");
                }
            }
        }
        FieldType::Map(_map_type) => {
            if is_gen_map_type_size {
                builder.append("value.parse_from_input_stream(is)?;\n");
            } else {
                let (key_field, value_field) = gen_map_key_value_field(&field.typ).unwrap();

                builder.append("let len = is.read_raw_varint32();\n");

                builder.append("let len = len.unwrap();\n");

                builder.append("let old_limit = is.push_limit(len as u64).unwrap();\n");

                let key_default_value = proto_utils::protobuf_to_rust_default_value(&key_field);
                builder.append("let mut key = ");
                builder.append(key_default_value.as_bytes());
                builder.append(";\n");

                let value_default_value = proto_utils::protobuf_to_rust_default_value(&value_field);
                builder.append("let mut value = ");
                builder.append(value_default_value.as_bytes());
                builder.append(";\n");

                builder.append("while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {\n");

                builder.append("match tag {\n");

                gen_parse_fn_inner(&key_field, true, true, builder);

                gen_parse_fn_inner(&value_field, true, false, builder);

                builder.append("_ => x_com_lib::rt::skip_field_for_tag(tag, is).unwrap(),\n");

                builder.append("}\n");

                builder.append("}\n");

                builder.append("is.pop_limit(old_limit);\n");

                builder.append("self.");
                builder.append(field.name.clone());
                builder.append(".insert(key, value);\n");
            }
        }
        FieldType::Message(_) => {
            if is_gen_map_type_size {
                builder.append("value.parse_from_input_stream(is)?;\n");
            } else {
                let struct_name = field.typ.get_message_or_enum_path().unwrap();
                let struct_type = proto_utils::protobuf_path_to_rust(&struct_name);
                builder.append("let len = is.read_raw_varint64();\n");

                builder.append("let len = len.unwrap();\n");

                builder.append("let old_limit = is.push_limit(len);\n");

                builder.append("let old_limit = old_limit.unwrap();\n");

                if is_array {
                    // builder.append("let mut value = ");
                    // builder.append(struct_type.as_bytes());
                    // builder.append("::default();\n");

                    builder.append(format!(
                        "let mut value = Box::new({}::default());\n",
                        struct_type
                    ));

                    builder.append("value.parse_from_input_stream(is)?;\n");

                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(value);\n");
                } else if is_optional {
                    // builder.append("let mut value = ");
                    // builder.append(struct_type.as_bytes());
                    // builder.append("::default();\n");

                    builder.append(format!(
                        "let mut value = Box::new({}::default());\n",
                        struct_type
                    ));

                    builder.append("value.parse_from_input_stream(is)?;\n");

                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = Some(value);\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".parse_from_input_stream(is)?;\n");
                }
                builder.append("is.pop_limit(old_limit);\n");
            }
        }
        FieldType::Enum(_) => {
            let type_name = field.typ.get_message_or_enum_path().unwrap();

            let enum_type = proto_utils::protobuf_path_to_rust(&type_name);

            if is_gen_map_type_size {
                builder.append("value = ");
                builder.append(enum_type.as_bytes());
                builder.append("::from_i32(is.read_int32().unwrap());\n");
            } else {
                builder.append("let value = ");
                builder.append(enum_type.as_bytes());
                builder.append("::from_i32(is.read_int32().unwrap());\n");
                if is_array {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(".push(value);\n");
                } else {
                    builder.append("self.");
                    builder.append(field.name.clone());
                    builder.append(" = value;\n");
                }
            }
        }

        _ => {}
    }
    builder.append("}, \n");
}

fn gen_parse_fn(struct_info: &Message, builder: &mut Builder) {
    //
    builder.append("fn parse_from_input_stream(&mut self, is: &mut x_com_lib::CodedInputStream<'_>) -> Result<(), Status> { \n");
    //

    builder.append("while let Some(tag) = is.read_raw_tag_or_eof().unwrap() {\n");

    builder.append("match tag {\n");

    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();
        gen_parse_fn_inner(field, false, false, builder);
    }

    builder.append("_ => {\n");

    builder.append("let wire_type = extract_wire_type_from_tag(tag);\n");
    builder.append("if wire_type.is_none() {\n");
    builder.append("return Err(Status::error(\"消息格式出错\".into()));\n");
    builder.append("}\n");

    builder.append("let result = is.skip_field(wire_type.unwrap());\n");
    builder.append(" if let Err(err) = result {\n");
    builder.append("return Err(Status::error(err.to_string()));\n");
    builder.append("}\n");

    builder.append("} \n");

    builder.append("} \n");

    builder.append("} \n");

    builder.append("Ok(())\n");
    builder.append("}\n\n");
}

fn gen_debug_and_default_fn(struct_name: &str, struct_info: &Message, builder: &mut Builder) {
    builder.append("impl std::fmt::Debug for ");
    builder.append(struct_name.as_bytes());
    builder.append(" { \n");

    builder.append("fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {\n");

    builder.append("f.debug_struct(\"");
    builder.append(struct_name);
    builder.append("\")");

    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();
        if field.name == "cached_size" {
            continue;
        }
        builder.append(".field(\"");
        builder.append(field.name.clone());

        builder.append("\", &self.");
        builder.append(field.name.clone());

        builder.append(")\n");
    }

    builder.append(" .finish()\n");
    builder.append("} \n");
    builder.append("} \n\n");

    builder.append("impl Default for ");
    builder.append(struct_name.as_bytes());
    builder.append(" { \n");

    builder.append("fn default() -> Self { \n");

    builder.append(struct_name.as_bytes());

    builder.append("{\n");

    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();
        builder.append(field.name.clone());

        builder.append(": ");
        //
        let default_value = proto_utils::protobuf_to_rust_default_value(field);

        builder.append(default_value.as_bytes());

        builder.append(",\n");
    }
    //

    builder.append("cached_size: x_com_lib::rt::CachedSize::new(),\n");

    builder.append("}\n");

    builder.append("}\n\n");

    builder.append("} \n\n");
}

fn gen_struct_serial(struct_name: &str, struct_info: &Message, builder: &mut Builder) {
    let struct_name = proto_utils::protobuf_path_to_rust(struct_name);
    //
    gen_debug_and_default_fn(&struct_name, struct_info, builder);
    //
    builder.append("impl RequestMessage for ");
    builder.append(struct_name.as_bytes());
    builder.append(" { \n");
    // 计算大小
    gen_compute_size_fn(struct_info, builder);
    // 序列化代码
    gen_serial_fn(struct_info, builder);
    // 反序列化代码
    gen_parse_fn(struct_info, builder);
    builder.append("} \n\n");
}

/**
 * 生成序列化代码
 */
pub fn gen_serial(service_api_descriptor: &ServiceApiDescriptor, builder: &mut Builder) {
    // 放进来
    for (key, value) in &service_api_descriptor.message_descriptor_holder {
        gen_struct_serial(key, &value.descriptor_proto, builder);
    }
    //
    for (key, value) in &service_api_descriptor.enum_descriptor_map {
        gen_enum_serial(key, value, builder);
    }
}
