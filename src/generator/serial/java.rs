use std::collections::HashMap;

use dxc_protobuf_parser::parser::model::{Enumeration, Field, FieldType, Message};
use string_builder::Builder;
use x_common_lib::{
    serial::api_descriptor::{
        gen_map_key_value_field, get_inner_field, get_wire_type, is_array, is_optional, make_tag, DescriptorProtoHolder, ServiceApiDescriptor
    },
    utils::{file_utils, string_utils},
};

use crate::{
    generator::{put_message_in_nest_map, NestMessage},
    proto_utils::{self, proto_field_type_to_java_object, proto_type_to_java_map_entry},
};

// use super::has_sub_message;

fn gen_set_get_has_method(
    struct_info: &Message,
    _service_api_descriptor: &ServiceApiDescriptor,
    builder: &mut Builder,
) {
    // 生成 set/get/has 这些内容
    let mut cur_optional_index = 0;
    let mut cur_optional_num = 0;
    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();

        let is_optional = is_optional(field);
        //

        let set_func_name = format!("set_{}", field.name);
        let set_func_name = string_utils::convert_to_small_camel_case(&set_func_name);

        let field_type = proto_utils::protobuf_type_to_java(field);
        // set
        builder.append(format!(
            "public void {}({} value) {{\n",
            set_func_name, field_type
        ));

        builder.append(format!("this.{} = value;\n", field.name));
        if is_optional {
            if cur_optional_num >= 32 {
                cur_optional_index += 1;
                cur_optional_num = 0;
            }
            let optional_index = 1 << cur_optional_num;
            builder.append(format!(
                "this.bitField{}_ |= 0x{:08X};\n",
                cur_optional_index, optional_index
            ));
        }
        builder.append("}\n");

        // get
        let get_func_name = format!("get_{}", field.name);
        let get_func_name = string_utils::convert_to_small_camel_case(&get_func_name);

        builder.append(format!("public {} {}() {{\n", field_type, get_func_name,));

        builder.append(format!("return this.{};\n", field.name));

        builder.append("}\n");

        //  has
        if is_optional {
            let get_func_name = format!("has_{}", field.name);
            let has_func_name = string_utils::convert_to_small_camel_case(&get_func_name);

            builder.append(format!("public boolean {}() {{\n", has_func_name,));

            let optional_index = 1 << cur_optional_num;
            builder.append(format!(
                "return ((bitField{}_ & 0x{:08X}) != 0); \n",
                cur_optional_index, optional_index
            ));
            builder.append("}\n");
            //
            cur_optional_num += 1;
        }
    }
}

fn gen_compute_size_fn_inner(
    field: &Field,
    is_gen_map_type_size: bool,
    is_key: bool,
    builder: &mut Builder,
) {
    let is_array = is_array(field);
    let is_optional = is_optional(field);
    let number = field.number as u32;
    let tag = make_tag(number, 2);
    let size_len = ::protobuf::rt::compute_raw_varint64_size(tag as u64);
    let has_func_name = format!("has_{}", field.name);
    let has_func_name = string_utils::convert_to_small_camel_case(&has_func_name);
    let field_type_name = proto_utils::protobuf_type_to_java(field);

    match &field.typ {
        FieldType::Double => {
            if is_gen_map_type_size {
                builder.append(format!("entrySize += {};\n", size_len + 8));
            } else {
                if is_array {
                    //
                    builder.append(format!("if (this.{} != null) {{ \n", field.name));
                    builder.append(format!(
                        "my_size += this.{}.size() * {};\n",
                        field.name,
                        size_len + 8
                    ));
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                    //
                } else {
                    //
                    builder.append(format!("if (this.{} != 0.0) {{\n", field.name));
                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Float => {
            if is_gen_map_type_size {
                builder.append(format!("entrySize += {};\n", size_len + 4));
            } else {
                if is_array {
                    //
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!(
                        "my_size += this.{}.size() * {};\n",
                        field.name,
                        size_len + 4
                    ));
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("my_size += {};\n", size_len + 4));
                    builder.append("}\n");
                    //
                } else {
                    //
                    builder.append(format!("if (this.{} != 0.0) {{\n", field.name));
                    builder.append(format!("my_size += {};\n", size_len + 4));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Int64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entrySize += computeInt64Size(1, entry.getKey());\n");
                } else {
                    builder.append("entrySize += computeInt64Size(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Long value : this.{}) {{ \n", field.name));
                    builder.append(format!("my_size += computeInt64Size({}, value);\n", number));

                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "my_size += computeInt64Size({},  this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "my_size += computeInt64Size({}, this.{});\n",
                        number, field.name
                    ));

                    builder.append("}\n");
                }
            }
        }
        FieldType::Uint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entrySize += computeUInt64Size(1, entry.getKey());\n");
                } else {
                    builder.append("entrySize += computeUInt64Size(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Long value : this.{}) {{ \n", field.name));
                    builder.append(format!(
                        "my_size += computeUInt64Size({}, value);\n",
                        number
                    ));

                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "my_size += computeUInt64Size({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "my_size += computeUInt64Size({}, this.{});\n",
                        number, field.name
                    ));

                    builder.append("}\n");
                }
            }
        }
        FieldType::Int32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entrySize += computeInt32Size(1, entry.getKey());\n");
                } else {
                    builder.append("entrySize += computeInt32Size(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Integer value : this.{}) {{ \n", field.name));
                    builder.append(format!("my_size += computeInt32Size({}, value);\n", number));

                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "my_size += computeInt32Size({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "my_size += computeInt32Size({}, this.{});\n",
                        number, field.name
                    ));

                    builder.append("}\n");
                }
            }
        }
        FieldType::Fixed64 => {
            if is_gen_map_type_size {
                builder.append(format!("entrySize += {};\n", size_len + 8));
            } else {
                if is_array {
                    //
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!(
                        "my_size += this.{}.size() * {};\n",
                        field.name,
                        size_len + 8
                    ));
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                    //
                } else {
                    //
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Fixed32 => {
            if is_gen_map_type_size {
                builder.append(format!("entrySize += {};\n", size_len + 4));
            } else {
                if is_array {
                    //
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!(
                        "my_size += this.{}.size() * {};\n",
                        field.name,
                        size_len + 4
                    ));
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("my_size += {};\n", size_len + 4));
                    builder.append("}\n");
                    //
                } else {
                    //
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!("my_size += {};\n", size_len + 4));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Bool => {
            if is_gen_map_type_size {
                builder.append(format!("entrySize += {};\n", size_len + 1));
            } else {
                if is_array {
                    //
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!(
                        "my_size += this.{}.size() * {};\n",
                        field.name,
                        size_len + 1
                    ));
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("my_size += {};\n", size_len + 1));
                    builder.append("}\n");
                    //
                } else {
                    //
                    builder.append(format!("if (this.{}) {{\n", field.name));
                    builder.append(format!("my_size += {};\n", size_len + 1));
                    builder.append("}\n");
                }
            }
        }
        FieldType::String => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entrySize += computeStringSize(1, entry.getKey());\n");
                } else {
                    builder.append("entrySize += computeStringSize(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (String value : this.{}) {{ \n", field.name));
                    builder.append(format!(
                        "my_size += computeStringSize({}, value);\n",
                        number
                    ));

                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "my_size += computeStringSize({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!(
                        "if (this.{} != null && !this.{}.isEmpty()) {{\n",
                        field.name, field.name
                    ));
                    builder.append(format!(
                        "my_size += computeStringSize({}, this.{});\n",
                        number, field.name
                    ));

                    builder.append("}\n");
                }
            }
        }
        FieldType::Map(_map_type) => {
            if is_gen_map_type_size {
                builder.append("int len = entry.getValue().computeSize();\n");
                let full_code = format!(
                    "entrySize += {} + computeInt64SizeNoTag(len) + len;\n",
                    size_len
                );
                builder.append(full_code);
            } else {
                let entry_type = proto_type_to_java_map_entry(&field.typ);
                builder.append(format!("if (this.{} != null) {{\n", field.name));

                builder.append(format!(
                    "for ({} entry : this.{}.entrySet()) {{\n",
                    entry_type, field.name
                ));
                builder.append("int entrySize = 0; \n");

                let (key_field, value_field) = gen_map_key_value_field(&field.typ).unwrap();

       
                gen_compute_size_fn_inner(&key_field, true, true, builder);
                builder.append("\n");
                gen_compute_size_fn_inner(&value_field, true, false, builder);
                builder.append("\n");

                let full_code = format!(
                    "my_size += {} + computeInt64SizeNoTag(entrySize) + entrySize; \n",
                    size_len
                );
                builder.append(full_code);
                builder.append("}\n");
                builder.append("}\n");
            }
        }

        FieldType::Message(_) => {
            if is_gen_map_type_size {
                builder.append("int len = entry.getValue().computeSize();\n");

                let full_code = format!(
                    "entrySize += {} + computeInt64SizeNoTag(len) + len;\n",
                    size_len
                );
                builder.append(full_code);
            } else {
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));

                    let array_inner_type = proto_utils::proto_field_type_to_java_object(&field.typ);

                    builder.append(format!(
                        "for ({} value : this.{}) {{ \n",
                        array_inner_type, field.name
                    ));
                    builder.append("int len = value.computeSize();\n");
                    let full_code = format!(
                        "my_size += {} + computeInt64SizeNoTag(len) + len;\n",
                        size_len
                    );
                    builder.append(full_code);
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("int len = {}.computeSize();\n", field.name));
                    let full_code = format!(
                        "my_size += {} + computeInt64SizeNoTag(len) + len;\n",
                        size_len
                    );
                    builder.append(full_code);
                    builder.append("}\n");

                    //
                } else {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("int len = {}.computeSize();\n", field.name));
                    let full_code = format!(
                        "my_size += {} + computeInt64SizeNoTag(len) + len;\n",
                        size_len
                    );
                    builder.append(full_code);
                    builder.append("}\n");
                    //
                }
            }
        }
        FieldType::Uint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entrySize += computeUInt32Size(1, entry.getKey());\n");
                } else {
                    builder.append("entrySize += computeUInt32Size(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Integer value : this.{}) {{ \n", field.name));
                    builder.append(format!(
                        "my_size += computeUInt32Size({}, value);\n",
                        number
                    ));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "my_size += computeUInt32Size({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "my_size += computeUInt32Size({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Enum(_) => {
            if is_gen_map_type_size {
                builder.append("entrySize += computeInt32Size(2, entry.getValue().getValue());\n");
            } else {
                let number = field.number;

                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!(
                        "for ({} value : this.{}) {{ \n",
                        field_type_name, field.name
                    ));
                    builder.append(format!(
                        "my_size += computeInt32Size({}, value.getValue());\n",
                        number
                    ));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "my_size += computeInt32Size({}, this.{}.getValue());\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!(
                        "my_size += computeInt32Size({}, this.{}.getValue());\n",
                        number, field.name
                    ));

                    builder.append("}\n");
                }
            }
        }
        FieldType::Sfixed32 => {
            if is_gen_map_type_size {
                builder.append(format!("entrySize += {};\n", size_len + 4));
            } else {
                if is_array {
                    //
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!(
                        "my_size += this.{}.size() * {};\n",
                        field.name,
                        size_len + 4
                    ));
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("my_size += {};\n", size_len + 4));
                    builder.append("}\n");
                    //
                } else {
                    //
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!("my_size += {};\n", size_len + 4));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sfixed64 => {
            if is_gen_map_type_size {
                builder.append(format!("entrySize += {};\n", size_len + 8));
            } else {
                if is_array {
                    //
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!(
                        "my_size += this.{}.size() * {};\n",
                        field.name,
                        size_len + 8
                    ));
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                    //
                } else {
                    //
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!("my_size += {};\n", size_len + 8));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entrySize += computeSInt32Size(1, entry.getKey());\n");
                } else {
                    builder.append("entrySize += computeSInt32Size(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Integer value : this.{}) {{ \n", field.name));
                    builder.append(format!("my_size += computeSInt32Size({}, value)\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "my_size += computeSInt32Size({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "my_size += computeSInt32Size({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("entrySize += computeSInt64Size(1, entry.getKey());\n");
                } else {
                    builder.append("entrySize += computeSInt64Size(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;

                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Long value : this.{}) {{ \n", field.name));
                    builder.append(format!("my_size += computeSInt64Size({}, value)\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "my_size += computeSInt64Size({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "my_size += computeSInt64Size({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        _ => {}
    }
}

fn gen_compute_size_fn(struct_info: &Message, builder: &mut Builder) {
    builder.append("@Override\n");

    builder.append("public int computeSize() {\n");

    builder.append(
        r#"if (cacheSize != 0) {
        return cacheSize;
    }
"#,
    );
    builder.append("int my_size = 0;\n");

    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();
        gen_compute_size_fn_inner(field, false, false, builder);
    }

    builder.append("cacheSize = my_size;\n");

    builder.append("return my_size;\n");

    builder.append("}\n");
}

fn gen_serial_fn_inner(
    field: &Field,
    is_gen_map_type_size: bool,
    is_key: bool,
    builder: &mut Builder,
) {
    let is_array = is_array(field);
    let is_optional = is_optional(field);

    let has_func_name = format!("has_{}", field.name);
    let has_func_name = string_utils::convert_to_small_camel_case(&has_func_name);

    let field_type_name = proto_utils::protobuf_type_to_java(field);

    match &field.typ {
        FieldType::Double => {
            if is_gen_map_type_size {
                builder.append("os.writeDouble(2, entry.getValue());\n");
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Double value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeDouble({}, value);\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "os.writeDouble({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0.0) {{\n", field.name));
                    builder.append(format!(
                        "os.writeDouble({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Float => {
            if is_gen_map_type_size {
                builder.append("os.writeFloat(2, entry.getValue());\n");
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Float value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeFloat({}, value);\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("os.writeFloat({}, this.{});\n", number, field.name));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0.0) {{\n", field.name));
                    builder.append(format!("os.writeFloat({}, this.{});\n", number, field.name));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Int64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeInt64(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeInt64(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Long value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeInt64({}, value);\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("os.writeInt64({}, this.{});\n", number, field.name));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!("os.writeInt64({}, this.{});\n", number, field.name));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Uint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeUInt64(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeUInt64(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Long value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeUInt64({}, value);\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "os.writeUInt64({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "os.writeUInt64({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Int32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeInt32(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeInt32(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Integer value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeInt32({}, value);\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("os.writeInt32({}, this.{});\n", number, field.name));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!("os.writeInt32({}, this.{});\n", number, field.name));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Fixed64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeFixed64(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeFixed64(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Long value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeFixed64({}, value);\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "os.writeFixed64({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "os.writeFixed64({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Fixed32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeFixed32(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeFixed32(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Integer value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeFixed32({}, value);\n", number));

                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "os.writeFixed32({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "os.writeFixed32({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Bool => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeBool(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeBool(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Boolean value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeBool({}, value);\n", number));

                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!("os.writeBool({}, this.{});\n", number, field.name));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{}) {{\n", field.name));
                    builder.append(format!("os.writeBool({}, true);\n", number));
                    builder.append("}\n");
                }
            }
        }
        FieldType::String => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeString(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeString(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (String value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeString({}, value);\n", number));

                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "os.writeString({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!(
                        "if (this.{} != null && !this.{}.isEmpty()) {{\n",
                        field.name, field.name
                    ));
                    builder.append(format!(
                        "os.writeString({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Map(_map_type) => {
            if is_gen_map_type_size {
                builder.append("os.writeTag(2, WireFormat.WIRETYPE_LENGTH_DELIMITED);\n");

                builder.append("os.writeInt32NoTag(entry.getValue().computeSize());\n");
                //

                builder.append("entry.getValue().serialWithOutputStream(os);\n");
            } else {
                let number = field.number;

                let (key_field, value_field) = gen_map_key_value_field(&field.typ).unwrap();


                let entry_type = proto_type_to_java_map_entry(&field.typ);

                builder.append(format!("if (this.{} != null) {{\n", field.name));

                builder.append(format!(
                    "for ({} entry : this.{}.entrySet()) {{\n",
                    entry_type, field.name
                ));
                builder.append("int entrySize = 0; \n");
                gen_compute_size_fn_inner(&key_field, true, true, builder);
                builder.append("\n");
                //
                gen_compute_size_fn_inner(&value_field, true, false, builder);
                builder.append("\n");
                //
                let wire_type = get_wire_type(field);
                let tag = make_tag(number as u32, wire_type);
                builder.append("os.writeInt32NoTag(");
                builder.append(tag.to_string().as_bytes());
                builder.append(");\n"); //
                builder.append("os.writeInt32NoTag(entrySize);\n");

                gen_serial_fn_inner(&key_field, true, true, builder);
                builder.append("\n");

                gen_serial_fn_inner(&value_field, true, false, builder);
                builder.append("\n");
                builder.append("}\n");

                builder.append("}\n");
            }
        }
        FieldType::Message(_) => {
            if is_gen_map_type_size {
                builder.append("os.writeTag(2, WireFormat.WIRETYPE_LENGTH_DELIMITED);\n");

                builder.append("os.writeInt32NoTag(entry.getValue().computeSize());\n");
                //

                builder.append("entry.getValue().serialWithOutputStream(os);\n");
            } else {
                // map
                let number = field.number;

                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));

                    let array_inner_type = proto_utils::proto_field_type_to_java_object(&field.typ);

                    builder.append(format!(
                        "for ({} value : this.{}) {{ \n",
                        array_inner_type, field.name
                    ));

                    builder.append(format!(
                        "os.writeTag({}, WireFormat.WIRETYPE_LENGTH_DELIMITED);\n",
                        number
                    ));

                    builder.append("os.writeInt32NoTag(value.computeSize());\n");

                    builder.append("value.serialWithOutputStream(os);\n");

                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));

                    builder.append(format!(
                        "os.writeTag({}, WireFormat.WIRETYPE_LENGTH_DELIMITED);\n",
                        number
                    ));

                    builder.append(format!(
                        "os.writeInt32NoTag(this.{}.computeSize());\n",
                        field.name
                    ));

                    builder.append(format!("this.{}.serialWithOutputStream(os);\n", field.name));

                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));

                    builder.append(format!(
                        "os.writeTag({}, WireFormat.WIRETYPE_LENGTH_DELIMITED);\n",
                        number
                    ));

                    builder.append(format!(
                        "os.writeInt32NoTag(this.{}.computeSize());\n",
                        field.name
                    ));

                    builder.append(format!("this.{}.serialWithOutputStream(os);\n", field.name));

                    builder.append("}\n");
                }
            }
        }
        FieldType::Uint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeUInt32(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeUInt32(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Integer value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeUInt32({}, value);\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "os.writeUInt32({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "os.writeUInt32({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Enum(_) => {
            if is_gen_map_type_size {
                builder.append("os.writeEnum(2, entry.getValue().getValue());\n");
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!(
                        "for ({} value : this.{}) {{ \n",
                        field_type_name, field.name
                    ));
                    builder.append(format!("os.writeEnum({}, value.getValue());\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "os.writeEnum({}, this.{}.getValue());\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!(
                        "os.writeEnum({}, this.{}.getValue());\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sfixed32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeSFixed32(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeSFixed32(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));

                    builder.append(format!("for (Integer value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeSFixed32({}, value);\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "os.writeSFixed32({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "os.writeSFixed32({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sfixed64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeSFixed64(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeSFixed64(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Long value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeSFixed64({}, value);\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "os.writeSFixed64({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "os.writeSFixed64({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeSInt32(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeSInt32(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Integer value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeSInt32({}, value);\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "os.writeSInt32({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "os.writeSInt32({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        FieldType::Sint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("os.writeSInt64(1, entry.getKey());\n");
                } else {
                    builder.append("os.writeSInt64(2, entry.getValue());\n");
                }
            } else {
                let number = field.number;
                if is_array {
                    builder.append(format!("if (this.{} != null) {{\n", field.name));
                    builder.append(format!("for (Long value : this.{}) {{ \n", field.name));
                    builder.append(format!("os.writeSInt64({}, value);\n", number));
                    builder.append("}\n");
                    builder.append("}\n");
                } else if is_optional {
                    builder.append(format!("if ({}()) {{\n", has_func_name));
                    builder.append(format!(
                        "os.writeSInt64({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                } else {
                    builder.append(format!("if (this.{} != 0) {{\n", field.name));
                    builder.append(format!(
                        "os.writeSInt64({}, this.{});\n",
                        number, field.name
                    ));
                    builder.append("}\n");
                }
            }
        }
        _ => {}
    }
}
fn gen_serial_fn(struct_info: &Message, builder: &mut Builder) {
    builder.append("@Override\n");

    builder
        .append("public void serialWithOutputStream(CodedOutputStream os) throws IOException {\n");

    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();

        gen_serial_fn_inner(field, false, false, builder);
    }
    builder.append("}\n");
}

fn gen_parse_fn_inner(
    field: &Field,
    is_gen_map_type_size: bool,
    is_key: bool,
    builder: &mut Builder,
) {
    let is_array = is_array(field);
    let number = field.number;

    let wire_type = get_wire_type(field);
    let tag = make_tag(number as u32, wire_type);
    //

    builder.append(format!("case {}: {{\n", tag));

    match &field.typ {
        FieldType::Double => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readDouble();\n");
                } else {
                    builder.append("value = is.readDouble();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readDouble());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readDouble();", field.name));
                }
            }
        }
        FieldType::Float => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readFloat();\n");
                } else {
                    builder.append("value = is.readFloat();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readFloat());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readFloat();", field.name));
                }
            }
        }
        FieldType::Int64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readInt64();\n");
                } else {
                    builder.append("value = is.readInt64();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readInt64());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readInt64();", field.name));
                }
            }
        }
        FieldType::Uint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readUInt64();\n");
                } else {
                    builder.append("value = is.readUInt64();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readUInt64());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readUInt64();", field.name));
                }
            }
        }
        FieldType::Int32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readInt32();\n");
                } else {
                    builder.append("value = is.readInt32();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readInt32());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readInt32();", field.name));
                }
            }
        }
        FieldType::Fixed64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readFixed64();\n");
                } else {
                    builder.append("value = is.readFixed64();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readFixed64());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readFixed64();", field.name));
                }
            }
        }
        FieldType::Fixed32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readFixed32();\n");
                } else {
                    builder.append("value = is.readFixed32();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readFixed32());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readDouble();", field.name));
                }
            }
        }
        FieldType::Bool => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readBool();\n");
                } else {
                    builder.append("value = is.readBool();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readBool());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readBool();", field.name));
                }
            }
        }
        FieldType::String => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readString();\n");
                } else {
                    builder.append("value = is.readString();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readString());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readString();", field.name));
                }
            }
        }
        FieldType::Map(_map_type) => {
            if is_gen_map_type_size {
                builder.append("value.parseFromInputStream(is);\n");
            } else {
                builder.append(format!("if (this.{} == null) {{\n", field.name));
                builder.append(format!("this.{} = new HashMap<>();\n", field.name));
                builder.append("}\n");

                builder.append("int len = is.readRawVarint32();\n");

                builder.append("int oldLimit = is.pushLimit(len);\n");

                let (key_field, value_field) = gen_map_key_value_field(&field.typ).unwrap();

     

                let key_object_type = proto_field_type_to_java_object(&key_field.typ);
                builder.append(format!("{}  key = null;\n", key_object_type));

                let value_object_type = proto_field_type_to_java_object(&value_field.typ);

                builder.append(format!("{}  value = null;\n", value_object_type));
                builder.append("while (true) {\n");
                builder.append("int mapTag = is.readTag();\n");
                builder.append("if (mapTag == 0) {\n");
                builder.append("break;\n");
                builder.append("}\n");

                builder.append("switch (mapTag) {\n");

                gen_parse_fn_inner(&key_field, true, true, builder);
                gen_parse_fn_inner(&value_field, true, false, builder);

                builder.append("default:\n");

                builder.append("is.skipField(tag);\n");

                builder.append("break;\n");

                builder.append("}\n");

                builder.append("}\n");

                builder.append("is.popLimit(oldLimit);\n");

                builder.append(format!("this.{}.put(key, value);\n", field.name));
            }
        }
        FieldType::Message(_) => {
            if is_gen_map_type_size {
                builder.append("value.parseFromInputStream(is);\n");
            } else {
                let struct_name = &field.name;

                let struct_type = proto_utils::protobuf_path_to_java(struct_name);
                builder.append("long len = is.readRawVarint64();\n");

                builder.append("int oldLimit = is.pushLimit((int)len);\n");

                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");
                    builder.append(format!("{}  value = new {}();\n", struct_type, struct_type));
                    builder.append("value.parseFromInputStream(is);\n");
                    builder.append(format!("this.{}.add(value);\n", field.name));
                } else {
                    builder.append(format!("{}  value = new {}();\n", struct_type, struct_type));
                    builder.append("value.parseFromInputStream(is);\n");
                    builder.append(format!("this.{} = value;\n", field.name));
                }
                builder.append("is.popLimit(oldLimit);\n");
            }
        }
        FieldType::Uint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readUInt32();\n");
                } else {
                    builder.append("value = is.readUInt32();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readUInt32());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readUInt32();", field.name));
                }
            }
        }
        FieldType::Enum(path) => {
            let type_name = path.get_path();
            let enum_type = proto_utils::protobuf_path_to_java(&type_name);
            if is_gen_map_type_size {
                builder.append(format!("value = {}.valueOf(is.readInt32());\n", enum_type));
            } else {
                builder.append(format!(
                    "{} value = {}.valueOf(is.readInt32());\n",
                    enum_type, enum_type
                ));
                if is_array {
                    builder.append(format!("this.{}.add(value);", field.name));
                } else {
                    builder.append(format!("this.{} = value;\n", field.name));
                }
            }
        }
        FieldType::Sfixed32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readSFixed32();\n");
                } else {
                    builder.append("value = is.readSFixed32();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readSFixed32());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readSFixed32();", field.name));
                }
            }
        }
        FieldType::Sfixed64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readSFixed64();\n");
                } else {
                    builder.append("value = is.readSFixed64();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");
                    builder.append(format!("this.{}.add(is.readSFixed64());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readSFixed64();", field.name));
                }
            }
        }
        FieldType::Sint32 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readSInt32();\n");
                } else {
                    builder.append("value = is.readSInt32();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readSInt32());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readSInt32();", field.name));
                }
            }
        }
        FieldType::Sint64 => {
            if is_gen_map_type_size {
                if is_key {
                    builder.append("key = is.readSInt64();\n");
                } else {
                    builder.append("value = is.readSInt64();\n");
                }
            } else {
                if is_array {
                    builder.append(format!("if (this.{} == null) {{\n", field.name));
                    builder.append(format!("this.{} = new ArrayList<>();\n", field.name));
                    builder.append("}\n");

                    builder.append(format!("this.{}.add(is.readSInt64());", field.name));
                } else {
                    builder.append(format!("this.{} = is.readSInt64();", field.name));
                }
            }
        }
        _ => {}
    }

    builder.append("}\n");
}
fn gen_parse_fn(struct_info: &Message, builder: &mut Builder) {
    builder.append("@Override\n");

    builder.append("public void parseFromInputStream(CodedInputStream is) throws IOException {\n");

    builder.append("boolean done = false;\n");
    builder.append("while (!done) {\n");

    builder.append("int tag = is.readTag();\n");

    builder.append("switch (tag) {\n");

    let mut cur_optional_index = 0;
    let mut cur_optional_num = 0;

    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();
        gen_parse_fn_inner(field, false, false, builder);
        //
        if is_optional(field) {
            if cur_optional_num >= 32 {
                cur_optional_index += 1;
                cur_optional_num = 0;
            }
            let optional_index = 1 << cur_optional_num;
            builder.append(format!(
                "this.bitField{}_ |= 0x{:08X};\n",
                cur_optional_index, optional_index
            ));

            cur_optional_num += 1;
        }

        builder.append(" break;\n");
    }

    builder.append("default:\n");
    builder.append("done = true;\n");
    builder.append(" break;\n");
    builder.append("}\n");
    builder.append("}\n");
    builder.append("}\n");
}

fn gen_serial_method(
    struct_info: &Message,
    _service_api_descriptor: &ServiceApiDescriptor,
    builder: &mut Builder,
) {
    gen_compute_size_fn(struct_info, builder);

    // 序列化代码
    gen_serial_fn(struct_info, builder);

    // 反序列化代码
    gen_parse_fn(struct_info, builder);
}

//
fn gen_class(
    nest_message: &NestMessage,
    struct_info: &DescriptorProtoHolder,
    service_api_descriptor: &ServiceApiDescriptor,
    builder: &mut Builder,
    is_inner: bool,
) {
    let struct_info = &struct_info.descriptor_proto;
    let struct_name = struct_info.name.replace(".", "");
    if is_inner {
        builder.append("static public class ");
    } else {
        builder.append("public class ");
    }
    builder.append(struct_name.as_bytes());
    builder.append(" extends RequestMessage { \n");
    //

    if !nest_message.sub_message.is_empty() {
        gen_class_and_enum_3(nest_message, service_api_descriptor, builder);
    }

    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();
        builder.append("private ");
        builder.append(proto_utils::protobuf_type_to_java(field));
        builder.append(" ");
        builder.append(field.name.as_bytes());
        builder.append(";\n");
        //设置名字
    }
    builder.append("private int cacheSize;\n");
    // 生成 bitFields_

    let mut cur_optional_index = 0;
    let mut cur_optional_num = 0;

    for field in &struct_info.fields {
      let field = get_inner_field(field).unwrap();
      
        if is_optional(field) {
            if cur_optional_num == 0 {
                builder.append(format!("private int bitField{}_;\n", cur_optional_index));
            }
            cur_optional_num += 1;
            if cur_optional_num >= 32 {
                cur_optional_index += 1;
                cur_optional_num = 0;
            }
        }
    }
    // 生成 set/get/has 这些内容
    gen_set_get_has_method(struct_info, service_api_descriptor, builder);
    //

    gen_serial_method(struct_info, service_api_descriptor, builder);

    builder.append("}");
}

fn gen_class_and_enum_3(
    nest_message: &NestMessage,
    service_api_descriptor: &ServiceApiDescriptor,
    builder: &mut Builder,
) {
    for (_key, value) in &nest_message.sub_message {
        if value.is_enum {
            gen_enum(
                service_api_descriptor
                    .enum_descriptor_map
                    .get(&value.message_name)
                    .unwrap(),
                builder,
                true,
            );
            // 枚举
        } else {
            gen_class(
                value,
                service_api_descriptor
                    .message_descriptor_holder
                    .get(&value.message_name)
                    .unwrap(),
                service_api_descriptor,
                builder,
                true,
            );
        }

        // if has_sub_message(&value.sub_message, service_api_descriptor) {
          if !value.sub_message.is_empty() {
            gen_class_and_enum_3(value, service_api_descriptor, builder);
        }
    }
}

fn gen_enum(enum_info: &Enumeration, builder: &mut Builder, _is_inner: bool) {
    let enum_name = enum_info.name.replace(".", "");
    builder.append(format!("public enum {} {{ \n", enum_name));
    for (i, field) in enum_info.values.iter().enumerate() {
        if i == enum_info.values.len() - 1 {
            builder.append(format!("{}({});\n", field.name, field.number));
        } else {
            builder.append(format!("{}({}),\n", field.name, field.number));
        }
    }

    builder.append("private final int value;\n");
    builder.append("public int getValue() {\n");
    builder.append("  return this.value;\n");
    builder.append("}\n");

    builder.append(format!("{}(int value) {{\n", enum_name));
    builder.append("  this.value = value;\n");
    builder.append("}\n");

    builder.append(format!("static {} valueOf(int value) {{\n", enum_name));
    builder.append(" switch (value) {\n");

    for (_i, field) in enum_info.values.iter().enumerate() {
        builder.append(format!("case {}: \n", field.number));
        builder.append(format!("return {};\n", field.name));
    }

    builder.append(" default:\n");
    builder.append("return null;\n");

    builder.append("}\n");

    builder.append("}\n");

    builder.append("}\n");
}

fn gen_class_and_enum_2(
    nest_message: &NestMessage,
    service_api_descriptor: &ServiceApiDescriptor,
    package_name: &str,
    project_path: &str,
) {
    for (_key, value) in &nest_message.sub_message {
        let mut builder = Builder::new(1024 * 1024);

        builder.append(format!("package {};\n", package_name));

        // builder.append("package io.dxmesh.x_com.source_api;\n");
        if value.is_enum {
            gen_enum(
                service_api_descriptor
                    .enum_descriptor_map
                    .get(&value.message_name)
                    .unwrap(),
                &mut builder,
                false,
            );
            // 枚举
        } else {
            builder.append("import com.google.protobuf.CodedInputStream;\n");
            builder.append("import com.google.protobuf.CodedOutputStream;\n");

            builder.append("import com.google.protobuf.WireFormat;\n");

            builder.append("import io.dxmesh.serial.RequestMessage;\n");
            builder.append("import java.util.ArrayList;\n");
            builder.append("import java.util.HashMap;\n");
            builder.append("import java.io.IOException;\n");
            builder.append("import static com.google.protobuf.CodedOutputStream.*;\n");

            gen_class(
                value,
                service_api_descriptor
                    .message_descriptor_holder
                    .get(&value.message_name)
                    .unwrap(),
                service_api_descriptor,
                &mut builder,
                false,
            );
        }

        let struct_name = value.message_name.replace(".", "");
        //
        let source_api_file_path = format!("{}/{}.java", project_path, struct_name);

        file_utils::create_file_path_sync(&source_api_file_path);

        file_utils::write_content_to_file_sync(&source_api_file_path, &builder.string().unwrap())
            .unwrap();
    }
}

pub fn gen_class_and_enum(
    service_api_descriptor: &ServiceApiDescriptor,
    package_name: &str,
    project_path: &str,
) {
    let mut nest_message = NestMessage {
        message_name: String::new(),
        sub_message: HashMap::new(),
        is_enum: false,
    };

    // 放进来
    for message_name in service_api_descriptor.message_descriptor_holder.keys() {
        put_message_in_nest_map(message_name, &mut nest_message, false);
    }
    //
    for enum_name in service_api_descriptor.enum_descriptor_map.keys() {
        // 排序
        put_message_in_nest_map(enum_name, &mut nest_message, true);
    }

    gen_class_and_enum_2(
        &nest_message,
        service_api_descriptor,
        package_name,
        project_path,
    );
}
