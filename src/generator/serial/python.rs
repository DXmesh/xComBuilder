use crate::generator::string_utils;
use crate::generator::{put_message_in_nest_map, NestMessage};
use crate::proto_utils;
use dxc_protobuf_parser::parser::model::Enumeration;
use std::collections::HashMap;
use string_builder::Builder;
use x_common_lib::serial::api_descriptor::{get_inner_field, DescriptorProtoHolder};
use x_common_lib::serial::api_descriptor::{is_optional, ServiceApiDescriptor};

use super::{sort_message, update_sort_level, SortMessage};

fn gen_enum(enum_info: &Enumeration, builder: &mut Builder, space_num: usize) {
    let enum_name = enum_info.name.replace(".", "");
    builder.append(" ".repeat(space_num));
    builder.append(format!("class {}(IntEnum):\n", enum_name));
    for field in &enum_info.values {
        builder.append(" ".repeat(space_num + 2));
        builder.append(field.name.as_bytes());
        builder.append(" = ");
        builder.append(field.number.to_string().as_bytes());
        builder.append("\n");
    }
    builder.append("\n");
}

fn gen_class(
    struct_info: &DescriptorProtoHolder,
    _service_api_descriptor: &ServiceApiDescriptor,
    builder: &mut Builder,
    space_num: usize,
) {
    let struct_info = &struct_info.descriptor_proto;
    let struct_name = struct_info.name.replace(".", "");
    builder.append(" ".repeat(space_num));
    builder.append(format!("class {}:\n", struct_name));
    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();
        builder.append(" ".repeat(space_num + 2));
        builder.append(field.name.as_bytes());
        builder.append(": ");
        builder.append(proto_utils::protobuf_type_to_python(field));
        // let default_value = proto_utils::protobuf_type_to_python_default_value(field);
        // if !default_value.is_empty() {
        //   builder.append(" = ");
        //   builder.append(default_value);
        // }
        //
        builder.append("\n");
    }
    builder.append(" ".repeat(space_num + 2));
    builder.append("def __init__(self) -> None:\n");
    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();

        if is_optional(field) {
            builder.append(" ".repeat(space_num + 3));
            builder.append(format!("self.{} = None\n", field.name));
        } else {
            let default_value = proto_utils::protobuf_type_to_python_default_value(field);

            if default_value.is_empty() {
                continue;
            }

            builder.append(" ".repeat(space_num + 3));

            builder.append(format!("self.{} = {}\n", field.name, default_value));
        }
    }

    // field.options

    builder.append("\n\n");
}
fn gen_class_and_enum_2(
    nest_message: &NestMessage,
    builder: &mut Builder,
    service_api_descriptor: &ServiceApiDescriptor,
    space_num: usize,
    sort_obj: &SortMessage,
) {
    let mut sub_sort_map: HashMap<String, i32> = HashMap::new();
    for (_key, value) in &nest_message.sub_message {
        let sort_level = sort_obj.sort_map.get(&value.message_name);
        if sort_level.is_some() {
            sub_sort_map.insert(value.message_name.clone(), sort_level.unwrap().clone());
        } else {
            sub_sort_map.insert(value.message_name.clone(), 0);
        }
    }
    //
    let mut keys: Vec<_> = sub_sort_map.iter().collect();

    keys.sort_by(|a, b| b.1.cmp(a.1));

    let sorted_keys: Vec<_> = keys.iter().map(|(k, _)| k).collect();

    for message_key in sorted_keys {
        let words: Vec<&str> = message_key.split(".").filter(|&s| !s.is_empty()).collect();

        let last_word = words.last().unwrap();

        let snake_word = string_utils::convert_to_snake_case(last_word);

        let value = nest_message.sub_message.get(&snake_word);
        if value.is_none() {
            continue;
        }

        let value = value.unwrap();

        if value.is_enum {
            gen_enum(
                service_api_descriptor
                    .enum_descriptor_map
                    .get(&value.message_name)
                    .unwrap(),
                builder,
                space_num,
            );
        } else {
            //
            gen_class(
                service_api_descriptor
                    .message_descriptor_holder
                    .get(&value.message_name)
                    .unwrap(),
                service_api_descriptor,
                builder,
                space_num,
            );
        }

        if !value.sub_message.is_empty() {
            gen_class_and_enum_2(
                &value,
                builder,
                service_api_descriptor,
                space_num + 2,
                sort_obj,
            );
        }
    }
}

//
pub fn gen_class_and_enum(service_api_descriptor: &ServiceApiDescriptor, builder: &mut Builder) {
    builder.append("from typing import Dict\n");
    builder.append("from typing import List\n");
    builder.append("from enum import IntEnum\n");
    builder.append("\n\n");

    let mut nest_message = NestMessage {
        message_name: String::new(),
        sub_message: HashMap::new(),
        is_enum: false,
    };
    // 放进来
    for message_name in service_api_descriptor.message_descriptor_holder.keys() {
        put_message_in_nest_map(message_name, &mut nest_message, false);
    }
    //
    for enum_name in service_api_descriptor.enum_descriptor_map.keys() {
        put_message_in_nest_map(enum_name, &mut nest_message, true);
    }
    let mut sort_obj = SortMessage {
        sort_map: HashMap::new(),
        ref_map: HashMap::new(),
    };
    sort_message(&mut nest_message, service_api_descriptor, &mut sort_obj);

    //
    update_sort_level(&mut sort_obj);
    //  根据引用来排序
    //  排序

    gen_class_and_enum_2(&nest_message, builder, service_api_descriptor, 0, &sort_obj);
}
