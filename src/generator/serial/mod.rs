pub mod java;
pub mod python;
pub mod rust;
use super::NestMessage;
use std::collections::{HashMap, HashSet};
use x_common_lib::serial::api_descriptor::{
    gen_map_value_field, get_inner_field, is_map, ServiceApiDescriptor,
};
pub struct SortMessage {
    sort_map: HashMap<String, i32>,
    ref_map: HashMap<String, HashSet<String>>,
}

// fn has_sub_message(
//     sub_message: &HashMap<String, NestMessage>,
//     service_api_descriptor: &ServiceApiDescriptor,
// ) -> bool {
//     if sub_message.is_empty() {
//         return false;
//     }

//     for (_key, value) in sub_message {
//         if value.is_enum {
//             //
//             continue;
//         }
//         let struct_info = service_api_descriptor
//             .message_descriptor_holder
//             .get(&value.message_name);

//         info!("struct_info = {:?}", struct_info);
//         if struct_info.is_none() {
//             continue;
//         }

//         let struct_info = struct_info.unwrap();
//         let struct_info = &struct_info.descriptor_proto;

//         if !struct_info.messages.is_empty() {
//             return true;
//         }
//     }

//     false
// }

fn update_sort_level(sort_obj: &mut SortMessage) {
    for (key, value) in &sort_obj.ref_map {
        //

        let sort_level = sort_obj.sort_map.get(key).unwrap();

        let sort_level = *sort_level;
        //

        for ref_message in value {
            let ref_sort_level = sort_obj.sort_map.get_mut(ref_message);
            if ref_sort_level.is_none() {
                continue;
            }

            let ref_sort_level = ref_sort_level.unwrap();

            if sort_level >= *ref_sort_level {
                *ref_sort_level = sort_level + 1;
            }
        }
    }
}

//
fn sort_message(
    nest_message: &mut NestMessage,
    service_api_descriptor: &ServiceApiDescriptor,
    sort_obj: &mut SortMessage,
) {
    for (_key, value) in &mut nest_message.sub_message {
        if value.is_enum {
            sort_obj
                .sort_map
                .entry(value.message_name.clone())
                .or_insert(9999);
        } else {
            //
            sort_obj
                .sort_map
                .entry(value.message_name.clone())
                .or_insert(0);

            let struct_info = service_api_descriptor
                .message_descriptor_holder
                .get(&value.message_name)
                .unwrap();
            //
            let struct_info = &struct_info.descriptor_proto;

            for field in &struct_info.fields {
                let field = get_inner_field(field).unwrap();
                let real_field = if is_map(field) {
                    gen_map_value_field(&field.typ).unwrap()
                } else {
                    field.clone()
                };
                
    
                let type_name = real_field.typ.get_message_or_enum_path();

                if type_name.is_none() {
                    continue;
                }

                let type_name = type_name.unwrap();

                let words: Vec<&str> = type_name.split(".").filter(|&s| !s.is_empty()).collect();

                let mut message_name = String::default();
                // 记录依赖的
                for word in words {
                    message_name = format!("{}.{}", message_name, word);
                    //
                    let ref_set = sort_obj
                        .ref_map
                        .entry(value.message_name.clone())
                        .or_insert(HashSet::new());
                    ref_set.insert(message_name.clone());

                    let sort_level = sort_obj.sort_map.entry(message_name.clone()).or_insert(0);
                    //
                    *sort_level = *sort_level + 1;
                }
            }
            //
        }

      
        if !value.sub_message.is_empty() {
            sort_message(value, service_api_descriptor, sort_obj);
        }
    }
}
