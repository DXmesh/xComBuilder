use std::collections::HashMap;
use dxc_protobuf_parser::parser::model::Enumeration;
use semver::Version;
use string_builder::Builder;
use tracing::error;
use x_common_lib::{
    serial::api_descriptor::{self, get_inner_field, DescriptorProtoHolder, ServiceApiDescriptor},
    utils::{file_utils, string_utils::extract_dxc_name_and_version},
};

use crate::proto_utils::{self, proto_field_type_to_ts};

use super::{put_message_in_nest_map, NestMessage};

pub async fn gen_http_api(input_path: String, output_path: String) {
    let input_path = if input_path == "./" {
        "./protos".into()
    } else {
        input_path
    };

    let output_path = if output_path == "./" {
        "./src/assets/api".into()
    } else {
        output_path
    };

    let input_path = file_utils::get_absoule_path(&input_path).unwrap();

    if !file_utils::is_dir_exist(&output_path).await {
        file_utils::create_dir(&output_path).await;
    }

    let output_path = file_utils::get_absoule_path(&output_path).unwrap();

    let file_list = file_utils::get_file_list(&input_path).await;

    if file_list.is_err() {
        panic!("获取路径 {} 下的 api 描述文件列表失败！", input_path);
    }
    //

    for filename in file_list.unwrap() {
        if !filename.starts_with("import-api-") {
            continue;
        }

        let sdk_file_full_path = format!("{}/{}", input_path, filename);
        let service_api_descriptor =
            ServiceApiDescriptor::load_with_include_path(sdk_file_full_path.as_str(), true);

        if service_api_descriptor.is_err() {
            let err = service_api_descriptor.err().unwrap();
            error!("解析描述文件 {} 出错：{}...", sdk_file_full_path, err);

            return;
        }

        let service_api_descriptor = service_api_descriptor.unwrap();
        //

        let dxc_name_and_version = filename.replace("import-api-", "").replace(".proto", "");

        let result = extract_dxc_name_and_version(&dxc_name_and_version);
        if result.is_none() {
            panic!("无法识别：{} 版本号！", filename);
        }

        let (dxc_name, dxc_version) = result.unwrap();

        let version = Version::parse(&dxc_version);

        if version.is_err() {
            panic!("无法识别：{} 版本号:{}", filename, &dxc_version);
        }

        let mut builder = Builder::new(1024 * 1024);
        gen_http_code(
            &service_api_descriptor,
            &dxc_name_and_version,
            &dxc_version,
            &mut builder,
        );


        let api_file_output_path =
            format!("{}/{}_{}.ts", output_path, dxc_name.to_lowercase(), dxc_version.replace(".", "_"));

        let is_succeed = file_utils::create_file(&api_file_output_path).await;

        if !is_succeed {
            error!("创建文件 import_api.rs 失败...");
            return;
        }

        file_utils::write_content_to_file(&api_file_output_path, &builder.string().unwrap())
            .await
            .unwrap();

        //
    }

    gen_config(&output_path).await;
}


pub async fn gen_raw_api(input_path: String, output_path: String) {
  let input_path = if input_path == "./" {
      "./protos".into()
  } else {
      input_path
  };

  let output_path = if output_path == "./" {
      "./src/assets/api".into()
  } else {
      output_path
  };

  let input_path = file_utils::get_absoule_path(&input_path).unwrap();

  if !file_utils::is_dir_exist(&output_path).await {
      file_utils::create_dir(&output_path).await;
  }

  let output_path = file_utils::get_absoule_path(&output_path).unwrap();

  let file_list = file_utils::get_file_list(&input_path).await;

  if file_list.is_err() {
      panic!("获取路径 {} 下的 api 描述文件列表失败！", input_path);
  }
  //

  for filename in file_list.unwrap() {
      if !filename.starts_with("import-api-") {
          continue;
      }

      let sdk_file_full_path = format!("{}/{}", input_path, filename);
      let service_api_descriptor =
          ServiceApiDescriptor::load_with_include_path(sdk_file_full_path.as_str(), true);

      if service_api_descriptor.is_err() {
          let err = service_api_descriptor.err().unwrap();
          error!("解析描述文件 {} 出错：{}...", sdk_file_full_path, err);

          return;
      }

      let service_api_descriptor = service_api_descriptor.unwrap();
      //

      let dxc_name_and_version = filename.replace("import-api-", "").replace(".proto", "");

      let result = extract_dxc_name_and_version(&dxc_name_and_version);
      if result.is_none() {
          panic!("无法识别：{} 版本号！", filename);
      }

      let (dxc_name, dxc_version) = result.unwrap();

      let version = Version::parse(&dxc_version);

      if version.is_err() {
          panic!("无法识别：{} 版本号:{}", filename, &dxc_version);
      }

      let mut builder = Builder::new(1024 * 1024);
      gen_raw_code(
          &service_api_descriptor,
          &dxc_name_and_version,
          &dxc_version,
          &mut builder,
      );


      let api_file_output_path =
          format!("{}/{}_{}.ts", output_path, dxc_name.to_lowercase(), dxc_version.replace(".", "_"));

      let is_succeed = file_utils::create_file(&api_file_output_path).await;

      if !is_succeed {
          error!("创建文件 import_api.rs 失败...");
          return;
      }

      file_utils::write_content_to_file(&api_file_output_path, &builder.string().unwrap())
          .await
          .unwrap();

      //
  }

  gen_config(&output_path).await;
}

async fn gen_config(output_path: &str) {
    let mut builder = Builder::new(1024 * 1024);

//     builder.append(
//         r#"
// import { Body, fetch } from "@tauri-apps/api/http";

// export default class Common {
//     public static  http_url = "http://127.0.0.1:9091/invoke";

//     public static content_type = "application/json";

//     static async Invoke(param: any) {
//         const resp = await fetch(Common.http_url, {
//             method: "POST",
//             headers: {
//                 'content-type': Common.content_type,
//             },
//             body: Body.json(param)
//         });
//         return resp.data;
//     }
// }

// export interface DXMResponse<T> {
//   err_code: number;
//   err_msg: string;
//   data: T;
// }



//     "#,
//     );

builder.append(
  r#"

export interface DXMResponse<T> {
err_code: number;
err_msg: string;
data: T;
}

"#,
);
    let config_file_output_path = format!("{}/common.ts", output_path);
    file_utils::write_content_to_file(&config_file_output_path, &builder.string().unwrap())
        .await
        .unwrap();
}

fn gen_http_code(
    service_api_descriptor: &ServiceApiDescriptor,
    _dxc_name: &str,
    dxc_version: &str,
    builder: &mut Builder,
) {
    builder.append("import Common, {DXMResponse} from \"./common\";\n");

    builder.append("import { Body, fetch } from \"@tauri-apps/api/http\";\n");

    builder.append(format!(
        "const dxc_name = \"{}\"\n",
        service_api_descriptor.service_name()
    ));

    builder.append(format!("const dxc_version = \"{}\"\n", &dxc_version));

    // 生成结构体

    let mut nest_message = NestMessage {
        message_name: String::new(),
        sub_message: HashMap::new(),
        is_enum: false,
    };

    // 放进来
    for message_name in service_api_descriptor.message_descriptor_holder.keys() {
        put_message_in_nest_map(message_name, &mut nest_message, false);
    }
    //
    for enum_name in service_api_descriptor.enum_descriptor_map.keys() {
        // 排序
        put_message_in_nest_map(enum_name, &mut nest_message, true);
    }
    //
    // 生成枚举

    for (_key, value) in &nest_message.sub_message {
        if !value.is_enum {
            continue;
        }
        gen_rust_enum(
            service_api_descriptor
                .enum_descriptor_map
                .get(&value.message_name)
                .unwrap(),
            builder,
        )
    }

    for (_key, value) in &nest_message.sub_message {
        if value.is_enum {
            continue;
        }
        gen_type(
            service_api_descriptor
                .message_descriptor_holder
                .get(&value.message_name)
                .unwrap(),
            builder,
        )
    }

    gen_http_function(service_api_descriptor, builder);
}


fn gen_raw_code(
  service_api_descriptor: &ServiceApiDescriptor,
  _dxc_name: &str,
  dxc_version: &str,
  builder: &mut Builder,
) {
  builder.append("import {DXMResponse} from \"./common\";\n");

  builder.append("import { invoke } from \"@tauri-apps/api/core\";\n");

  builder.append(format!(
      "const dxc_name = \"{}\"\n",
      service_api_descriptor.service_name()
  ));

  builder.append(format!("const dxc_version = \"{}\"\n", &dxc_version));

  // 生成结构体

  let mut nest_message = NestMessage {
      message_name: String::new(),
      sub_message: HashMap::new(),
      is_enum: false,
  };

  // 放进来
  for message_name in service_api_descriptor.message_descriptor_holder.keys() {
      put_message_in_nest_map(message_name, &mut nest_message, false);
  }
  //
  for enum_name in service_api_descriptor.enum_descriptor_map.keys() {
      // 排序
      put_message_in_nest_map(enum_name, &mut nest_message, true);
  }
  //
  // 生成枚举

  for (_key, value) in &nest_message.sub_message {
      if !value.is_enum {
          continue;
      }
      gen_rust_enum(
          service_api_descriptor
              .enum_descriptor_map
              .get(&value.message_name)
              .unwrap(),
          builder,
      )
  }

  for (_key, value) in &nest_message.sub_message {
      if value.is_enum {
          continue;
      }
      gen_type(
          service_api_descriptor
              .message_descriptor_holder
              .get(&value.message_name)
              .unwrap(),
          builder,
      )
  }

  gen_raw_function(service_api_descriptor, builder);
}


fn gen_rust_enum(enum_info: &Enumeration, builder: &mut Builder) {
    let enum_name = enum_info.name.replace(".", "");
    builder.append(format!("export enum {} {{ \n", enum_name));

    for field in &enum_info.values {
        builder.append(format!("  {} = {}, \n", &field.name, field.number));
    }

    builder.append("}\n");
}

fn gen_type(struct_info: &DescriptorProtoHolder, builder: &mut Builder) {
    let struct_info = &struct_info.descriptor_proto;

    let struct_name = struct_info.name.replace(".", "");
    builder.append(format!("export type {} = {{ \n", struct_name));

    for field in &struct_info.fields {
        let field = get_inner_field(field).unwrap();
        let mut field_name = field.name.clone();
        let mut field_type = proto_field_type_to_ts(&field.typ);
        if api_descriptor::is_optional(field) {
            field_name = format!("{}?", field_name);
        }
        if api_descriptor::is_array(field) {
            field_type = format!("Array<{}>", field_type);
        }

        builder.append(format!("  {}: {}, \n", &field_name, field_type));
    }

    builder.append("}\n");
}

fn gen_http_function(service_api_descriptor: &ServiceApiDescriptor, builder: &mut Builder) {
    let methods = service_api_descriptor.methods();

    for method in methods {
        builder.append(format!("export async function {} (", method.name));

        let input_type = method.input_type.get_path();

        let is_param_empty = input_type.is_empty();

        let mut post_params = String::from("{}");

        let output_type = method.output_type.get_path();

        let is_output_empty = output_type.is_empty();

        if !is_param_empty {
            builder.append("param: ");
            let ts_input_type = proto_utils::protobuf_path_to_ts(&input_type);
            builder.append(ts_input_type.as_bytes());
            //
            post_params = String::from("param");
        }
        let mut ts_output_type = String::from("{}");

        if !is_output_empty {
            ts_output_type = proto_utils::protobuf_path_to_ts(&output_type);
        }

        builder.append(") { \n");

        builder.append(format!(
            r#"    const resp = await fetch(Common.http_url, {{
        method: "POST",
        headers: {{
            'content-type': Common.content_type,
        }},
        body: Body.json({{
            dxc_name,
            dxc_version,
            api: "{}",
            params: {}
        }})
    }});
    return resp.data as DXMResponse<{}>;"#,
            method.name, post_params, ts_output_type
        ));

        builder.append("\n} \n\n");
    }
}


fn gen_raw_function(service_api_descriptor: &ServiceApiDescriptor, builder: &mut Builder) {
  let methods = service_api_descriptor.methods();

  for method in methods {
      builder.append(format!("export async function {} (", method.name));

      let input_type = method.input_type.get_path();

      let is_param_empty = input_type.is_empty();

      let mut post_params = String::from("{}");

      let output_type = method.output_type.get_path();

      let is_output_empty = output_type.is_empty();

      if !is_param_empty {
          builder.append("param: ");
          let ts_input_type = proto_utils::protobuf_path_to_ts(&input_type);
          builder.append(ts_input_type.as_bytes());
          //
          post_params = String::from("param");
      }
      let mut ts_output_type = String::from("{}");

      if !is_output_empty {
          ts_output_type = proto_utils::protobuf_path_to_ts(&output_type);
      }

      builder.append(") { \n");

      builder.append(format!(
          r#"    const ret = await invoke("invoke_dxc", {{
            invokeRequest: {{
              dxc_name,
              dxc_version,
              api: "{}",
              params: {}
            }}
  }});
  return ret as DXMResponse<{}>;"#,
          method.name, post_params, ts_output_type
      ));

      builder.append("\n} \n\n");
  }
}
