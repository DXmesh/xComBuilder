use std::collections::HashMap;
use x_common_lib::base::config::Config;
use x_common_lib::serial::api_descriptor::ServiceApiDescriptor;
use x_common_lib::utils::{file_utils, string_utils};
pub mod gen_sdk;
pub mod init_project;
pub mod new_project;
pub mod pack_project;
pub mod gen_tauri;
pub mod serial;
pub mod service;
pub mod gen_serial_code;
pub mod gen_doc;

#[derive(Debug)]
pub struct NestMessage {
    message_name: String,
    is_enum: bool,
    sub_message: HashMap<String, NestMessage>,
}

pub async fn get_source_proto_file_name(path: &str) -> String {
    let proto_path = format!("{}/protos", path);

    let proto_file_list = file_utils::get_file_list(&proto_path).await;
    if proto_file_list.is_err() {
        panic!("获取路径 {} 下的 api 描述文件列表失败！", proto_path);
    }

    let proto_file_list = proto_file_list.unwrap();

    let source_file_name = proto_file_list
        .iter()
        .enumerate()
        .find(|(_, ele)| ele.starts_with("source-api"))
        .map(|(_, ele)| ele);

    if source_file_name.is_none() {
        panic!("缺少 protos/source-api proto 描述文件！");
    }

    let source_file_name = source_file_name.unwrap();

    source_file_name.into()
}

pub fn get_dxc_name_and_version_source_api(path: &str, source_file_name: &str) -> (String, String) {
    let new_source_version = source_file_name
        .replace("source-api", "version")
        .replace(".proto", "");

    let dxc_version = string_utils::extract_version(&new_source_version).expect("获取版本号错误！");

    let dxc_version = dxc_version.replace("_", ".");

    // let proto_path = format!("{}/protos", path);

    let dsl_path = format!("{}/protos/{}", path, source_file_name);
    //

    let service_api_descriptor =
        ServiceApiDescriptor::load_with_include_path(dsl_path.as_str(),  true)
            .expect("获取应用名失败！");

    (service_api_descriptor.service_name().into(), dxc_version)
}
#[allow(dead_code)]
pub async fn get_dxc_name_and_version(cargo_toml_path: &str) -> (String, String) {
    // let cargo_toml_path = format!("{}/Cargo.toml", path);
    let project_toml = Config::load(&cargo_toml_path);
    let package_name = project_toml
        .get_str("package", "name")
        .expect("Cargo.toml 文件中 package 缺少 name 字段");

    let package_version = project_toml
        .get_str("package", "version")
        .expect("Cargo.toml 文件中 package 缺少 version 字段");

    (package_name.into(), package_version.into())
}


fn put_message_in_nest_map(message_name: &str, nest_message: &mut NestMessage, is_enum: bool) {
    let words: Vec<&str> = message_name.split(".").filter(|&s| !s.is_empty()).collect();
    let mut cur_nest_message = nest_message;
    // 添加对象

    let mut i = 0;
    while i < words.len() {
        let word = words[i];
        let snake_word = string_utils::convert_to_snake_case(word);
        let sub_message = cur_nest_message
            .sub_message
            .entry(snake_word.clone())
            .or_insert(NestMessage {
                message_name: String::new(),
                is_enum: false,
                sub_message: HashMap::new(),
            });
        // 最后一个元素
        if i == words.len() - 1 {
            sub_message.message_name = message_name.to_string();
            sub_message.is_enum = is_enum
        }

        cur_nest_message = sub_message;

        i += 1;
    }
}
