use dxc_protobuf_parser::parser::model::{Field, FieldType};
use x_common_lib::serial::api_descriptor::{get_map_key_value_type, is_enum};
use x_common_lib::{
    serial::api_descriptor::{self, is_map},
    utils::string_utils,
};

#[allow(dead_code)]
pub fn is_nest(input: &str) -> bool {
    let mut dot_count = 0;
    for c in input.chars() {
        if c == '.' {
            dot_count += 1;
            if dot_count > 2 {
                return true;
            }
        }
    }
    false
}

pub fn protobuf_path_to_rust(input: &str) -> String {
    let mut words: Vec<&str> = input.split(".").filter(|&s| !s.is_empty()).collect();
    if words.len() == 1 {
        return words.get(0).unwrap().to_string();
    }
    let last_word = words.pop().unwrap();

    let snake_words: Vec<String> = words
        .into_iter()
        .map(string_utils::convert_to_snake_case)
        .collect();

    format!("{}::{}", snake_words.join("::"), last_word)
}

pub fn protobuf_path_to_python(input: &str) -> String {
    let mut words: Vec<&str> = input.split(".").filter(|&s| !s.is_empty()).collect();
    if words.len() == 1 {
        return words.get(0).unwrap().to_string();
    }
    let last_word = words.pop().unwrap();

    format!("{}.{}", words.join("."), last_word)
}

pub fn protobuf_path_to_ts(input: &str) -> String {
    let mut words: Vec<&str> = input.split(".").filter(|&s| !s.is_empty()).collect();
    if words.len() == 1 {
        return words.get(0).unwrap().to_string();
    }
    let last_word = words.pop().unwrap();

    format!("{}.{}", words.join("."), last_word)
}

pub fn protobuf_to_rust_default_value(field: &Field) -> String {
    if is_map(field) {
        return "std::collections::HashMap::new()".to_string();
    }

    if api_descriptor::is_optional(field) {
        return "None".to_string();
    }

    if api_descriptor::is_array(field) {
        return "Vec::new()".to_string();
    }

    match &field.typ {
        FieldType::Double | FieldType::Float => String::from("0.0"),
        FieldType::Int32
        | FieldType::Int64
        | FieldType::Uint32
        | FieldType::Uint64
        | FieldType::Sint32
        | FieldType::Sint64
        | FieldType::Fixed32
        | FieldType::Fixed64
        | FieldType::Sfixed32
        | FieldType::Sfixed64 => String::from("0"),
        FieldType::Bool => String::from("false"),
        FieldType::String => String::from("String::default()"),
        FieldType::Message(path) => {
            let struct_name: String = path.get_path();
            let struct_type = protobuf_path_to_rust(&struct_name);
            format!("Box::new({}::default())", struct_type)
        }
        FieldType::Enum(path) => {
            let enum_name = path.get_path();
            let enum_type = protobuf_path_to_rust(&enum_name);
            String::from(enum_type + "::from_i32(0)")
        }
        _ => panic!("不支持类型:{:?}", field.typ),
    }
}

fn proto_field_type_to_rust(field_type: &FieldType) -> String {
    match field_type {
        FieldType::Double => String::from("f64"),
        FieldType::Float => String::from("f32"),
        FieldType::Int32 => String::from("i32"),
        FieldType::Int64 => String::from("i64"),
        FieldType::Uint32 => String::from("u32"),
        FieldType::Uint64 => String::from("u64"),
        FieldType::Sint32 => String::from("i32"),
        FieldType::Sint64 => String::from("i64"),
        FieldType::Fixed32 => String::from("u32"),
        FieldType::Fixed64 => String::from("u64"),
        FieldType::Sfixed32 => String::from("i32"),
        FieldType::Sfixed64 => String::from("i64"),
        FieldType::Bool => String::from("bool"),
        FieldType::String => String::from("String"),
        FieldType::Bytes => String::from("Vec<u8>"),
        FieldType::Message(path) => {
            let type_name = path.get_path();
            format!("Box<{}>", protobuf_path_to_rust(&type_name))
        }
        FieldType::Enum(path) => {
            let type_name = path.get_path();
            protobuf_path_to_rust(&type_name)
        }
        FieldType::Map(path) => make_rust_map_type(&path.0, &path.1),
        _ => panic!("不支持类型:{:?}", field_type),
    }
}

pub fn protobuf_path_to_java(input: &str) -> String {
    let words: Vec<&str> = input.split(".").filter(|&s| !s.is_empty()).collect();
    return words.join(".");
}

fn proto_field_type_to_java(field_type: &FieldType) -> String {
    match field_type {
        FieldType::Double => String::from("double"),
        FieldType::Float => String::from("float"),
        FieldType::Int32 => String::from("int"),
        FieldType::Int64 => String::from("long"),
        FieldType::Uint32 => String::from("int"),
        FieldType::Uint64 => String::from("long"),
        FieldType::Sint32 => String::from("int"),
        FieldType::Sint64 => String::from("long"),
        FieldType::Fixed32 => String::from("int"),
        FieldType::Fixed64 => String::from("long"),
        FieldType::Sfixed32 => String::from("int"),
        FieldType::Sfixed64 => String::from("long"),
        FieldType::Bool => String::from("boolean"),
        FieldType::String => String::from("String"),
        FieldType::Bytes => String::from("bytes[]"),
        FieldType::Enum(path) | FieldType::Message(path) => {
            let type_name = path.get_path();
            protobuf_path_to_java(&type_name)
        }
        _ => panic!("不支持类型:{:?}", field_type),
    }
}

pub fn proto_field_type_to_java_object(field_type: &FieldType) -> String {
    match field_type {
        FieldType::Double => String::from("Double"),
        FieldType::Float => String::from("Float"),
        FieldType::Int32 => String::from("Integer"),
        FieldType::Int64 => String::from("Long"),
        FieldType::Uint32 => String::from("Integer"),
        FieldType::Uint64 => String::from("Long"),
        FieldType::Sint32 => String::from("Integer"),
        FieldType::Sint64 => String::from("Long"),
        FieldType::Fixed32 => String::from("Integer"),
        FieldType::Fixed64 => String::from("Long"),
        FieldType::Sfixed32 => String::from("Integer"),
        FieldType::Sfixed64 => String::from("Long"),
        FieldType::Bool => String::from("Boolean"),
        FieldType::String => String::from("String"),
        FieldType::Bytes => String::from("bytes[]"),
        FieldType::Enum(path) | FieldType::Message(path) => {
            let path_string = path.get_path();
            protobuf_path_to_java(&path_string)
        }
        _ => panic!("不支持类型:{:?}", field_type),
    }
}

fn make_rust_map_type(key_type: &FieldType, value_type: &FieldType) -> String {
    let key_type = proto_field_type_to_rust(key_type);

    let mut value_type_str = proto_field_type_to_rust(value_type);
    if is_enum(value_type) {
        value_type_str = format!("Option<{}>", value_type_str);
    }
    format!(
        "std::collections::HashMap<{}, {}>",
        key_type, value_type_str
    )
}

pub fn protobuf_type_to_rust(field: &Field) -> String {
    if is_map(field) {
        let (key_field_type, value_field_type) = get_map_key_value_type(&field.typ).unwrap();

        return make_rust_map_type(key_field_type, value_field_type);
    }

    let mut rust_type = proto_field_type_to_rust(&field.typ);

    let is_optional = api_descriptor::is_optional(field);

    if is_optional || is_enum(&field.typ) {
        rust_type = format!("Option<{}>", rust_type);
    }

    let is_array = api_descriptor::is_array(field);
    if is_array {
        rust_type = format!("Vec<{}>", rust_type);
    }
    rust_type
}

pub fn protobuf_type_to_java(field: &Field) -> String {
    if is_map(field) {
        let (key_field_type, value_field_type) = get_map_key_value_type(&field.typ).unwrap();

        let key_type = proto_field_type_to_java_object(key_field_type);
        let value_type = proto_field_type_to_java_object(value_field_type);

        return format!("HashMap<{}, {}>", key_type, value_type);
    }

    let mut java_type = proto_field_type_to_java(&field.typ);

    let is_array = api_descriptor::is_array(field);
    if is_array {
        let java_object_type = proto_field_type_to_java_object(&field.typ);

        java_type = format!("ArrayList<{}>", java_object_type);
    }
    java_type
}

pub fn proto_type_to_java_map_entry(field_type: &FieldType) -> String {
    let (key_field_type, value_field_type) = get_map_key_value_type(field_type).unwrap();

    let key_type = proto_field_type_to_java_object(key_field_type);
    let value_type = proto_field_type_to_java_object(value_field_type);
    format!("HashMap.Entry<{}, {}>", key_type, value_type)
}

fn proto_field_type_to_python(field_type: &FieldType) -> String {
    match field_type {
        FieldType::Double => String::from("float"),
        FieldType::Float => String::from("float"),
        FieldType::Int32 => String::from("int"),
        FieldType::Int64 => String::from("int"),
        FieldType::Uint32 => String::from("int"),
        FieldType::Uint64 => String::from("int"),
        FieldType::Sint32 => String::from("int"),
        FieldType::Sint64 => String::from("int"),
        FieldType::Fixed32 => String::from("int"),
        FieldType::Fixed64 => String::from("int"),
        FieldType::Sfixed32 => String::from("int"),
        FieldType::Sfixed64 => String::from("int"),
        FieldType::Bool => String::from("bool"),
        FieldType::String => String::from("str"),
        FieldType::Bytes => String::from("bytes"),
        FieldType::Enum(path) | FieldType::Message(path) => {
            let type_name = path.get_path();
            protobuf_path_to_python(&type_name)
        }
        _ => panic!("不支持类型:{:?}", field_type),
    }
}




pub fn proto_field_type_to_ts(field_type: &FieldType) -> String {
    match field_type {
        FieldType::Double => String::from("number"),
        FieldType::Float => String::from("number"),
        FieldType::Int32 => String::from("number"),
        FieldType::Int64 => String::from("number | string"),
        FieldType::Uint32 => String::from("number"),
        FieldType::Uint64 => String::from("number | string"),
        FieldType::Sint32 => String::from("number"),
        FieldType::Sint64 => String::from("number | string"),
        FieldType::Fixed32 => String::from("number"),
        FieldType::Fixed64 => String::from("number | string"),
        FieldType::Sfixed32 => String::from("number"),
        FieldType::Sfixed64 => String::from("number | string"),
        FieldType::Bool => String::from("boolean"),
        FieldType::String => String::from("string"),
        FieldType::Bytes => String::from("string"),
        FieldType::Enum(path) | FieldType::Message(path) => {
            let type_name = path.get_path();
            let words: Vec<&str> = type_name.split(".").filter(|&s| !s.is_empty()).collect();

            words[0].to_string()
        }
        FieldType::Map(path) =>{

            let key_type = proto_field_type_to_ts(&path.0);
            let value_type = proto_field_type_to_ts(&path.1);

            format!("Map<{}, {}>", key_type, value_type)

        }
        _ => panic!("不支持类型:{:?}", field_type),
    }
}


pub fn proto_field_type_to_proto(field_type: &FieldType) -> String {
  match field_type {
      FieldType::Double => String::from("double"),
      FieldType::Float => String::from("float"),
      FieldType::Int32 => String::from("int32"),
      FieldType::Int64 => String::from("int64"),
      FieldType::Uint32 => String::from("uint32"),
      FieldType::Uint64 => String::from("uint64"),
      FieldType::Sint32 => String::from("sint32"),
      FieldType::Sint64 => String::from("sint64"),
      FieldType::Fixed32 => String::from("fixed32"),
      FieldType::Fixed64 => String::from("fixed64"),
      FieldType::Sfixed32 => String::from("sfixed32"),
      FieldType::Sfixed64 => String::from("sfixed64"),
      FieldType::Bool => String::from("boolean"),
      FieldType::String => String::from("string"),
      FieldType::Bytes => String::from("bytes"),
      FieldType::Enum(path) | FieldType::Message(path) => {
          let type_name = path.get_path();
          let words: Vec<&str> = type_name.split(".").filter(|&s| !s.is_empty()).collect();

          words[0].to_string()
      }
      FieldType::Map(path) =>{

        let key_type = proto_field_type_to_proto(&path.0);
        let value_type = proto_field_type_to_proto(&path.1);

        format!("map<{}, {}>", key_type, value_type)

      }
      _ => panic!("不支持类型:{:?}", field_type),
  }
}

pub fn protobuf_type_to_python(field: &Field) -> String {
    //
    if is_map(field) {
        let (key_type, value_type) = get_map_key_value_type(&field.typ).unwrap();
        let key_type = proto_field_type_to_python(key_type);
        let value_type = proto_field_type_to_python(value_type);
        return format!("Dict[{}, {}]", key_type, value_type);
    }

    let mut rust_type = proto_field_type_to_python(&field.typ);

    let is_array = api_descriptor::is_array(field);
    if is_array {
        rust_type = format!("List[{}]", rust_type);
    }
    rust_type
}

pub fn protobuf_type_to_python_default_value(field: &Field) -> String {
    //
    if is_map(field) {
        return String::from("{}");
    }
    if api_descriptor::is_array(field) {
        return String::from("[]");
    }

     match &field.typ {
      FieldType::Double => String::from("0.0"),
      FieldType::Float => String::from("0.0"),
      FieldType::Int32 => String::from("0"),
      FieldType::Int64 => String::from("0"),
      FieldType::Uint32 => String::from("0"),
      FieldType::Uint64 => String::from("0"),
      FieldType::Sint32 => String::from("0"),
      FieldType::Sint64 => String::from("0"),
      FieldType::Fixed32 => String::from("0"),
      FieldType::Fixed64 => String::from("0"),
      FieldType::Sfixed32 => String::from("0"),
      FieldType::Sfixed64 => String::from("0"),
      FieldType::Bool => String::from("False"),
      FieldType::String => String::from("\"\""),
      FieldType::Bytes => String::from("bytes()"),
      FieldType::Message(path) => {
        let type_name = path.get_path();
        format!("{}()", protobuf_path_to_python(&type_name))
      }
      FieldType::Enum(_) =>{
        return  String::default();
      }
      _ => panic!("不支持类型:{:?}", field.typ),
  }
 
}
